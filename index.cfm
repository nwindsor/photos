<!doctype html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> 

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" rel="stylesheet">    
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <style>
        * { box-sizing: border-box; }

        /* force scrollbar */
        html { overflow-y: scroll; }

        body { font-family: sans-serif; }

        /* ---- grid ---- */

        .grid {
        
        }

        /* clear fix */
        .grid:after {
        content: '';
        display: block;
        clear: both;
        }

        /* ---- .grid-item ---- */

        .grid-sizer,
        .item {
        width: 30%;
        margin-bottom:10px;
        }

        .item {
        float: left;
        }

        .item img {
        display: block;
        max-width: 100%;
        }

        .item a {
            color: inherit;
            text-decoration: inherit;
        }

        .sortfield {
            border: none;
            border-radius: 1rem;
            outline: none;
            transition: .1s;
            display: inline-flex;
            align-items: center;
            padding: 0.3rem;
            padding-right: 0.5rem;
            padding-left: 0.5rem;
            background: none;
            margin-left:0.6rem;
            margin-right:0.6rem;
        }

        .selected {
            box-shadow: 0 0 0 1pt #043B86;
            color: #043B86;
        }

        .descend i {
            transform: scaleY(-1);
        }

        .sortfield:hover {
            box-shadow: 0 0 0 1pt #00000040;
        }

        .selected:hover {
            box-shadow: 0 0 0 1pt #043B8680;
        }

        .sortfield span {
            padding-left: 0.3rem;
        }
        
        #sortbar {
            display: flex;
            flex-wrap: wrap;
            flex-direction: row;
            justify-content: center;
            align-items: center;
            align-content: center;
            margin-left: 5%;
            margin-right: 5%;
        }

        .bottombar {
            border: none;
            border-radius: 5rem;
            outline: none;
            transition: .1s;
            text-align: center;
            bottom: 5%;
            color: white;
            padding: 0.5rem;
            padding-right: 4rem;
            padding-left: 4rem;
            background: #888;
            font-size: 1.5rem;
            position: fixed;
            align-items: center;
            left: 50%;
            transform: translate(-50%,0);
            display: none;
        }

        .bottombar i {
            margin-right: 0.3rem;
        }

        .bottombar:hover {
            background: #aaa;
        }

        .navback:hover i {
            transition: .1s;
            margin-right: 0.25rem;
        }

        .bottombar input {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            z-index: 10;
        }

        .childcounter {
            top: 100%;
            transform: translateY(-100%);
            position: relative;
        }

        .childcounter span {
            margin-left: 0.2rem;
            border: none;
            border-radius: 5rem;
            outline: none;
            text-align: left;
            color: white;
            background: #00000066;
            padding-left: 0.5rem;
            padding-right: 0.5rem;
            padding: 0.3rem;
        }
    </style>
    
    <script>
    <!--- js --->
        nameFormat = "#gamertag#";
        descFormat = "";
        libraries = {};

        <!--- navigation array; [libraryId, [itemid, name], photoId] --->
        position = [];

        <!--- sets up and reloads masonry for the given gallery items --->
        function reMason()
        {
            $('#gallery').masonry('reloadItems');
                $("#gallery").imagesLoaded().progress( function() {
                    $("#gallery").masonry();
                }); 
        }
        
        <!--- generates HTML for a button for a 'sort by' field --->
        function sortField(strField)
        {
            return `
                <button id="sort_`+strField+`" class="sortfield" onClick="sortBy('`+strField+`');">
                    <i class="bi bi-chevron-down"></i>
                    <span>` + camelToTitle(strField) + `</span>    
                </button>
            `;
        }

        <!--- generates HTML for a library's sort bar given its library id--->
        function sortBar(libraryId)
        {
            return libraries[libraryId].visibleColumns.split(", ").slice(0, -1).map(sortField).join("");
        }
        
        <!---
            reloads items, sorted by a specific field.
            if currently sorted by said field, flips the sort (ascending vs descending).
        --->
        function sortBy(sort)
        {
            if($("#sort_" + sort).hasClass("selected"))
            {
                <!--- flips the sort order (ascending vs descending) --->
                itemSort(position[0], sort + ($("#sort_" + sort).hasClass("descend") ? " ASC" : " DESC"));
                $("#sort_" + sort).toggleClass("descend");
            }
            else
            {
                <!--- sorts by the given field --->
                $(".selected").removeClass("selected");
                $("#sort_" + sort).addClass("selected");
                itemSort(position[0], sort + ($("#sort_" + sort).hasClass("descend") ? " DESC" : " ASC"));
            }
        }

        <!--- fills gallery with items in a library, sorted by a given sort string. --->
        function itemSort(libraryId, str)
        {
            $.get("/rest/api/view/libraries/" + libraryId, {sort: str}, (data) => {
                $("#gallery").html('<div class="grid-sizer"></div>' + data.map(itemCard).join(""));
                reMason();
            });
        }

        <!--- 
         * Converts a camelCased string into a fully capitalized and spaced out string.
         * example: databaseTable -> Database Table
        --->
        function camelToTitle(title)
        {
            return title.charAt(0).toUpperCase() + title.slice(1).replace(/([A-Z])/g, ' $1');
        }

        <!---
         * Any text enclosed in #hashtags# are replaced with the field in a struct
         * matching said text. 
        --->
        function formatFromStruct(str, data)
        {
            // Result string
            r = "";

            // Sorts out tags from the item format string

            // Splits the array by the # operator.
            spl = str.split('#');
            for(let i = 0; i < spl.length; i++)
            {
                // Determines if index is odd or even
                if(i % 2)
                {
                    // The array index is even, and thus a tag or an escaped # symbol.
                    if(spl[i] == "")
                    {
                        // This is an escaped # symbol.
                        r += "#";
                    }
                    else
                    {
                        // This is a tag. Replaces it with the item's value at the tag's key.
                        r += data[spl[i]];
                    }
                }
                else
                {
                    // The array index is odd, and therefore text. Adds it to the string.
                    r += spl[i];
                }
            }

            return r;
        }

        <!---
         * Given a library retreived from a REST call, generates a card, which has:
         * - an image (the library thumbnail if possible, if not, the first matching image it can find)
         * - the name of the library, spaced out with the first letter of each word capitalized.
         --->
        function libraryCard(library)
        {
            return card(
                library.thumbnail ? 'id=' + library.thumbnail : 'lib=' + library.id,
                camelToTitle(library.name),
                'viewItems(' + library.id + ');',
                library.numItems
                );
        }

        <!---
         * Given an item retreived from a REST call, generates a card, which has:
         * - an image (the item thumbnail if possible, if not, the first matching image)
         * - the name of the item a la the library's format. #tags# are replaced with details from the source database.
        --->
        function itemCard(item)
        {
            itemname = formatFromStruct(nameFormat, item);

            return card(
                item.thumbnail ? 'id=' + item.thumbnail : 'item=' + item.galleryitemid,
                itemname,
                'viewPhotos(' + item.galleryitemid + ', `' + itemname + '`);',
                item.numphotos
            );
        }

        <!---
         * Given a photo retreived from a REST call, generates a card, which has:
         * - a photo
         * - the id of the photo followed by its type
        --->
        function photoCard(photo)
        {
            return card(
                'id=' + photo.id,
                photo.id + '.' + photo.type,
                'viewSizes(' + photo.id + ');',
                0
            );
        }

        <!---
            Generates a card given the url arguments to an image and the text to go below it.
        --->
        function card(imgArgs, text, onclick, count)
        {
            return `
            <div class="item">
                <a class="stretched-link" onClick="` + onclick + `">
                <div class="card">
                    <div>
                        <img class="card-img-top" src=/img.cfm?` + imgArgs + `>
                        `+ ((count == 1 || count == 0) ? '' : '<div class="childcounter"><span>' + count + '</span></div>')+` 
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">` + text + `</h5>
                    </div>
                </div>
                </a>
            </div>`;
        }

        <!--- Returns the HTML code for an image with the following photo ID. --->
        function photoSize(photo)
        {
            return '<div class="item"><img src=/img.cfm?id=' + photo.id + '></div>';
        }

        <!--- Views a list of libraries as retrieved from a REST call --->
        function viewLibraries()
        {
            $("#sortbar").hide();

            <!--- Get libraries from a REST call --->
            $.get("/rest/api/view/libraries").done(function(data) {
                libraries = data;
                $("#galleryheader").html("Libraries");

                $("#gallery").html('<div class="grid-sizer"></div>' + Object.values(data).map(libraryCard).join(""));
                
                position = [];
                history.pushState({p: position}, "libraries");
                
                reMason();
            });
        }

        <!---  --->
        function viewItems(libraryId)
        {
            $.get("/rest/api/view/libraries/" + libraryId).done(function(data) {
                $("#galleryheader").html(camelToTitle(libraries[libraryId].name));
                nameFormat = libraries[libraryId].itemNameFormat == undefined ? "#galleryItemId#" : libraries[libraryId].itemNameFormat;
                
                $(".bottombar").hide();
                $("#sortbar").show();
                $("#sortbar").html(sortBar(libraryId));
                
                $("#gallery").html('<div class="grid-sizer"></div>' + data.map(itemCard).join(""));
                
                position = [libraryId];
                history.pushState({p: position}, "items");

                reMason();
            });
        }

        function viewPhotos(itemId, name)
        {
            $.get("/rest/api/view/items/" + itemId).done(function(data) {
                $("#galleryheader").html(name);

                $("#sortbar").hide();
                $(".bottombar").show();

                $("#gallery").html('<div class="grid-sizer"></div>' + data.map(photoCard).join(""));
                
                position = [position[0], [itemId, name]];
                history.pushState({p: position}, "photos");

                reMason();
            });

            
        }

        function viewSizes(photoId)
        {
            $.get("/rest/api/view/photos/" + photoId).done(function(data) {
                $("#galleryheader").text("Viewing All Sizes");
                $(".bottombar").hide();
                $("#gallery").html('<div class="grid-sizer"></div>' + data.map(photoSize).join(""));

                position[2] = photoId;
                history.pushState({p: position}, "uwu");

                reMason();
            });
        }

        function previous()
        {
            switch(position.length)
            {
                case 0:
                    window.history.go(-1);
                case 1:
                    viewLibraries();
                    break;
                case 2:
                    viewItems(position[0]);
                    break;
                case 3:
                    viewPhotos(position[1][0], position[1][1]);
                    break;
            }
        }

        function load()
        {
            switch(position.length)
            {
                case 0:
                    viewLibraries();
                    break;
                case 1:
                    viewItems(position[0]);
                    break;
                case 2:
                    viewPhotos(position[1][0], position[1][1]);
                    break;
                case 3:
                    viewSizes(position[2]);
                    break;
            }
        }

        window.addEventListener('popstate', (event) => {
            previous();
        });

        $(document).ready(function() {
            load();

            var $grid = $('.grid').masonry({
                itemSelector: '.item',
                percentPosition: true,
                columnWidth: '.grid-sizer',
                gutter: 10
            });
            // layout Masonry after each image loads
            $grid.imagesLoaded().progress( function() {
            $grid.masonry();
            });  

            $("#file").on('change', function () {
                <!--- upload file --->
                var files = document.getElementById("file").files;
                var req = new XMLHttpRequest();
                var data = new FormData();

                data.append('upload', files[0]);
                data.append('i', position[1][0]);

                req.open("POST", "/rest/api/edit/upload");
                req.send(data);

                setTimeout(() => {
                    load();
                }, 1000);
            });

        });
        

    </script>
</head>

<!--- TODO: add header --->

<body>
    <!--- HEADER --->

    <!--- SEARCH --->
    
    <!--- DESC --->
    <div class="position-relative fs-2 mb-3 text-center navback">
        <i id="back" class="bi bi-chevron-left"></i>
        <span id="galleryheader">Libraries</span>
        <a class="stretched-link" onClick="previous();"></a>
    </div>
    
    <!--- SORT BAR --->
    <div id="sortbar"></div>

    <!--- BOXES --->
    <div class="container py-5">
        <div id="gallery" class="grid"></div>
    
        
    
    <!--- FOOTER --->
    </div>

    <!--- UI --->
    <form enctype="multipart/form-data">
        <div class="bottombar">
            <input id="file" type="file" />
            <i class="bi bi-camera"></i>
            Add Photo
        </div>
    </input>
    </form>
        

    
</body>
</html>