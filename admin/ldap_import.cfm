<cfparam name="url.step" default="1" />


<cfinvoke component="list" method="list_data" list_id="68" returnVariable="branches"></cfinvoke>
<cfinvoke component="list" method="list_data" list_id="73" returnVariable="tasks"></cfinvoke>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: User Import</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" -->
<h2>LDAP User Import</h2>
<p>Use this to import new users into the system so you can assign them permissions.</p>

<cfquery datasource="#application.dsn#" name="getLDAP">
SELECT * FROM server_ldap
</cfquery>

<cfquery datasource="#application.dsn#" name="getTZ">
SELECT * from timezones
WHERE
	name NOT LIKE 'Etc%' AND
    name NOT LIKE 'System%'
ORDER BY utcoffset, name
</cfquery>

<cfif getLDAP.recordcount eq 0>
<p style="font-style:italic;">No LDAP settings have been defined for login.  They must exist before you can use them to import users.</p>
<cfelse>

<cfswitch expression="#url.step#">

<cfcase value="1">
    <form method="post" action="ldap_import.cfm?step=2">
        <table class="formTable">
            <tr>
                <td>LDAP Config to Use:</td>
                <td>
                    <select name="ldap" id="ldap">
                    <cfoutput query="getLDAP">
                        <option value="#id#">#LDAPEmailDomain# : #ldapServerFQDN# : #directoryLookupField#</option>
                    </cfoutput>    
                    </select>
                </td>
            </tr>
            <tr>
                <td>Group To Search:</td>
                <td><input type="text" name="groupname" id="groupname" value="" /></td>
            </tr>
            <tr>
                <td>Email To Search:</td>
                <td><input type="text" name="username" id="username" value="" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Find Users"></td>
            </tr>
        
        </table>
    </form>
</cfcase>
<cfcase value="2">
	<cfif isDefined('url.ldap')><cfset form.ldap=url.ldap></cfif>
    <cfif isDefined('url.groupname')><cfset form.groupname=url.groupname></cfif>
    <cfif isDefined('url.username')><cfset form.username=url.username></cfif>
    
	<cfif not isDefined('form.ldap') OR not isNumeric(form.ldap)><cflocation url="ldap_import.cfm?message=You must select LDAP settings." /></cfif>
    
	<cfquery dbtype="query" name="thisLDAP">
    SELECT * FROM getLDAP WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#form.ldap#" />
    </cfquery>
	<cfquery datasource="#application.dsn#" name="findUsers">
    SELECT * FROM users
    </cfquery>
    
	<!---(#directoryLookupField#=#lookupVal#)--->
    <cfldap action="query" name="findDN" server="#thisLDAP.LDAPServerFQDN#" start="#thisLDAP.LDAPBaseDN#" 
    filter="(&(objectclass=person))" attributes="memberOf,dn,cn,#thisLDAP.ldaplastnamefield#,#thisLDAP.ldapfirstnamefield#,mail" scope="subtree"
    username="#thisLDAP.LDAPAdminDN#" password="#thisLDAP.LDAPAdminPassword#" sort="#thisLDAP.ldaplastnamefield#,#thisLDAP.ldapfirstnamefield#"/>
    
    <cfif isDefined('form.groupname') AND form.groupname neq ''>
    	<cfquery dbtype="query" name="findDN">
        SELECT * FROM findDN WHERE memberOf LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#form.groupname#%" />
        </cfquery>
    </cfif>
    <cfif isDefined('form.username') AND form.username neq ''>
    	<cfquery dbtype="query" name="findDN">
        SELECT * FROM findDN WHERE mail LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#form.username#%" />
        </cfquery>
    </cfif>
    
    <cfif findDN.recordcount eq 0>
    	<p style="font-style:italic;">No users exist</p>
    <cfelse>
    	[<a href="javascript:void(0);" onclick="checkBoxSelectAll(document.getElementsByName('user'));">Select All</a>]
    	[<a href="javascript:void(0);" onclick="checkBoxDeSelectAll(document.getElementsByName('user'));">Select None</a>]
        <form method="post" action="ldap_import.cfm?step=3">
        	<cfoutput>
            	<input type="hidden" name="ldap" value="#form.ldap#" />
            	<input type="hidden" name="groupname" value="#form.groupname#" />
            	<input type="hidden" name="username" value="#form.username#" />
			</cfoutput>
            <h3>Select User(s)</h3>
            <table class="formTable">
                <cfoutput query="findDN">
                <cfset first = evaluate(thisLDAP.ldapfirstnamefield)>
                <cfset last = evaluate(thisLDAP.ldaplastnamefield)>
                <cfif first neq '' AND last neq '' and mail neq ''>
                <cfquery dbtype="query" name="findUser">
                SELECT * FROM findUsers WHERE email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#mail#" />
                </cfquery>
                
                <tr <cfif findUser.recordcount neq 0>class="disabled"</cfif>>
                    <td><input type="checkbox" name="user" value="#cn#:#mail#:#last#:#first#" id="user_#currentRow#" <cfif findUser.recordcount neq 0>disabled</cfif>></td>
                    <td><label for="user_#currentRow#">#last#</label></td>
                    <td><label for="user_#currentRow#">#first#</label></td>
                    <td><label for="user_#currentRow#">#mail#</label></td>
                </tr>
                </cfif>
                </cfoutput>
            </table>
            <h3>Settings</h3>
            <table class="formTable">
                        <tr>
            	<td>Timezone:</td>
                <td>
                    
                    <select name="timezone" id="timezone" required="yes">
                    	<cfoutput query="getTZ">
                        	<option value="#name#" <cfif name eq 'America/Chicago'>selected</cfif>>[UTC #numberformat(utcoffset,'+-00.00')#] #name#</option>
                        </cfoutput>
                    </select>
                    
                </td>
            </tr>
            <tr>
            	<td>Admin (Admin Screens):</td>
                <td>
                	<input type="radio" name="admin" value="1"  id="admin_1"> <label for="admin_1">Yes</label>
                	<input type="radio" name="admin" value="0" checked id="admin_0"> <label for="admin_0">No</label>
                </td>
            </tr>
            <tr>
            	<td style="vertical-align:top;">Allowed Branches:</td>
                <td>
                <cfoutput query="branches">
                	<input type="checkbox" name="allowedBranch" value="#name#" id="allowedBranch_#id#"> <label for="allowedBranch_#id#">#name#</label><br>
                </cfoutput>
                </td>
            </tr>
            <tr>
            	<td style="vertical-align:top;">Allowed Transactions:</td>
                <td>
                <cfoutput query="tasks">
                	<input type="checkbox" name="allowedTasks" value="#id#" id="allowedTasks_#id#"> <label for="allowedTasks_#id#">#name#</label><br>
                </cfoutput>
                </td>
            </tr>
            <tr>
            	<td>Default Branch:</td>
                <td>
                	
                    <select name="defaultBranch" id="defaultBranch" required="yes">
                    	<option value="">--Select--</option>
                    	<cfoutput query="branches">
                        	<option value="#name#">#name#</option>
                        </cfoutput>
                    </select>
                </td>
            </tr>
            
            </table><br>
            <input type="submit" value="Import Users">
    	</form>
    </cfif>
    
</cfcase>
<cfcase value="3">
	<cfif not isDefined('form.user') OR form.user eq ''><cflocation url="ldap_import.cfm?step=2&ldap=#form.ldap#&groupname=#form.groupname#&username=#form.username#&message=You must select 1 or more users"></cfif>
	<cfif not isDefined('form.admin') OR form.admin eq ''><cflocation url="ldap_import.cfm?step=2&ldap=#form.ldap#&groupname=#form.groupname#&username=#form.username#&message=You must select if the users will be admins"></cfif>
	<cfif not isDefined('form.timezone') OR form.timezone eq ''><cflocation url="ldap_import.cfm?step=2&ldap=#form.ldap#&groupname=#form.groupname#&username=#form.username#&message=You must select the timezone for new users"></cfif>
	
	<cfloop list="#form.user#" index="l">
    	<cfif listLen(l,":") eq 4>
			<cfset cn=listGetAt(l,1,":")>
            <cfset email=listGetAt(l,2,":")>
            <cfset last=listGetAt(l,3,":")>
            <cfset first=listGetAt(l,4,":")>
            
        	<cfquery datasource="#application.dsn#" name="import">
            INSERT INTO users(email, firstname, lastname, timezone, status, admin,defaultBranch,allowedBranch,allowedTasks)
            VALUES(
            	<cfqueryparam cfsqltype="cf_sql_varchar" value="#email#" />,
            	<cfqueryparam cfsqltype="cf_sql_varchar" value="#first#" />,
            	<cfqueryparam cfsqltype="cf_sql_varchar" value="#last#" />,
            	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.timezone#" />,
                'A',
                <cfqueryparam cfsqltype="cf_sql_bit" value="#form.admin#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.defaultBranch#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.allowedBranch#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.allowedTasks#" />
            )
            </cfquery>
            
        </cfif>
        
    </cfloop>
    <cflocation url="index.cfm?Message=Users Imported.">
</cfcase>
</cfswitch>

</cfif>
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>