<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>
<cfif isDefined('url.action') AND url.action eq 'delete'>
	<cfif not isDefined('url.id') OR not isNumeric(url.id)><cflocation url="ldap.cfm?message=Invalid deletion." /></cfif>
    <cfquery datasource="#application.dsn#" name="del">
    DELETE FROM server_ldap WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.id#" />
    </cfquery>
    <cflocation url="ldap.cfm?message=Configuration removed" />
</cfif>

<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: LDAP List</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" -->
<h2>System Settings: LDAP Configuration</h2>

<a href="index.cfm">Back to Admin Main</a><br><br>
<p style="font-size:1em;line-height:1.45em;">LDAP configurations are used to integrate your corporate directory with SendIt.  You may have as many LDAP configurations as necessary.  Each one will be used per the domain name
   if the user enters that domain as their e-mail address.  It's important to note that using many LDAP configurations or remote LDAP services can cause performance issues.  You
   should also ensure that there is a clear firewall path between the SendIt server and your LDAP server.</p>
   
<a href="ldap_add.cfm">Add New LDAP Configuration</a><br><br>
<cfquery datasource="#application.dsn#" name="getLDAP">
SELECT * FROM server_ldap
</cfquery>
<cfif getLDAP.recordcount eq 0>
<div style="font-style:italic;">There are no LDAP configurations defined.</div>
<cfelse>
<table class="messageTable" >
    	<tr>
        	<th>Email Domain</th>
			<th>Server</th>
			<th>Base DN</th>
			<th>Admin DN</th>
			<th>Lookup Field</th>
			<th>Email/User</th>
            <th>&nbsp;</th>
        </tr>
        <cfoutput query="getLDAP">
        	<tr>
            	<td>#LDAPEmailDomain#</td>
                <td>#LDAPServerFQDN#</td>
                <td>#LDAPBaseDN#</td>
                <td>#LDAPAdminDN#</td>
                <td>#directoryLookupField#</td>
                <td>#EmailOrUser#</td>
                <td>
                	<a href="ldap_edit.cfm?id=#id#"><img src="../Images/edit.gif" border="0" alt="Edit"></a>
                    &nbsp;
                	<a href="ldap.cfm?id=#id#&action=delete" onclick="return confirm('Are you sure you want to delete this?');"><img src="../Images/delete.gif" border="0" alt="delete"></a>
                </td>
             </tr>
        </cfoutput>
</table>
</cfif>
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>