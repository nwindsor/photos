<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfif isDefined('url.action')>
	<cfswitch expression="#url.action#">
        <cfcase value="insert">
        	
			<cfif form.password1 neq ''>
            	<cfif form.password1 neq form.password2>
                    <cflocation url="user_add.cfm?message=Passwords dont match" />
                </cfif>
                <cfif len(form.password1) lt 6>
                    <cflocation url="user_add.cfm?message=Passwords must be at least 6 characters" />
                </cfif>
                <cfif len(form.password1) gt 25>
                    <cflocation url="user_add.cfm?message=Passwords must must be less than 25 characters" />
                </cfif>
            </cfif>
			
			<cfif not isDefined('form.allowedTasks')><cflocation url="user_add.cfm?message=User must be granted at least one function"></cfif>
            
            <cfquery datasource="#application.dsn#" name="updateUser">
            insert into users(admin,history,firstname,lastname,timezone,password,email,status,jdeUser,keyFobID,defaultBranch,allowedBranch,allowedTasks)
            VALUES(<cfqueryparam cfsqltype="cf_sql_bit" value="#form.admin#" />
            	,<cfqueryparam cfsqltype="cf_sql_bit" value="#form.history#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.firstname#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.lastname#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.timezone#" />
            	,<cfif form.password1 eq ''>NULL<cfelse><cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(form.password1 & application.salt)#" /></cfif>
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,'A'
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.jdeUser#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.keyFobID#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.defaultBranch#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.allowedBranch#" />
                ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.allowedTasks#" />
                )
                
        	</cfquery>
        
            <cflocation url="user_list.cfm?message=User Added" />
        </cfcase>    
    
    </cfswitch>
	

</cfif>

<cfquery datasource="#application.dsn#" name="getTZ">
SELECT * from timezones
WHERE
	name NOT LIKE 'Etc%' AND
    name NOT LIKE 'System%'
ORDER BY utcoffset, name
</cfquery>

<cfinvoke component="list" method="list_data" list_id="68" returnVariable="branches"></cfinvoke>
<cfinvoke component="list" method="list_data" list_id="73" returnVariable="tasks"></cfinvoke>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Add User</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" -->
<h2>Viewing User</h2>
 	<a href="user_list.cfm">Search Again</a> | <a href="index.cfm">Admin Main</a><br><br>
 	<cfoutput>
    	<form method="post" action="user_add.cfm?action=insert" autocomplete="off" />
        <table class="formTable">
            <tr>
            	<td>E-mail:</td>
                <td><input type="text" name="email" id="email" maxlength="100" style="width:150px;" value="" required autocomplete="off"/></td>
            </tr>
        	<tr>
            	<td>First Name:</td>
                <td><input type="text" name="firstname" id="firstname" maxlength="50" style="width:150px;" value="" required autocomplete="off" /></td>
            </tr>
        	<tr>
            	<td>Last Name:</td>
                <td><input type="text" name="lastname" id="lastname" maxlength="50" style="width:150px;" value="" required autocomplete="off" /></td>
            </tr>
            <tr>
            	<td>Timezone:</td>
                <td>
                    
                    <select name="timezone" id="timezone" required="yes">
                    	<cfoutput query="getTZ">
                        	<option value="#name#" <cfif name eq 'America/Chicago'>selected</cfif>>[UTC #numberformat(utcoffset,'+-00.00')#] #name#</option>
                        </cfoutput>
                    </select>
                    
                </td>
            </tr>
            <tr>
            	<td>Admin (Admin Screens):</td>
                <td>
                	<input type="radio" name="admin" value="1"  id="admin_1"> <label for="admin_1">Yes</label>
                	<input type="radio" name="admin" value="0" checked id="admin_0"> <label for="admin_0">No</label>
                </td>
            </tr>
            <tr>
            	<td>Supervisor:</td>
                <td>
                	<input type="radio" name="history" value="1"  id="history_1"> <label for="history_1">Yes</label>
                	<input type="radio" name="history" value="0" checked id="history_0"> <label for="history_0">No</label>
                </td>
            </tr>
            <tr>
            	<td style="vertical-align:top;">Allowed Branches:</td>
                <td>
                <cfoutput query="branches">
                	<input type="checkbox" name="allowedBranch" value="#name#" id="allowedBranch_#id#"> <label for="allowedBranch_#id#">#name#</label><br>
                </cfoutput>
                </td>
            </tr>
            <tr>
            	<td>Default Branch:</td>
                <td>
                	
                    <select name="defaultBranch" id="defaultBranch" required="yes">
                    	<option value="">--Select--</option>
                    	<cfoutput query="branches">
                        	<option value="#name#">#name#</option>
                        </cfoutput>
                    </select>
                </td>
            </tr>
            <tr>
            	<td style="vertical-align:top;">Allowed Transactions:</td>
                <td>
                <cfoutput query="tasks">
                	<input type="checkbox" name="allowedTasks" value="#id#" id="allowedTasks_#id#"> <label for="allowedTasks_#id#">#name#</label><br>
                </cfoutput>
                </td>
            </tr>
            <tr>
            	<td>JDE User ID:</td>
                <td>
                <input type="text" name="jdeUser" id="jdeUser" maxlength="100" style="width:150px;" autocomplete="new-password" required />
                </td>
            </tr>
            <tr>
            	<td>Key FOB ID:</td>
                <td>
                <input type="text" name="keyFobID" id="keyFobID" maxlength="100" style="width:150px;" autocomplete="new-password" required />
                </td>
            </tr>
           
        	<tr>
            	<td>New Password:</td>
                <td><input type="password" style="width:150px;" name="password1" value="" maxlength="15"  autocomplete="new-password"/> <span style="font-style:italic;font-size:.85em;">Leave blank for no change</span></td>
            </tr>
            <tr>
            	<td>New Password Repeat:</td>
                <td><input type="password" style="width:150px;" name="password2" value="" maxlength="15"  autocomplete="new-password" /></td>
            </tr>
            <tr>
            	<td><input type="button" onclick="disableButton(this);" value="Add User"></td>
                <td style="text-align:right;"></td>
            </tr>
        </table>  
        	
        </form>
        
        
    </cfoutput>

<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>