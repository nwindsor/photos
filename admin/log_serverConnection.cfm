<cfparam name="url.ip" default="" />
<cfparam name="url.mapid" default="" />
<cfparam name="url.start_dt" default="#dateadd('d',-3,createDateTime(year(now()),month(now()),day(now()),0,0,0))#" />
<cfparam name="url.end_dt" default="#createDateTime(year(now()),month(now()),day(now()),23,59,59)#" />

<cfif isDefined('url.start_tm') AND url.start_tm neq ''>
	<cfset url.start_dt = dateFormat(url.start_dt,'yyyy-mm-dd') & ' ' & url.start_tm>
</cfif>
<cfif isDefined('url.end_tm') AND url.end_tm neq ''>
	<cfset url.end_dt = dateFormat(url.end_dt,'yyyy-mm-dd') & ' ' & url.end_tm>
</cfif>

<cfif isDate(url.start_dt) AND isDate(url.end_dt) AND datecompare(url.start_dt,url.end_dt) eq 1>
	<cfset tmp=url.start_dt>
    <cfset url.start_dt=url.end_dt>
    <cfset url.end_dt=tmp>
</cfif>

<cfinvoke component="list" method="list_data" list_id="83" returnVariable="maps"></cfinvoke>
<cfquery datasource="#application.dsn#" name="getIPs">
SELECT DISTINCT l.ip,u.firstname,u.lastname
FROM log_connectionRate l
	INNER JOIN users u ON (l.userid=u.id)
ORDER BY l.ip
</cfquery>

<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Warehouse</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>Warehouse - Device Connection Rate</h2>
<p>This is experimental. Only 24 hours of data is maintained potentially now, and GPS/location is inaccurate in the building. Another method may be more valuable.</p>
<cfoutput>
<form method="get" action="log_serverConnection.cfm">
<table class="formTable">
	<tr>
    	<td>IP:</td>
        <td>
        	<select name="ip" id="ip">
            	<option value="">-Select</option>
                <cfloop query="getIPs">
                	<option value="#ip#" <cfif ip eq url.ip>selected</cfif>>#ip# - #firstname# #lastname#</option>
                </cfloop>
            </select>
        </td>
    </tr>
    <tr>	
    	<td>Map:</td>
        <td>
        	<select name="mapid" id="mapid">
            	<option value="">-Select</option>
            	<cfoutput query="maps">
                	<option value="#id#" <cfif url.mapid eq id>selected</cfif>>#name#</option>
                </cfoutput>
            </select>
        </td>
    </tr>
    <tr>
    	<td>From:</td>
        <td><input type="text" class="datefield" name="start_dt" value="#dateformat(url.start_dt,'mm/dd/yyyy')#" style="width:100px;" maxlength="10" /> <input type="text" name="start_tm" id="start_tm" style="width:65px;" value="#timeformat(url.start_dt,'hh:mm tt')#" placeholder="hh:mm tt" /></td>
    </tr>
    <tr>
    	<td>To:</td>
        <td><input type="text" class="datefield" name="end_dt" value="#dateformat(url.end_dt,'mm/dd/yyyy')#" style="width:100px;" maxlength="10" />  <input type="text" name="end_tm" id="end_tm" style="width:65px;" value="#timeformat(url.end_dt,'hh:mm tt')#" placeholder="hh:mm tt" /></td>
    </tr>
    
	<tr>
    	<td colspan="2"><input type="button" value="Show Result" onclick="this.value='Working...';this.form.submit();this.disabled=true;"></td>
    </tr>
</table>
</form>
</cfoutput>

<cfif isDefined('url.ip') AND isNumeric(url.mapid)>

<cfquery datasource="#application.dsn#" name="getLog">
SELECT * FROM log_connectionRate WHERE ip=<cfqueryparam cfsqltype="cf_sql_varchar" value="#url.ip#" />
</cfquery>


<h2>Geolocation-Based Heatmap of Server Response</h2>
<p>Response times are averaged within a box of coordinates sized based on the divison of the facility into the number of width/height boxes indicated on the "map setup".</p>

<!--- AFSI US rectangle coordinates--->
<cfquery dbtype="query" name="getMapInfo">
SELECT * FROM maps WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.mapid#" />
</cfquery>

<cfset minLong=listGetAt(getMapInfo.description,1)>
<cfset maxLong=listGetAt(getMapInfo.description,2)>
<cfset minLat=listGetAt(getMapInfo.description,3)>
<cfset maxLat=listGetAt(getMapInfo.description,4)>

<cfset boxHeight=listGetAt(getMapInfo.description,5)>
<cfset boxWidth=listGetAt(getMapInfo.description,6)>


<!--- creates a grid for calculating averages, the space is roughly a 2:3 aspect ratio, so to make squares instead of rectangles we do irregular amounts which are stored on the map setup now --->
<cfset latPerBox=(maxLat-minLat)/boxHeight>
<cfset longPerBox=(maxLong-minLong)/boxWidth>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = google.visualization.arrayToDataTable([
        ['Average Response', 'Longitude', 'Latitude', 'Response Quality', 'Response Factor'],
		
		<cfoutput>
<cfset counter=0>
<cfloop from="#boxHeight#" to="1" index="y" step="-1">

	<cfloop from="1" to="#boxWidth#" index="x" step="1">
    
    	<cfset startLong=minLong+((x-1)*longPerBox)>
    	<cfset endLong=minLong+(x*longPerBox)>
    	<cfset startLat=minLat+((y-1)*latPerBox)>
    	<cfset endLat=minLat+(y*latPerBox)>
        <cfquery dbtype="query" name="getVal">
        SELECT avg(responsetime) resp, count(*) polls
        FROM getLog
        WHERE latitude BETWEEN <cfif startLat lt endLat>'#startLat#' AND '#endLat#'<cfelse>'#endLat#' AND '#startLat#'</cfif>
        	  AND
              longitude BETWEEN <cfif startLong lt endLong>'#startLong#' AND '#endLong#'<cfelse>'#endLong#' AND '#startLong#'</cfif>
			  AND attempt_ts BETWEEN 
							<cfqueryparam cfsqltype="cf_sql_timestamp" value="#castToUTC(url.start_dt,session.timezone)#" /> AND
							<cfqueryparam cfsqltype="cf_sql_timestamp" value="#castToUTC(url.end_dt,session.timezone)#" />
        </cfquery>
        <cfset curClass="">
        <cfif isNumeric(getVal.resp)>
        	<cfif getVal.resp gt 750><cfset curClass='red'><cfset bubbleLabel=">750ms">
        	<cfelseif getVal.resp gt 150><cfset curClass='orange'><cfset bubbleLabel=">150ms <=750ms">
            <cfelseif getVal.resp gt 85><cfset curClass='yellow'><cfset bubbleLabel=">85ms <=150ms">
            <cfelse><cfset curClass='green'><cfset bubbleLabel="<=85ms"></cfif>
        </cfif>
        
<!---        <div class="graphBox #curClass#" title="Long: #startLong# thru #endLong# by Lat: #startLat# thru #endLat#"><cfif isNumeric(getVal.resp)>#getVal.polls#|#getVal.resp#<cfelse>&nbsp;</cfif></div>
   --->     
        <cfif getVal.resp neq ''>
			<cfif counter neq 0>,</cfif><cfset counter=counter+1>['#getVal.Polls#|#getVal.resp#',#startLong*10000000#,#startLat*10000000#,'#bubbleLabel#',#1/getVal.resp*1000#]
		</cfif>
    </cfloop>

</cfloop>
</cfoutput>

      ]);

      var options = {
		//colors: ['yellow','green','orange','red'],
		series: {
			
			   '<=85ms': {color:'green',visibleInLegend:true},
			   '>85ms <=150ms': {color:'yellow',visibleInLegend:true},
			   '>150ms <=750ms': {color:'orange',visibleInLegend:true},
			   '>750ms': {color:'red',visibleInLegend:true}
			
		},
		<!---
		colorAxis: {
			minValue:1,
			maxValue:10,
			colors: ['red','orange','yellow','green']/*,
			values: ['<=100ms','>100ms <=250ms','>250ms <=1000ms','>1000ms']*/
		},--->
        //title: 'Geo-Based Heatmap of Server Response Time',
        <cfoutput>
		hAxis: {title: 'Longitude',  viewWindow:{max: #maxLong*10000000#,min:#minLong*10000000#}, gridlines:{count:#boxheight#}},
        vAxis: {title: 'Latititude', viewWindow:{max: #maxLat*10000000#,min:#minLat*10000000#}, gridlines:{count:#boxwidth#}},
		</cfoutput>
		backgroundColor:{
			stroke: '#666'
		},
		sizeAxis: {
				minValue:1,
				maxValue:30,
				minSize:10,
				maxSize:20,
				sortBubblesBySize: false
		},
		bubble: {
			opacity: .6,
			textStyle: {fontSize:7}	
		},
		legend: {
			position: 'none'
		}
      };
      var chart = new google.visualization.BubbleChart(document.getElementById('series_chart_div'));
      chart.draw(data, options);
    }
    </script>
    <cfoutput><div id="series_chart_div" style="width: 1200px; height: #1200/boxheight*boxwidth#px;"></div></cfoutput>
    
    <!--- now a map --->
    
    <h2>Path Taken</h2>
    <p>This path is a maximum of 500 points. Paths longer than 500 points are smoothed to 500 points by skipping every x point. This ensures the Google map API isn't overwhelmed by plotted points.</p>
    
    <div id="map" style="width:900px;height:500px;border:1px solid #666;"></div>
    <script>

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          <cfoutput>center: {lat: #(minLat+maxLat)/2#, lng: #(minLong+maxLong)/2#},</cfoutput>
          mapTypeId: google.maps.MapTypeId.SATELLITE
        });
		
	    map.setTilt(45);
        
		var flightPlanCoordinates = [
		  <cfset modulo=1>
		  <cfif getLog.recordcount gt 500><cfset modulo=int(getLog.recordcount/500)></cfif>
		  <cfset lastLat=0>
		  <cfset lastLong=0>
		  <cfoutput query="getLog">
		    <cfif currentRow neq 1>,</cfif>
		  	<cfif currentRow eq 1 OR CurrentRow mod modulo eq 0>
				{lat: #latitude#, lng: #longitude#}<cfset lastLat=latitude><cfset lastLong=longitude>
			</cfif>
		  </cfoutput>
        ];
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          //geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });
		var marker = new google.maps.Marker({
			<cfoutput>position: {lat: #getLog.latitude#, lng: #getLog.longitude#},</cfoutput>
			map: map,
			label: 'A',
			title: 'Start'
		  });
		var marker = new google.maps.Marker({
			<cfoutput>position: {lat: #lastLat#, lng: #lastLong#},</cfoutput>
			map: map,
			label: 'B',
			title: 'Finish'
		  });

        flightPath.setMap(map);
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwHMeatUojuxBGKBBHiYFJ2I_bp1ejaYY&signed_in=true&callback=initMap">
    </script>
</cfif>  
  
  
  
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>