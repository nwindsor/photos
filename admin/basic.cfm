<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfif isDefined('url.action') AND url.action eq 'update'>
	       
    <cfquery datasource="#application.dsn#" name="updSys">
    UPDATE server_settings
    SET
    	domainname=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.domainname#" />,
        emaildomain=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.emaildomain#" />,
        sitename=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.sitename#" />,
        companyname=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.companyname#" />,
        jdedsn=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.jdedsn#" />,
        timezone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.timezone#" />,
        logRetentionDays=<cfqueryparam cfsqltype="cf_sql_bigint" value="#form.logRetentionDays#" />,
        adminExceptionEmail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.adminExceptionEmail#" />,
        systemEmail=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.systemEmail#" />,
        erpLogin=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.erpLogin#" />,
        erpPassword=<cfqueryparam cfsqltype="cf_sql_varchar" value="#encrypt(form.erpPassword,application.encryption_key)#" />,
        <!---sourceCategoryOn=<cfqueryparam cfsqltype="cf_sql_bit" value="#form.sourceCategoryOn#" />,--->
        welcomeMessage=<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#form.welcomeMessage#" />,
        logConnectionRate=<cfqueryparam cfsqltype="cf_sql_bit" value="#form.logConnectionRate#" />,
        rfidAuth=<cfqueryparam cfsqltype="cf_sql_bit" value="#form.rfidAuth#" />
    </cfquery>
    
    <cfinvoke component="../useful" method="setServerSettings" />
    
	<cflocation url="index.cfm?message=Basic Settings saved" />
</cfif>

<cfquery datasource="#application.dsn#" name="getSettings">
SELECT * FROM server_settings
</cfquery>


<cfquery datasource="#application.dsn#" name="getTZ">
SELECT * from timezones
WHERE
	name NOT LIKE 'Etc%' AND
    name NOT LIKE 'System%'
ORDER BY utcoffset, name
</cfquery>

<cfinvoke component="list" method="list_data" list_id="2" returnVariable="dsns"></cfinvoke>

<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Scrap Dashboard</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<script language="javascript">

function toggleVisible(){
	
	if($('#erpPassword').attr("type") == "password"){
		$('#erpPassword').attr("type","text");	
	}
	else{
		$('#erpPassword').attr("type","password");	
	}
}	

</script>

<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>System Settings: Basic Settings</h2>
<a href="index.cfm">Back to Admin Main</a>
<br><br>
<cfoutput>
<form method="post" action="basic.cfm?action=update">
<input style="display:none">
<input type="password" style="display:none">
<table class="formTable">
	<tr>
    	<td>Scrap Fully Qualified Domain Name:<br><span style="font-size:.84em;">(ex: scrap.mydomain.com)</span></td>
        <td>https://<input type="text" name="domainname" value="#getSettings.domainname#" maxlength="255" style="width:200px;" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Email messages will contain links using this address to get back to the SysMon application.  It will ALWAYS be HTTPS, so please be sure to have port 443 setup on your web server with an appropriate SSL certificate assigned to your SysMon FQDN."></td>
    </tr>
	<tr>
    	<td>E-Mail Domain:<br><span style="font-size:.84em;">(ex: mydomain.com)</span></td>
        <td>@<input type="text" name="emaildomain" value="#getSettings.emaildomain#" maxlength="255" style="width:238px;" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Users in this domain will be able to send/receive globally.  Other email domains will only be allowed to reply or send messages to this domain."></td>
    </tr>
	<tr>
    	<td>Site Name:</td>
        <td><input type="text" name="sitename" value="#getSettings.sitename#" maxlength="255" style="width:250px;" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Your sitename is used in the page title as well as included in part of outbound e-mail notifications."></td>
    </tr>
	<tr>
    	<td>Company Name:</td>
        <td><input type="text" name="companyName" value="#getSettings.companyName#" maxlength="255" style="width:250px;" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Your company name is included in outbound e-mail notifications."></td>
    </tr>
	<tr>
    	<td>Admin Exception E-mail:</td>
        <td><input type="text" name="adminExceptionEmail" value="#getSettings.adminExceptionEmail#" maxlength="255" style="width:250px;" placeholder="root@domain.com" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="This e-mail will receive notification in the event that a user has an exception.  Leave blank for no exception messages."></td>
    </tr>
	<tr>
    	<td>System E-mail:</td>
        <td><input type="text" name="systemEmail" value="#getSettings.systemEmail#" maxlength="255" style="width:250px;" placeholder="root@domain.com" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="This e-mail is the from on system generated messages."></td>
    </tr>
    <tr>
    	<td>JDE DSN/Environment:</td>
        <td>
        	<select name="jdedsn" id="jdedsn" style="width:253px;">
            	<cfoutput query="dsns">
                	<option value="#name#" <cfif getSettings.jdedsn eq name>selected</cfif>>#listGetAt(description,1,';')#</option>
                </cfoutput>
            </select>
        </td>
    </tr>
	<tr>
    	<td>JDE AIS User:</td>
        <td><input type="text" name="erpLogin" id="erpLogin" maxlength="20" value="#getSettings.erpLogin#" style="width:250px;" autocomplete="off"></td>
    </tr>
	<tr>
    	<td>JDE AIS Password:</td>
        <cfset thisPass="">
        <cfif getSettings.erpPassword neq ''><cfset thisPass=decrypt(getSettings.erpPassword,application.encryption_key)></cfif>
        <td><input type="password" name="erpPassword" id="erpPassword" maxlength="20" value="#thisPass#" style="width:250px;" autocomplete="off"><div><a href="javascript:void(0);" onclick="toggleVisible();">Show/Hide Password</a></div></td>
    </tr>
	
	<!---
    <tr>
    	<td>Source/Category On:</td>
        <td>
        	<div style="display:inline-block;width:253px;">
        	<input type="radio" name="sourceCategoryOn" id="sourceCategoryOn_1" value="1" <cfif getSettings.sourceCategoryOn eq 1>checked</cfif>> <label for="sourceCategoryOn_1">Yes</label>
        	<input type="radio" name="sourceCategoryOn" id="sourceCategoryOn_0" value="0" <cfif getSettings.sourceCategoryOn eq 0>checked</cfif>> <label for="sourceCategoryOn_0">No</label>
        	</div>
        <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="During the recording of scrap the source/category field will be displayed and required or not.">
        </td>
    </tr>--->
	<tr>
    	<td>Store Connection Rate:</td>
        <td>
        <input type="radio" name="logConnectionRate" id="logConnectionRate1" value="1" <cfif getSettings.logConnectionRate eq 1>checked</cfif>> <label for="logConnectionRate1">Yes</label>
        <input type="radio" name="logConnectionRate" id="logConnectionRate0" value="0" <cfif getSettings.logConnectionRate eq 0>checked</cfif>> <label for="logConnectionRate0">No</label>
        <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="The system can log the server response times that are tested every 2-3 seconds for each user. Only 1 day of data is alllowed to build, but it can be used to detect network issues when combined with GPS enabled devices.">
        </td>
    </tr>
	<tr>
    	<td>RFID Authentication:</td>
        <td>
        <input type="radio" name="rfidAuth" id="rfidAuth1" value="1" <cfif getSettings.rfidAuth eq 1>checked</cfif>> <label for="rfidAuth1">Yes</label>
        <input type="radio" name="rfidAuth" id="rfidAuth0" value="0" <cfif getSettings.rfidAuth eq 0>checked</cfif>> <label for="rfidAuth0">No</label>
        <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="The system can allow RFID based authentication if desired. This is done when a compatible RFID reader is setup as keyboard on the device. It is only allowed on devices on subnets specified in the subnet admin list.">
        </td>
    </tr>
	<tr>
    	<td>Log Retention (days:)</td>
        <td><input type="text" name="logRetentionDays" value="#getSettings.logRetentionDays#" maxlength="4" style="width:250px;" placeholder="365" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="The number of days to retain system logs."></td>
    </tr>
    <tr>
        <td>Timezone:</td>
        <td>
            
            <select name="timezone" id="timezone" required="yes" style="width:255px;">
                <cfoutput query="getTZ">
                    <option value="#name#" <cfif getSettings.timezone eq name>selected</cfif>>[UTC #numberformat(utcoffset,'+-00.00')#] #name#</option>
                </cfoutput>
            </select>
            <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="The system timezone indicates how to convert displayed times for users.">
        </td>
    </tr>
	<tr>
    	<td colspan="2" style="vertical-align:top;">Welcome Message:</td>
    </tr>
    <tr>
        <td colspan="2">
        	<textarea name="welcomeMessage" style="width:550px;height:100px;">#getSettings.welcomeMessage#</textarea>
        </td>
    </tr>
</table>

                <input type="button" value="Save Settings" id="sbmt" name="sbmt" onclick="disableButton(this);"> 
</form>
</cfoutput>
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>