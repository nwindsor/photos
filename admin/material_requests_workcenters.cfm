<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfif isDefined('url.action') AND url.action eq 'update'>
  
  <cfset tm = createodbcdatetime(now())>
  <cfset usr = session.userid>
  <cfloop list ="#form.WCids#" index ="i">
    <cfif i neq ''>
      <cfquery datasource="#application.dsn#" name="updateWorkcenter">
      UPDATE material_request_workcenters
        SET 
          workcenter=<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="100" value="#evaluate('form.Workcenter_' & i)#"/>,
          operationSequence=<cfqueryparam cfsqltype="cf_sql_integer" maxlength="15" value="#evaluate('form.Sequence_' & i)#"/>,
          jdeShorthand=<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#evaluate('form.JDE_' & i)#"/>,
          productionLine=<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#evaluate('form.PL_' & i)#"/>,
          scrapAbbreviation=<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#evaluate('form.SA_' & i)#"/>,
          completionLocation=<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#evaluate('form.CL_' & i)#" />,
          modify_user=#usr#,
          modify_date=#tm#
        WHERE ID = <cfqueryparam cfsqltype="cf_sql_bigint" value="#i#">
      </cfquery>
    </cfif>
  </cfloop>
</cfif>

<cfif isDefined('form.Workcenter_new') AND form.Workcenter_new neq ''>
  
        <cfquery name = "getrecordcount" datasource="#application.dsn#">
        SELECT *
        FROM material_request_workcenters
      </cfquery>
      
      <cfquery datasource="#application.dsn#" name="insertWorkcenter">
        INSERT INTO material_request_workcenters(workcenter,operationSequence,jdeShorthand,productionLine,scrapAbbreviation,completionLocation,modify_user,modify_date,create_user,create_date)
        VALUES(
                <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="100" value="#form.Workcenter_new#" />,
                <cfqueryparam cfsqltype="cf_sql_integer" maxlength="10" value="#form.Sequence_new#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#form.JDE_new#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#form.PL_new#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#form.SA_new#" />,
                <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#form.CL_new#" />,
                #usr#,#tm#,#usr#,#tm#)         
      </cfquery>
  <cflocation url="material_requests_workcenters.cfm"/>
</cfif>

<cfif isDefined('url.action') AND url.action eq 'delete'>
	<cfif not isDefined('url.element_id')><cf_error message="No element defined." /></cfif>
    <cfquery datasource="#application.dsn#" name="deleteElm">
      DELETE FROM material_request_workcenters WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.element_id#" />
    </cfquery>
    <cflocation url="material_requests_workcenters.cfm"/>	
</cfif>

  
<cfquery name = "getMRW" datasource="#application.dsn#">
  SELECT id, workcenter, operationSequence,jdeShorthand,productionLine,scrapAbbreviation,completionLocation
  FROM material_request_workcenters
  ORDER BY productionLine,operationSequence asc ;
</cfquery>

<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Label Printer Setup</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<script language="javascript">
  function wcChanged(wcID){

  }

</script>

<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>System Settings: Material Request Workcenters</h2>

<cfoutput>
<a href="index.cfm">Back to Admin Main</a>

<form method="post" action="material_requests_workcenters.cfm?&action=update">
<input type="hidden" name="WCids" value="" />
<br><br>
<table border=1 class="formTable">
	<tr>
    	<th>ID</th>
        <th>Workcenter</th>
        <th>Sequence</th>
        <th>JDE Shorthand</th>
        <th>Production Line</th>
        <th>Scrap Abbreviation</th>
        <th>Completion Location</th>
        <th>&nbsp;</th>
    </tr>
        <cfloop query = "getMRW">
        <tr>
        	<td class="head"><input type="hidden" name="WCids" value="#id#" />#id#</td>
              <td><input name="Workcenter_#id#" type="text" maxlength="100" class="textbox" value="#Workcenter#" style="width:170px;"></td>
              <td><input name="Sequence_#id#" type="text" maxlength="15" class="textbox" value="#operationSequence#" style="width:80px;"></td>
              <td><input name="JDE_#id#" type="text" maxlength="15" class="textbox" value="#jdeShorthand#" style="width:120px;"></td>
              <td><input name="PL_#id#" type="text" maxlength="8" class="textbox" value="#productionLine#" style="width:120px;"></td>
              <td><input name="SA_#id#" type="text" maxlength="8" class="textbox" value="#scrapAbbreviation#" style="width:120px;"></td> 
              <td><input name="CL_#id#" type="text" maxlength="8" class="textbox" value="#completionLocation#" style="width:120px;"></td> 
              <td><a href="material_requests_workcenters.cfm?&action=delete&element_id=#id#" onClick="return confirm('Are you sure you want to delete this list element?');"><img src="../../images/delete.gif" alt="Delete" border="0" /></a></td>   
        </tr>
        </cfloop>
     
    	<tr>
        	<td class="head">[New]</td>
            <td><input name="Workcenter_new" id="elm_name_new" type="text" class="textbox" value="" style="width:170px;" maxlength="100"></td>
            <td><input name="Sequence_new" type="text" class="textbox" value="" style="width:80px;" maxlength="15"></td>
            <td><input name="JDE_new" type="text" class="textbox" value="" style="width:120px;" maxlength="1000"></td>
            <td><input name="PL_new" type="text" class="textbox" value="" style="width:120px;" maxlength="1000"></td>
            <td><input name="SA_new" type="text" class="textbox" value="" style="width:120px;" maxlength="1000"></td>
            <td><input name="CL_new" type="text" class="textbox" value="" style="width:120px;" maxlength="1000"></td>
            <td>&nbsp;</td>
        </tr>
</table>
<input type="submit" name="update" value="Update" /> <input type="reset" name="reset" value="Reset" /> <input type="button" onClick="window.location.href='index.cfm';" value="Cancel"/>
</form>
</cfoutput>
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>