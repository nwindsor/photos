<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>
<cfif not isDefined('url.id') OR not isNumeric(url.id)><cflocation url="ldap.cfm?Message=Invalid config specified" /></cfif>

<cfquery datasource="#application.dsn#" name="getConfig">
SELECT * FROM server_ldap WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.id#" />
</cfquery>

<cfquery datasource="#application.dsn#" name="getTZ">
SELECT * from timezones
WHERE
	name NOT LIKE 'Etc%' AND
    name NOT LIKE 'System%'
ORDER BY utcoffset, name
</cfquery>

<cfif isDefined('url.action') AND url.action eq 'update'>
	
    <cfquery datasource="#application.dsn#" name="upd">
    UPDATE server_ldap
    SET
    	LDAPEmailDomain=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.LDAPEmailDomain#" />,
        LDAPBaseDN=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.LDAPBaseDN#" />,
        LDAPServerFQDN=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.LDAPServerFQDN#" />,
        LDAPAdminDN=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.LDAPAdminDN#" />,
        LDAPAdminPassword=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.LDAPAdminPassword#" />,
        directoryLookupField=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.directoryLookupField#" />,
        EmailOrUser=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.EmailOrUser#" />,
        LDAPFirstNameField=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.LDAPFirstNameField#" />,
        LDAPLastNameField=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.LDAPLastNameField#" />,
        memberOfDN=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.memberOfDN#" />,
        defaultTimezone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.defaultTimezone#" />
        
    WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.id#" />
    </cfquery>
    <cfinvoke component="../useful" method="setServerSettings" />
	<cflocation url="ldap.cfm?message=LDAP Configuration Saved" />
</cfif><!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: LDAP Settings Edit</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" -->
<h2>Edit LDAP Configuration</h2>

<a href="ldap.cfm">Back to LDAP List</a> | <a href="index.cfm">Back to Admin Main</a><br><br>

<cfoutput>
<form method="post" action="ldap_edit.cfm?id=#url.id#&action=update">
<table class="formTable">
	<tr>
    	<td>E-mail Domain</td>
        <td><input type="text" name="LDAPEmailDomain" id="LDAPEmailDomain" value="#getConfig.LDAPEmailDomain#" style="width:350px;" maxlength="255" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Users whose email address matches this domain will be have authentication attempted against this configuration."></td>
    </tr>
	<tr>
    	<td>LDAP Server</td>
        <td><input type="text" name="LDAPServerFQDN" id="LDAPServerFQDN" value="#getConfig.LDAPServerFQDN#" style="width:350px;" maxlength="255" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Address of your LDAP server.  It may be a FQDN or IP address."></td>
    </tr>
	<tr>
    	<td>Base Search DN</td>
        <td><input type="text" name="LDAPBaseDN" id="LDAPBaseDN" value="#getConfig.LDAPBaseDN#" style="width:350px;" maxlength="255" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="This is the starting OU within your directory where you house user accounts.  No accounts above this directory will be found."></td>
    </tr>
	<tr>
    	<td>LDAP Admin DN</td>
        <td><input type="text" name="LDAPAdminDN" id="LDAPAdminDN" value="#getConfig.LDAPAdminDN#" style="width:350px;" maxlength="255" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="DN of a user account authorized to perform searches on the directory."></td>
    </tr>
	<tr>
    	<td>LDAP Admin Password</td>
        <td><input type="text" name="LDAPAdminpassword" id="LDAPAdminpassword" value="#getConfig.LDAPAdminpassword#" style="width:350px;" maxlength="255" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Password of Admin account.  Stored in cleartext in the DB, do not use your main domain admin!"></td>
    </tr>
	<tr>
    	<td>LDAP Directory Lookup Field</td>
        <td><input type="text" name="directoryLookupField" id="directoryLookupField" value="#getConfig.directoryLookupField#" style="width:350px;" maxlength="255" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="The attribute youll be comparing the user account or email address to.  On windows this is mail or sAMAccountName"></td>
    </tr>
	<tr>
    	<td>Lookup Based On</td>
        <td>
		<div style="display:inline-block;width:354px;font-size:1em;">
        <input type="radio" name="EmailOrUser" value="email" id="EmailOrUser_email" <cfif getConfig.EmailOrUser eq 'email'>checked</cfif>> <label for="EmailOrUser_email">Full E-mail Address</label>
		<input type="radio" name="EmailOrUser" value="user" id="EmailOrUser_user" <cfif getConfig.EmailOrUser eq 'user'>checked</cfif>> <label for="EmailOrUser_user">User Account / Login</label>
        </div>
        <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="If email the directory search will use the entire email address entered by the user.  If user then the search will use only the characters before the @ symbol.">
        </td>
    </tr>
	<tr>
    	<td>LDAP Member Of Requirement (DN)</td>
        <td><input type="text" name="memberOfDN" id="memberOfDN" value="#getConfig.memberOfDN#" style="width:350px;" maxlength="1000" placeholder="CN=Domain Users,OU=Enterprise Wide Groups (Security),DC=mydomain,DC=com" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Domain users must be members of this group in order to login to SendIt w/domain credentials.  Leave this field blank for no membership requirement."></td>
    </tr>
	<tr>
    	<td>Directory Firstname Attribute</td>
        <td><input type="text" name="LDAPfirstnamefield" id="LDAPfirstnamefield" value="#getConfig.LDAPfirstnamefield#" style="width:350px;" maxlength="255" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="The field on the user account in your directory that contains the first name of the user.  Used in automatic account creation."></td>
    </tr>
	<tr>
    	<td>Directory Lastname Attribute</td>
        <td><input type="text" name="LDAPlastnamefield" id="LDAPlastnamefield"  value="#getConfig.LDAPlastnamefield#" style="width:350px;" maxlength="255" required/> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="The field on the user account in your directory that contains the last name of the user.  Used in automatic account creation."></td>
    </tr>
	<tr>
    	<td>Domain Default Timezone</td>
        <td>
            <select name="defaulttimezone" id="defaulttimezone" required="yes">
                <cfoutput query="getTZ">
                    <option value="#name#" <cfif name eq getConfig.defaulttimezone>selected</cfif>>[UTC #numberformat(utcoffset,'+-00.00')#] #name#</option>
                </cfoutput>
            </select>
        </td>
    </tr>
</table>
<input type="button" onclick="disableButton(this);" value="Save Configuration" />
</form>
</cfoutput>

<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>