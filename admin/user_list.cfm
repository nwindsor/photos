
<cfparam name="url.email" default="" />
<cfparam name="url.firstname" default="" />
<cfparam name="url.lastname" default="" />

<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfinvoke component="list" method="list_data" list_id="73" returnVariable="tasks"></cfinvoke>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Manage Users</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>User List</h2>

<a href="index.cfm">Back to Admin Main</a><br><br>

<div style="font-weight:700;font-size:1em;">Search</div>
<cfoutput>
<form method="get" action="user_list.cfm">
<input type="hidden" name="action" value="search">
<table class="formTable">
	
    <tr>
    	<td>E-mail:</td>
        <td><input type="text" name="email" id="email" value="#url.email#" style="width:150px;" maxlength="155" /></td>
    </tr>
    <tr>
    	<td>First Name:</td>
        <td><input type="text" name="firstname" id="firstname" value="#url.firstname#" style="width:150px;" maxlength="155" /></td>
    </tr>
    <tr>
    	<td>Last Name:</td>
        <td><input type="text" name="lastname" id="lastname" value="#url.lastname#" style="width:150px;" maxlength="155" /></td>
    </tr>
</table>
<input type="submit" value="Find Users">
</form>
</cfoutput>

<cfif isDefined('url.action') AND url.action eq 'search'>
	
   <p> <a href="user_add.cfm"><img src="../Images/plus.gif" border="0" alt="Add" class="plusButton" />  Add User</a></p>
    <cfquery datasource="#application.dsn#" name="getUsers">
    SELECT u.lastname, u.firstname, u.email, u.status, u.id, u.jdeuser, u.defaultbranch, u.allowedbranch, u.allowedTasks, u.history
    FROM users u
    WHERE
        0=0
        <cfif url.email neq ''>
            AND email LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#url.email#%" />
        </cfif>
        <cfif url.firstname neq ''>
            AND firstname LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#url.firstname#%" />
        </cfif>
        <cfif url.lastname neq ''>
            AND lastname LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#url.lastname#%" />
        </cfif>
    ORDER BY lastname, firstname, email
    </cfquery>
	<table class="messageTable" >
    	<tr>
        	<th>E-mail</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Status</th>
			<th>Supervisor</th>
			<th>JDE User</th>
			<th>Def BP</th>
			<th>All. BP</th>
			<th>Tasks</th>
        </tr>
        <cfoutput query="getUsers">
        	<tr onmouseover="$(this).addClass('highlight');" onmouseout="$(this).removeClass('highlight');">
            	<td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" ><a href="user_view.cfm?userid=#id#">#email#</a></td>
                <td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" >#firstname#</td>
                <td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" >#lastname#</td>
                <td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" >#status#</td>
                <td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" >#history#</td>
                <td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" >#jdeuser#</td>
                <td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" >#defaultbranch#</td>
                <td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" >#allowedbranch#</td>
                <td onclick="window.location.href='user_view.cfm?userid=#id#';" style="cursor:pointer;" >
				<cfloop list="#allowedTasks#" index="i">
					<cfquery dbtype="query" name="gettask">
					SELECT * FROM tasks WHERE id=#i#
					</cfquery>
					#getTask.name#<br>
				</cfloop>
				
				</td>
            </tr>
        </cfoutput>
	</table><br><br>
    <a href="user_add.cfm"><img src="../Images/plus.gif" border="0" alt="Add" class="plusButton" />  Add User</a>
<cfelse>
<p style="font-style:italic;">Search above to display users.</p>
</cfif>
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>