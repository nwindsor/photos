<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>
<cfif not isDefined('url.userid') OR not isNumeric(url.userid)><cflocation url="user_list.cfm?message=Invalid user." /></cfif>


<cfquery datasource="#application.dsn#" name="getUser">
SELECT * FROM users WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.userid#" />
</cfquery>

<cfif getUser.recordcount neq 1><cflocation url="user_list.cfm?message=Invalid user." /></cfif>

<cfif isDefined('url.action')>
	<cfswitch expression="#url.action#">
    	<cfcase value="clearlockout">
        	<cfquery datasource="#application.dsn#" name="clearLock">
            UPDATE users
            SET lockoutExpires=null,
                lockedOut=0
            WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.userid#" />
            </cfquery>
            <cflocation url="user_view.cfm?userid=#url.userid#&message=Lockout cleared" />
        </cfcase>
        <cfcase value="update">
        	
			<cfif form.password1 neq ''>
            	<cfif form.password1 neq form.password2>
                    <cflocation url="user_view.cfm?userid=#url.userid#&message=Passwords dont match" />
                </cfif>
                <cfif len(form.password1) lt 6>
                    <cflocation url="user_view.cfm?userid=#url.userid#&message=Passwords must be at least 6 characters" />
                </cfif>
                <cfif len(form.password1) gt 25>
                    <cflocation url="user_view.cfm?userid=#url.userid#&message=Passwords must must be less than 25 characters" />
                </cfif>
            </cfif>
            
            <cfquery datasource="#application.dsn#" name="updateUser">
            UPDATE users
            SET
            	admin=<cfqueryparam cfsqltype="cf_sql_bit" value="#form.admin#" />
            	,history=<cfqueryparam cfsqltype="cf_sql_bit" value="#form.history#" />
                ,firstname=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.firstname#" />
                ,lastname=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.lastname#" />
                ,status=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.status#" />
                ,timezone=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.timezone#" />
                ,jdeUser=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.jdeUser#" />
                ,keyFobID=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.keyFobID#" />
                ,allowedBranch=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.allowedBranch#" />
                ,allowedTasks=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.allowedTasks#" />
                ,defaultBranch=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.defaultBranch#" />
                
            	<cfif form.password1 neq ''>,password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(form.password1 & application.salt)#" /></cfif>
                
            WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.userid#" />
        	</cfquery>
        
            <cflocation url="user_list.cfm?userid=#url.userid#&message=User updated&action=search&email=&firstname=&lastname=" />
        </cfcase>
        <cfcase value="delete">
        	<cfif getUser.firstname eq 'root'><cflocation url="user_view.cfm?userid=#url.userid#&message=You cannot delete the root user." /></cfif>
            <cfif getUser.admin eq 1><cflocation url="user_view.cfm?userid=#url.userid#&message=You cannot delete an admin user.  Make them not an admin first." /></cfif>
        	
            <cftry>
            <cfquery datasource="#application.dsn#" name="delUser">
            DELETE FROM users WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.userid#" />
            </cfquery>
            
            <cflocation url="user_list.cfm?message=User deleted" />
            	<cfcatch type="any">
            		<cflocation url="user_view.cfm?userid=#url.userid#&message=User cannot be deleted.  They have records attached to them.  Please de-activate." />
                </cfcatch>
            </cftry>
        </cfcase>
    
    
    
    </cfswitch>
	

</cfif>

<cfquery datasource="#application.dsn#" name="getTZ">
SELECT * from timezones
WHERE
	name NOT LIKE 'Etc%' AND
    name NOT LIKE 'System%'
ORDER BY utcoffset, name
</cfquery>

<cfinvoke component="list" method="list_data" list_id="68" returnVariable="branches"></cfinvoke>
<cfinvoke component="list" method="list_data" list_id="73" returnVariable="tasks"></cfinvoke>

<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Edit User</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" -->	<h2>Viewing User</h2>
 	<a href="user_list.cfm">Search Again</a> | <a href="index.cfm">Admin Main</a><br><br>
 	<cfoutput>
    	<form method="post" action="user_view.cfm?userid=#url.userid#&action=update" autocomplete="off" />
        <table class="formTable">
            <tr>
            	<td>E-mail:</td>
                <td>#getUser.email#</td>
            </tr>
        	<tr>
            	<td>First Name:</td>
                <td><input type="text" name="firstname" id="firstname" value="#getUser.firstname#" maxlength="50" style="width:150px;" required /></td>
            </tr>
        	<tr>
            	<td>Last Name:</td>
                <td><input type="text" name="lastname" id="lastname" value="#getUser.lastname#" maxlength="50" style="width:150px;" required /></td>
            </tr>
            <tr>
            	<td>Timezone:</td>
                <td>
                    
                    <select name="timezone" id="timezone" required="yes">
                    	<cfoutput query="getTZ">
                        	<option value="#name#" <cfif name eq getUser.timezone>selected</cfif>>[UTC #numberformat(utcoffset,'+-00.00')#] #name#</option>
                        </cfoutput>
                    </select>
                    
                </td>
            </tr>
            
            <tr>
            	<td>Status:</td>
                <td>                
                	<input type="radio" name="status" value="A" <cfif getUser.status eq 'A'>checked</cfif> id="status_a"> <label for="status_a">Active</label>
                	<input type="radio" name="status" value="D" <cfif getUser.status eq 'D'>checked</cfif> id="status_d"> <label for="status_d">Deactivated</label>
                	<input type="radio" name="status" value="I" <cfif getUser.status eq 'I'>checked</cfif> id="status_i"> <label for="status_i">Initialized</label>
                </td>
            </tr>
            <tr>
            	<td>Admin (Admin Screens):</td>
                <td>
                	<input type="radio" name="admin" value="1" <cfif getUser.admin>checked</cfif> id="admin_1"> <label for="admin_1">Yes</label>
                	<input type="radio" name="admin" value="0" <cfif NOT getUser.admin>checked</cfif> id="admin_0"> <label for="admin_0">No</label>
                </td>
            </tr>
            <tr>
            	<td>Supervisor:</td>
                <td>
                	<input type="radio" name="history" value="1" <cfif getUser.history>checked</cfif> id="history_1"> <label for="history_1">Yes</label>
                	<input type="radio" name="history" value="0" <cfif NOT getUser.history>checked</cfif> id="history_0"> <label for="history_0">No</label>
                </td>
            </tr>
            <tr>
            	<td style="vertical-align:top;">Allowed Branches:</td>
                <td>
                <cfoutput query="branches">
                	<input type="checkbox" name="allowedBranch" value="#name#" id="allowedBranch_#id#" <cfif listFind(getUser.allowedBranch,name)>checked</cfif>> <label for="allowedBranch_#id#">#name#</label><br>
                </cfoutput>
                </td>
            </tr>
            <tr>
            	<td>Default Branch:</td>
                <td>
                	
                    <select name="defaultBranch" id="defaultBranch" required="yes">
                    	<option value="">--Select--</option>
                    	<cfoutput query="branches">
                        	<option value="#name#" <cfif name eq getUser.defaultBranch>selected</cfif>>#name#</option>
                        </cfoutput>
                    </select>
                </td>
            </tr>
            <tr>
            	<td style="vertical-align:top;">Allowed Transactions:</td>
                <td>
                <cfoutput query="tasks">
                	<input type="checkbox" name="allowedtasks" value="#id#" id="allowedtasks_#id#" <cfif listFind(getUser.allowedtasks,id)>checked</cfif>> <label for="allowedtasks_#id#">#name#</label><br>
                </cfoutput>
                </td>
            </tr>
            <tr>
            	<td>JDE User ID:</td>
                <td>
                <input type="text" name="jdeUser" id="jdeUser" value="#getUser.jdeUser#" maxlength="100" style="width:150px;" autocomplete="new-password" required />
                </td>
            </tr>
            <tr>
            	<td>Key FOB ID:</td>
                <td>
                <input type="text" name="keyFobID" id="keyFobID" value="#getUser.keyFobID#" maxlength="100" style="width:150px;" autocomplete="new-password" required />
                </td>
            </tr>
            <tr>
            	<td>Locked Out:</td>
                <td><cfif getUser.lockedOut eq 1>Yes #dateformat(getUser.lockoutExpires,'mm/dd/yyyy')# #timeformat(getUser.lockoutExpires,'hh:mm tt')# Server Time  <a href="user_view.cfm?userid=#url.userid#&action=clearlockout">[Clear]</a><cfelse>No</cfif></td>
            </tr>
        	<tr>
            	<td>New Password:</td>
                <td><input type="password" style="width:150px;" name="password1" value="" maxlength="15" autocomplete="new-password"/> <span style="font-style:italic;font-size:.85em;">Leave blank for no change</span></td>
            </tr>
            <tr>
            	<td>New Password Repeat:</td>
                <td><input type="password" style="width:150px;" name="password2" value="" maxlength="15" autocomplete="new-password" /></td>
            </tr>
            <tr>
            	<td><input type="button" onclick="disableButton(this);" value="Update User"></td>
                <td style="text-align:right;"><input type="button" value="Delete User" style="margin-left:100px;" onclick="if(confirm('Are you sure you want to delete this user?')==1){window.location.href='user_view.cfm?userid=#url.userid#&action=delete'}" /></td>
            </tr>
        </table>  
        	
        </form>
        
        
    </cfoutput>
    
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>