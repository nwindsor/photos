<cfparam name="url.start_dt" default="#dateadd('d',-30,createDateTime(year(now()),month(now()),day(now()),0,0,0))#" />
<cfparam name="url.end_dt" default="#createDateTime(year(now()),month(now()),day(now()),23,59,59)#" />

<cfif isDate(url.start_dt) AND isDate(url.end_dt) AND datecompare(url.start_dt,url.end_dt) eq 1>
	<cfset tmp=url.start_dt>
    <cfset url.start_dt=url.end_dt>
    <cfset url.end_dt=tmp>
</cfif>

<cfif isDate(url.end_dt)>
	<cfset url.end_dt = createdateTime(year(url.end_dt),month(url.end_dt),day(url.end_dt),23,59,59)>
</cfif>

<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfif isDefined('url.action') AND url.action eq 'runnow'>
	<cfinvoke component="../useful" method="cleanup" />
    <cflocation url="log_cleanup.cfm?message=Maintenance Complete" />
</cfif>	


<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: System Cleanup Log</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>Log Viewer: Cleanup/Maintenance/Reminder Log</h2>

<cfquery datasource="#application.dsn#" name="getLog">
SELECT * FROM log_taskrun
WHERE
	0=0
    <cfif isDate(url.start_dt) AND isDate(url.end_dt)>
    	AND attempt_ts BETWEEN
        	<cfqueryparam cfsqltype="cf_sql_timestamp" value="#castToUTC(url.start_dt,session.timezone)#" /> AND
        	<cfqueryparam cfsqltype="cf_sql_timestamp" value="#castToUTC(url.end_dt,session.timezone)#" />
    </cfif>
ORDER BY attempt_ts desc
</cfquery>
<a href="index.cfm">Back to Admin Main</a><br><br>

<div style="font-weight:700;font-size:1em;">Filters</div>
<cfoutput>
<form method="get" action="log_cleanup.cfm">
<table class="formTable">
	<tr>
    	<td>Date:</td>
        <td>
        	<input type="text" class="datefield" name="start_dt" value="#dateformat(url.start_dt,'mm/dd/yyyy')#" style="width:100px;" maxlength="10" /> -
        	<input type="text" class="datefield" name="end_dt" value="#dateformat(url.end_dt,'mm/dd/yyyy')#" style="width:100px;" maxlength="10" /> 
        </td>
    </tr>
    
</table>
<input type="submit" value="Apply Filter">
</form>
</cfoutput>
<br><br>
<table class="formTable">
<tr>
<td style="vertical-align:top;">
	<table class="messageTable" >
    	<tr>
        	<th>Timestamp</th>
        </tr>
        <cfoutput query="getLog">
        	<tr onmouseover="$(this).addClass('highlight');" onmouseout="$(this).removeClass('highlight');">
                <td>#dateFormat(castFromUTC(attempt_ts,'#session.timezone#'),'mm/dd/yyyy')# #timeformat(castFromUTC(attempt_ts,'#session.timezone#'),'hh:mm tt')#</td>
            </tr>
        </cfoutput>
	</table>
</td>
<td style="vertical-align:top;padding-left:40px;">
<a href="log_cleanup.cfm?action=runnow">Run Cleanup Tasks Now</a>
	<ul>
    	<li>Clears old logs</li>
    </ul>
</td>
</tr>
</table>
<br><br style="clear:both;">
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>