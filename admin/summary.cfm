<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfif isDefined('url.action') AND url.action eq 'reset'>

    
    <cfinvoke component="../useful" method="setServerSettings" />
    
	<cflocation url="index.cfm?message=Basic Settings reset" />
</cfif>


<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Settings Summary</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->


<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>System Settings: Settings Summary</h2>
<a href="index.cfm">Back to Admin Main</a>
<br><br>
<a href="summary.cfm?action=reset">Reset Server Settings</a>
<p>This page is a temporary place for outputting variables. For security it doesnt output unless the code is updated.</p>

<cfoutput>
<h3>Application</h3>
<!---<cfdump var="#application#">--->

<h3>Session</h3>
<!---<cfdump var="#session#">--->

</cfoutput>
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>