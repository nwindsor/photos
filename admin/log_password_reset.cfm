<cfparam name="url.start_dt" default="#dateadd('d',-30,createDateTime(year(now()),month(now()),day(now()),0,0,0))#" />
<cfparam name="url.end_dt" default="#createDateTime(year(now()),month(now()),day(now()),23,59,59)#" />
<cfparam name="url.success" default="0,1" />
<cfparam name="url.ip" default="" />
<cfparam name="url.email" default="" />


<cfif isDate(url.start_dt) AND isDate(url.end_dt) AND datecompare(url.start_dt,url.end_dt) eq 1>
	<cfset tmp=url.start_dt>
    <cfset url.start_dt=url.end_dt>
    <cfset url.end_dt=tmp>
</cfif>

<cfif isDate(url.end_dt)>
	<cfset url.end_dt = createdateTime(year(url.end_dt),month(url.end_dt),day(url.end_dt),23,59,59)>
</cfif>

<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Scrap Dashboard</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" -->

<cfquery datasource="#application.dsn#" name="getLog">
SELECT * FROM log_password_reset
WHERE
	0=0
    <cfif isDate(url.start_dt) AND isDate(url.end_dt)>
    	AND attempt_ts BETWEEN
        	<cfqueryparam cfsqltype="cf_sql_timestamp" value="#castToUTC(url.start_dt,session.timezone)#" /> AND
        	<cfqueryparam cfsqltype="cf_sql_timestamp" value="#castToUTC(url.end_dt,session.timezone)#" />
    </cfif>
    <cfif url.success neq ''>
    	AND success IN (<cfqueryparam cfsqltype="cf_sql_bit" value="#url.success#" list="yes" />)
    </cfif>
    <cfif url.ip neq ''>
    	AND ip LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#url.ip#%" />
    </cfif>
    <cfif url.email neq ''>
    	AND email LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#url.email#%" />
    </cfif>
ORDER BY attempt_ts desc
</cfquery>

<h2>Log Viewer: Password Reset Log</h2>

<a href="index.cfm">Back to Admin Main</a><br><br>

<div style="font-weight:700;font-size:1em;">Filters</div>
<cfoutput>
<form method="get" action="log_password_reset.cfm">
<table class="formTable">
	<tr>
    	<td>Date:</td>
        <td>
        	<input type="text" class="datefield" name="start_dt" value="#dateformat(url.start_dt,'mm/dd/yyyy')#" style="width:100px;" maxlength="10" /> -
        	<input type="text" class="datefield" name="end_dt" value="#dateformat(url.end_dt,'mm/dd/yyyy')#" style="width:100px;" maxlength="10" /> 
        </td>
    </tr>
    <tr>
    	<td>Success:</td>
        <td>
        	<input type="checkbox" name="success" value="1" id="success_1" <cfif listfind(url.success,1)>checked</cfif>> <label for="success_1">Yes</label>
        	<input type="checkbox" name="success" value="0" id="success_0" <cfif listfind(url.success,0)>checked</cfif>> <label for="success_0">No</label>
        </td>
    </tr>
    <tr>
    	<td>IP:</td>
        <td><input type="text" name="ip" id="ip" value="#url.ip#" style="width:150px;" maxlength="15" /></td>
    </tr>
    <tr>
    	<td>E-mail:</td>
        <td><input type="text" name="email" id="email" value="#url.email#" style="width:150px;" maxlength="155" /></td>
    </tr>
</table>
<input type="submit" value="Apply Filter">
</form>
</cfoutput>
<br><br>
	<table class="messageTable" >
    	<tr>
        	<th>E-mail</th>
            <th>Success</th>
            <th>IP Address</th>
            <th>Step</th>
            <th>Reason</th>
            <th>Timestamp</th>
        </tr>
        <cfoutput query="getLog">
        	<tr onmouseover="$(this).addClass('highlight');" onmouseout="$(this).removeClass('highlight');">
            	<td>#email#</td>
                <td>#success#</td>
                <td>#ip#</td>
                <td>#step#</td>
                <td>#reason#</td>
                <td>#dateFormat(castFromUTC(attempt_ts,'#session.timezone#'),'mm/dd/yyyy')# #timeformat(castFromUTC(attempt_ts,'#session.timezone#'),'hh:mm tt')#</td>
            </tr>
        </cfoutput>
	</table>

<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>