<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfif isDefined('url.action') AND url.action eq 'delete'>
    <cfif not isDefined('url.printer_id') OR not isNumeric(url.printer_id)><cflocation url="labelPrinters.cfm?message=Invalid printer" /></cfif>
    
    <cfquery datasource="#application.dsn#" name="delBranches">
    DELETE FROM label_printer_branches WHERE printer_id=<cfqueryparam cfsqltype="bigint" value="#url.printer_id#" />
    </cfquery>
    <cfquery datasource="#application.dsn#" name="delPrinter">
    DELETE FROM label_printers WHERE printer_id=<cfqueryparam cfsqltype="bigint" value="#url.printer_id#" />
    </cfquery>
    
    <cflocation url="labelPrinters.cfm?message=Printer removed" />
    
</cfif>
<cfif isDefined('url.action') AND url.action eq 'update'>
    
    <cfinclude template="/Includes/timezone.cfm">
    <cfset tm=CastToUTC(createODBCDateTime(now()),session.timezone)>
    <cfset usr=session.userid>
                
                
    <!--- update code --->
    
    <cfif isDefined('form.printerList') AND form.printerList neq ''>
        <cfloop list="#form.printerList#" index="i">
            <cfset nm=evaluate('form.printer_name_' & i) />
            <cfset dsc=evaluate('form.printer_description_' & i) />
            <cfset appid=evaluate('form.printer_labelAppID_' & i) />
            <cfif isDefined('form.printer_branch_' & i)><cfset brn=evaluate('form.printer_branch_' & i) /><cfelse><cfset brn=""/></cfif>
        
                
                <cfquery datasource="#application.dsn#" name="updatePrinter">
                UPDATE label_printers
                    SET
                    printer_name=<cfqueryparam cfsqltype="varchar" value="#nm#" />,
                    printer_description=<cfqueryparam cfsqltype="varchar" value="#dsc#" />,
                    printer_labelAppID=<cfqueryparam cfsqltype="varchar" value="#appid#" />,
                    modify_user=#usr#,
                    modify_date=#tm#
                    
                WHERE printer_id=<cfqueryparam cfsqltype="bigint" value="#i#" />
                </cfquery>
        
               <cfquery datasource="#application.dsn#" name="delBranches">
                DELETE FROM label_printer_branches WHERE printer_id=<cfqueryparam cfsqltype="bigint" value="#i#" />
                </cfquery>
                
                <cfif brn neq ''>
                    <cfloop list="#brn#" index="j">
                        <cfif isNumeric(i)>
                            <cfquery datasource="#application.dsn#" name="insertBranches">
                            INSERT INTO label_printer_branches(printer_id, branch_plant)
                            VALUES (
                                <cfqueryparam cfsqltype="bigint" value="#i#" />,
                                <cfqueryparam cfsqltype="bigint" value="#j#" />
                            )
                            </cfquery>
                        </cfif>
                    </cfloop>
                </cfif>
        </cfloop>
    </cfif>
    
    <!--- add code --->
    <cfif isDefined('form.printer_name_new') AND trim(form.printer_name_new) neq ''>
    
        <cftransaction>
            <cfquery datasource="#application.dsn#" name="insLabelPrinter">
            INSERT INTO label_printers(printer_name, printer_description, printer_labelAppID, modify_user, modify_date, create_user, create_date)
            VALUES(
                <cfqueryparam cfsqltype="varchar" value="#form.printer_name_new#" />,
                <cfqueryparam cfsqltype="varchar" value="#form.printer_description_new#" />,
                <cfqueryparam cfsqltype="bigint" value="#form.printer_labelAppID_new#" />,
                #usr#,
                #tm#,
                #usr#,
                #tm#
                )

            </cfquery>   
            <cfquery datasource="#application.dsn#" name="getNewPrinter">
            SELECT printer_id FROM label_printers WHERE

                create_user=#usr# AND
                create_date=#tm# AND
                printer_name=<cfqueryparam cfsqltype="varchar" value="#form.printer_name_new#" />

            </cfquery>

            <cfif isDefined('form.printer_branch_new') AND form.printer_branch_new neq ''>
                <cfloop list="#form.printer_branch_new#" index="i">
                    <cfif isNumeric(i)>
                        <cfquery datasource="#application.dsn#" name="insertBranches">
                        INSERT INTO label_printer_branches(printer_id, branch_plant)
                        VALUES (
                            <cfqueryparam cfsqltype="bigint" value="#getNewPrinter.printer_id#" />,
                            <cfqueryparam cfsqltype="bigint" value="#i#" />
                        )
                        </cfquery>
                    </cfif>
                </cfloop>
            </cfif>
        </cftransaction>
        
    
    </cfif>
    
<cflocation url="labelPrinters.cfm" />    

</cfif>
   
<cfinvoke component="list" method="list_data" list_id="68" returnVariable="branchplants"></cfinvoke>

<cfquery datasource="#application.dsn#" name="getPrinters">
    SELECT lp.*,branch_plant
    FROM label_printers lp
        LEFT JOIN label_printer_branches lpb ON (lp.printer_id=lpb.printer_id)
    ORDER BY lp.printer_name
</cfquery>
        
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Label Printer Setup</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<script language="javascript">


</script>

<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>System Settings: Label Printers</h2>
<a href="index.cfm">Back to Admin Main</a>
<br><br>
<cfoutput>
<form method="post" action="labelPrinters.cfm?action=update"  autocomplete="off">
    <input autocomplete="false" name="hidden" type="text" style="display:none;">
<input style="display:none">
<table class="formTable">
<tr>
    <th rowspan="1">ID</th>
    <th rowspan="1">Name / Barcode</th>
    <th rowspan="1">Description</th>
    <th rowspan="1">Label App ID</th>
    <th colspan="#branchplants.recordcount#">Enabled in Branch(es)</th>
    <th rowspan="1">Configuration</th>
</tr>
<!---<tr>
    <cfloop query="branchplants">
    <th>#name#</th>
    </cfloop>
</tr>--->
<cfquery dbtype="query" name="getPrinterList">
SELECT DISTINCT printer_id FROM getPrinters
</cfquery>
<input type="hidden" name="printerList" value="#valueList(getPrinterList.printer_id)#" />    
    
    <cfoutput query="getPrinters" group="printer_id">
    <tr>
        <td>#printer_id#</td>
        <cfquery dbtype="query" name="getSelectedBranches">SELECT branch_plant FROM getPrinters WHERE printer_id=#printer_id#</cfquery>
        <cfset selectedBranches=valueList(getSelectedBranches.branch_plant)>
        <td><input type="text" name="printer_name_#printer_id#" id="printer_name_#printer_id#" value="#printer_name#" maxlength="255" style="width:150px;" autocomplete="off" /></td>
        <td><input type="text" name="printer_description_#printer_id#" id="printer_description_#printer_id#" value="#printer_description#" maxlength="255" style="width:250px;" autocomplete="off"/></td>
        <td><input type="text" name="printer_labelappid_#printer_id#" id="printer_labelappid_#printer_id#" value="#printer_labelAppID#" maxlength="15" style="width:45px;" autocomplete="off"/></td>
   
        <cfloop query="branchplants">
            <td>
                <input type="checkbox" name="printer_branch_#printer_id#" id="printer_branch_#id#_#printer_id#" value="#id#" <cfif listFind(selectedBranches,id) neq 0>checked</cfif>> <label for="printer_branch_#id#_#printer_id#">#name#</label>
            </td>    
            
        </cfloop>
              <td><a href="printer_Config.cfm?printer_id=#printer_id#">Configure Location</a></td> 
        <td><a href="labelPrinters.cfm?printer_id=#printer_id#&action=delete" onclick="return confirm('Are you sure you want to delete printer: #printer_name#');"><img src="/images/delete.gif" alt="Delete" /></a></td>
       
    </tr>
    </cfoutput>

<tr>
<td>&nbsp;</td>
    <td><input type="text" name="printer_name_new" id="printer_name_new" value="" maxlength="255" style="width:150px;" autocomplete="false" placeholder="[NEW]"/></td>
    <td><input type="text" name="printer_description_new" id="printer_description_new" value="" maxlength="255" style="width:250px;" autocomplete="false"/></td>
    <td><input type="text" name="printer_labelappid_new" id="printer_labelappid_new" value="" maxlength="15" style="width:45px;" autocomplete="false"/></td>
    <cfloop query="branchplants">
        <td>
            <input type="checkbox" name="printer_branch_new" id="printer_branch_#id#_new" value="#id#"> <label for="printer_branch_#id#_new">#name#</label>
        </td>    
    </cfloop>
     
</tr>
</table>
<br /><br />
                <input type="button" value="Save Settings" id="sbmt" name="sbmt" onclick="disableButton(this);"> 
</form>
</cfoutput>
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>