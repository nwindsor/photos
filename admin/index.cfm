<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Administration</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>Tools</h2>
  	<h3>Your IP: <cfoutput>#cgi.remote_addr#</cfoutput></h3>
    <!---<cfdump var="#cgi#">--->
    <table>
    <tr>
    <td valign="top" style="font-size:1em;">
    
    <h3>Basic Configuration</h3>
    <ul>
    	<li><a href="basic.cfm">Basic Settings</a></li>
    	<li><a href="ldap.cfm">LDAP Authentication</a></li>
    	<li><a href="appearance.cfm">Color Theme / Appearance</a></li>
    	<li><a href="summary.cfm">Setting Summary</a></li>
    </ul>
    <h3>Warehouse Setup</h3>
    <ul>
        <li><a href="labelPrinters.cfm">Label Printers</a></li>  
        <li><a href="/materialRequest/matReqReport.cfm">Mat Req Form Report</a></li>  
        <li><a href="material_requests_workcenters.cfm?">Material Request Workcenters</a></li>
    </ul>    
    <h3>Management Editing/Review</h3>
    <ul>       
    	<!---<li><a href="item_cost_update.cfm">Mass Item Cost Update Wizard</a></li>--->
    	<li><a href="user_list.cfm">Manage DB Users</a></li> 
    	<li><a href="ldap_import.cfm">Import Users from LDAP</a></li> 
    </ul>
    
    <h3>Logging</h3>
    <ul>
    	<li><a href="log_serverConnection.cfm">Connection Rate</a></li>
    	<li><a href="log_view_login.cfm">Login Attempts</a></li>
    	<li><a href="log_password_reset.cfm">Password Reset</a></li>
    	<li><a href="log_cleanup.cfm">System Cleanup</a></li>
    </ul>
    
    </td>
    <td valign="top" style="font-size:1em">
    	<h3>Lists</h3>
        <cfinvoke component="list" method="list_data" list_id="1" returnVariable="lists"></cfinvoke>
        <ul>
		<cfoutput query="lists">
        	<li><a href="list_edit.cfm?id=#id#"><div style="display:inline-block;width:40px;">[#id#]</div> #name#</a></li>
        </cfoutput>
    	</ul>
    </td>
    </tr>
    </table>

<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>