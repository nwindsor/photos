<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfif isDefined('url.action') AND url.action eq 'update'>
	<cfif isDefined('form.logo') and form.logo neq ''>
    	<cffile action="upload" filefield="logo" destination="#getTempDirectory()#" nameconflict="makeunique" />
    	<cfif fileExists('#application.docRoot#\images\logo.png')><cffile action="delete" file="#application.docRoot#images\logo.png" /></cfif>
        <cffile action="copy" source="#file.serverdirectory#\#file.serverfile#" destination="#application.docRoot#images\logo.png" />
    </cfif>
    <cfif isDefined('form.favicon') AND form.favicon neq ''>
    	<cffile action="upload" filefield="favicon" destination="#getTempDirectory()#" nameconflict="makeunique" />
    	<cfif fileExists('#application.docRoot#\favicon.png')><cffile action="delete" file="#application.docRoot#\favicon.png" /></cfif>
        <cffile action="copy" source="#file.serverdirectory#\#file.serverfile#" destination="#application.docRoot#\favicon.png" />
    </cfif>
    
    <cfquery datasource="#application.dsn#" name="updSys">
    UPDATE server_settings
    SET
    <cfif form.headerColor neq ''>headerColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.headerColor#" />,</cfif>
    <cfif form.headerAccentColor neq ''>headerAccentColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.headerAccentColor#" />, </cfif>
    <cfif form.menuOverlayColor neq ''>menuOverlayColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.menuOverlayColor#" />,</cfif>
    <cfif form.menuOverlayHighlightColor neq ''>menuOverlayHighlightColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.menuOverlayHighlightColor#" />,</cfif>
    <cfif form.menuOverlaySelectedColor neq ''>menuOverlaySelectedColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.menuOverlaySelectedColor#" />,</cfif>
    <cfif form.menuBackgroundColor neq ''>menuBackgroundColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.menuBackgroundColor#" />,</cfif>
    <cfif form.menuBorderColor neq ''>menuBorderColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.menuBorderColor#" />,</cfif>
    <cfif form.menuTextColor neq ''>menuTextColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.menuTextColor#" />,</cfif>
    <cfif form.menuTextHighlightColor neq ''>menuTextHighlightColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.menuTextHighlightColor#" />,</cfif>
    <cfif form.linkColor neq ''>linkColor=<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.linkColor#" />,</cfif>
    timezone=timezone
    </cfquery>
    
    <cfinvoke component="../useful" method="setServerSettings" />
    
	<cflocation url="appearance.cfm?message=Appearance Settings saved" />
</cfif>

<cfquery datasource="#application.dsn#" name="getSettings">
SELECT * FROM server_settings
</cfquery>

<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Appearance Settings</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" --><h2>System Settings: Appearance</h2>
<cfoutput>
<a href="index.cfm">Back to Admin Main</a>
<form method="post" action="appearance.cfm?action=update" enctype="multipart/form-data" >
<table class="formTable">
	<tr>
    	<td colspan="2">&nbsp;</td>
        <td>Current</td>
        <td>Default</td>
    </tr>
	<tr>
    	<td>Logo:<br><span style="font-size:.84em;">30x[Width] PNG</span></td>
    	<td><input type="file" name="logo" style="width:200px;" /> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="For best appearance logos should be no more than 30 pixels high and 100 pixels wide.  All logos should be on a transparent background or one which matches the header color."></td>
    	<td><div style="border:1px solid ##cccccc;padding:10px;text-align:center;"><img src="/images/logo.png" /></div></td>
    	<td><div style="border:1px solid ##cccccc;padding:10px;text-align:center;"><img src="/images/logo_default.png" /></div></td>
    </tr>
    <tr>
    	<td>Favicon:<br><span style="font-size:.84em;">16x16 PNG</span></td>
        <td><input type="file" name="favicon" style="width:200px;" /> <img src="../Images/blue-question-mark.png" style="cursor:pointer;" alt="" title="Favicons must be a 16x16 PNG"></td>
        <td><div style="border:1px solid ##cccccc;padding:10px;text-align:center;"><img src="/favicon.png" /></div></td>
        <td><div style="border:1px solid ##cccccc;padding:10px;text-align:center;"><img src="/favicon_default.png" /></div></td>
    </tr>
	<script language="javascript">
	$(function() {
		$('##headerColor, ##headerAccentColor, ##menuOverlayColor,##menuOverlayHighlightColor,##menuOverlaySelectedColor,##menuBackgroundColor,##menuBorderColor,##linkColor,##menuTextHighlightColor,##menuTextColor ').ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
		})
	});
	</script>
    <tr>
    	<td>Header Background:</td>
        <td>##<input type="text" name="headerColor" id="headerColor" value="#application.headerColor#" required placeholder="006599" style="width:125px;" maxlength="6"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.headerColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##headerColor').val('#application.headerColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##006599;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##headerColor').val('006599');"></div></td>
    </tr>
    <tr>
    	<td>Header Accent:</td>
        <td>##<input type="text" name="headerAccentColor" id="headerAccentColor" value="#application.headerAccentColor#" required placeholder="333" style="width:125px;" maxlength="6"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.headerAccentColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##headerAccentColor').val('#application.headerAccentColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##333;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##headerAccentColor').val('333');"></div></td>
    </tr>
    <tr>
    	<td>Menu Overlay:</td>
        <td>##<input type="text" name="menuOverlayColor" id="menuOverlayColor" value="#application.menuOverlayColor#" required placeholder="C3E7FB" style="width:125px;" maxlength="6"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.menuOverlayColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuOverlayColor').val('#application.menuOverlayColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##C3E7FB;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuOverlayColor').val('C3E7FB');"></div></td>
    </tr>
    <tr>
    	<td>Menu Overlay Hover:</td>
        <td>##<input type="text" name="menuOverlayHighlightColor" id="menuOverlayHighlightColor" value="#application.menuOverlayHighlightColor#" required placeholder="006599"  maxlength="6" style="width:125px;"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.menuOverlayHighlightColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuOverlayHighlightColor').val('#application.menuOverlayHighlightColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##F8BA70;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuOverlayHighlightColor').val('F8BA70');"></div></td>
    </tr>
    <tr>
    	<td>Menu Overlay Selected:</td>
        <td>##<input type="text" name="menuOverlaySelectedColor" id="menuOverlaySelectedColor" value="#application.menuOverlaySelectedColor#" required placeholder="F8A33E" maxlength="6" style="width:125px;"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.menuOverlaySelectedColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuOverlaySelectedColor').val('#application.menuOverlaySelectedColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##F8A33E;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuOverlaySelectedColor').val('F8A33E');"></div></td>
    </tr>
    <tr>
    	<td>Menu Background:</td>
        <td>##<input type="text" name="menuBackgroundColor" id="menuBackgroundColor" value="#application.menuBackgroundColor#" required placeholder="eeeeee" maxlength="6" style="width:125px;"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.menuBackgroundColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuBackgroundColor').val('#application.menuBackgroundColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##eeeeee;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuBackgroundColor').val('eeeeee');"></div></td>
    </tr>
    <tr>
    	<td>Menu Border:</td>
        <td>##<input type="text" name="menuBorderColor" id="menuBorderColor" value="#application.menuBorderColor#" required placeholder="004163" maxlength="6" style="width:125px;"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.menuBorderColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuBorderColor').val('#application.menuBorderColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##004163;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuBorderColor').val('004163');"></div></td>
    </tr>
    <tr>
    	<td>Menu Text:</td>
        <td>##<input type="text" name="menuTextColor" id="menuTextColor" value="#application.menuTextColor#" required placeholder="004163" maxlength="6" style="width:125px;"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.menuTextColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuTextColor').val('#application.menuTextColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##004163;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuTextColor').val('004163');"></div></td>
    </tr>
    <tr>
    	<td>Menu Text Hover/Select:</td>
        <td>##<input type="text" name="menuTextHighlightColor" id="menuTextHighlightColor" value="#application.menuTextHighlightColor#" required placeholder="693900" maxlength="6" style="width:125px;"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.menuTextHighlightColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuTextHighlightColor').val('#application.menuTextHighlightColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##693900;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##menuTextHighlightColor').val('693900');"></div></td>
    </tr>
    <tr>
    	<td>Link:</td>
        <td>##<input type="text" name="linkColor" id="linkColor" value="#application.linkColor#" required placeholder="F18300" maxlength="6" style="width:125px;"/></td>
        <td><div style="border:1px solid ##cccccc;background-color:###application.linkColor#;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##linkColor').val('#application.linkColor#');"></div></td>
        <td><div style="border:1px solid ##cccccc;background-color:##F18300;width:25px;height:25px;cursor:pointer;margin:auto;" onclick="$('##linkColor').val('F18300');"></div></td>
    </tr>
</table>
                <input type="button" value="Save Settings" id="sbmt" name="sbmt" onclick="disableButton(this);"> 
</form>
</cfoutput>

<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>