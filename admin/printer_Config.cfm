<cfif not session.admin><cflocation url="../index.cfm?message=Permission denied." /></cfif>

<cfif isDefined("form.submitbutton")>

<cftransaction>
    <cfquery datasource="#application.dsn#" name = "Delete">
    DELETE FROM label_printer_location
    WHERE printer_id = <cfqueryparam cfsqltype="cf_sql_bigint" value="#url.printer_id#">
    </cfquery>
    <cfif isDefined("form.enabled_check")>
        <cfloop list = "#form.enabled_check#" index = "i">


            <cfquery datasource="#application.dsn#" name = "Abbreviation">
                SELECT *
                FROM list_element
                WHERE id = <cfqueryparam cfsqltype="cf_sql_bigint" value="#i#">
            </cfquery>

            <cfif Abbreviation.recordcount>

                <cfquery datasource="#application.dsn#" name = "Insert">
                    INSERT INTO label_printer_location(printer_id,line,work_center)
                    VALUES(
                    <cfqueryparam cfsqltype="cf_sql_bigint" value="#url.printer_id#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#Abbreviation.abbreviation#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#Abbreviation.name#">)                
                </cfquery>
            </cfif>

        </cfloop>
    </cfif>
</cftransaction>
    <cflocation url ="printer_Config.cfm?printer_id=#url.printer_id#"/>
</cfif>
  <cfquery datasource="#application.dsn#" name="printerlocation">


   SELECT loc.id locid,*
    FROM list_element loc
    LEFT JOIN label_printer_location prn ON prn.work_center=loc.[name] AND prn.line=loc.abbreviation and prn.printer_id= #url.printer_id#
    WHERE loc.list_id=160
    ORDER BY loc.sortorder DESC
  </cfquery>
        
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Label Printer Locations</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" -->
<script language="javascript">


</script>

<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>


<cfoutput>
<form action="printer_Config.cfm?printer_id=#printer_id#" method ="Post">


<div id="content-main">
<h2>Printer Configuration: Label Printers</h2>
<a href="labelPrinters.cfm">Back To Label Printers</a>
<br>

<!-- InstanceBeginEditable name="Main" -->
<cfquery datasource="#application.dsn#" name="getPrinterLocation">
SELECT printer_name FROM label_printers WHERE printer_id=#url.printer_id#
</cfquery>
<cfoutput>
 <cfset current_printer=valueList(getPrinterLocation.printer_name)>
<h3>Configuring Printer: <b>#current_printer#</b></h3>
</cfoutput>
    <table border=2 class="formTable">
        <tr>
            <th>Enabled</th>
            <th>Line</th>
            <th>Location</th>    
            
        </tr>           
            
        <cfloop query = "printerlocation">
 
            <tr>
               <td><input type="checkbox" name="enabled_check"  id="enabled_#id#" value="#id#" <cfif work_center NEQ ''>checked </cfif>></td>
                 <td ><label class= "enabled_check" for ="enabled_#id#">#abbreviation#</label></td>
                <td ><label class= "enabled_check" for ="enabled_#id#">#name#</label></td>
            </tr>
            <label class= "enabled_check" for ="enabled_#id#">
        </cfloop>
                                
                <td><input type="submit" value="submit" name="submitbutton"/></td>
 
    </table>
<!-- InstanceEndEditable -->    
</div>
</cfoutput>
</form>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>