<cfif not isDefined('url.id')><cf_error message="No list defined." /></cfif>
<cfif not isDefined('session.admin') OR session.admin neq 1>
	Permission denied.<cfabort>
</cfif>

<cfif isDefined('url.action') AND url.action eq 'update'>
	<cfset tm = createodbcdatetime(now())>
    <cfset usr = session.userid>
    
	<!--- process updates --->
    <cftransaction>
    <cfif isDefined('form.ids') AND form.ids neq ''>
    	<cfloop list="#form.ids#" index="i">
        	<cfif i neq ''>
            	<cfquery datasource="#application.dsn#" name="updateRecord">
                UPDATE list_element
                	SET
                    	name=<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="100" value="#evaluate('form.elm_name_' & i)#" />,
                        description=<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="1000" value="#evaluate('form.elm_description_' & i)#" />,
                        abbreviation=<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#evaluate('form.elm_abbreviation_' & i)#" />,
                        sortorder=<cfqueryparam cfsqltype="cf_sql_integer" value="#evaluate('form.elm_sortorder_' & i)#" />,
                        default_selected=<cfif isDefined('form.elm_default_selected_' & i)>1<cfelse>0</cfif>,
                        modify_user=#usr#,
                        modify_date=#tm#
                    WHERE ID = <cfqueryparam cfsqltype="cf_sql_bigint" value="#i#" />
                </cfquery>
            </cfif>
        </cfloop>
    </cfif>
    </cftransaction>
    
    <!--- insert new record --->
	<cfif   isDefined('form.elm_name_new') AND form.elm_name_new neq ''>
        <cftransaction>
        	<cfquery datasource="#application.dsn#" name="insertElm">
            INSERT INTO list_element(list_id,name, description, abbreviation, sortorder, default_selected, modify_user, modify_date)
            VALUES(
            		<cfqueryparam cfsqltype="cf_sql_integer" value="#url.id#" />,
                    <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="100" value="#form.elm_name_new#" />,
            		<cfqueryparam cfsqltype="cf_sql_varchar" maxlength="1000" value="#form.elm_description_new#" />,
                    <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="15" value="#form.elm_abbreviation_new#" />,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#form.elm_sortorder_new#" />,
                    <cfif isDefined('form.elm_default_selected_new')>1<cfelse>0</cfif>,
                    #usr#,#tm#)
            </cfquery>
        </cftransaction>
	</cfif>
</cfif>

<cfif isDefined('url.action') AND url.action eq 'delete'>
	<cfif not isDefined('url.element_id')><cf_error message="No element defined." /></cfif>
    <cfquery datasource="#application.dsn#" name="deleteElm">
    DELETE FROM list_element WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.element_id#" />
    </cfquery>	
</cfif>


<cfquery datasource="#application.dsn#" name="getList">
SELECT * FROM list_element WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#url.id#" />
</cfquery>

<cfinvoke component="list"
		  method="list_data"
          list_id="#url.id#"
          returnvariable="getData" />
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Main.dwt.cfm" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title><cfoutput>#application.sitename#</cfoutput>:: Whouse List Edit</title>
<!-- InstanceEndEditable -->
<cfinclude template="/includes/head.cfm" />
<!-- InstanceBeginEditable name="head" --><script language="javascript">
$(function() {
	$('#elm_name_new').focus();	
});

</script>
<!-- InstanceEndEditable -->
</head>

<body>
<div id="wrapper">
<div id="header">
  <h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>
<cfinclude template="/includes/menus.cfm" />
<cfinclude template="/includes/message.cfm" />

<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="user">
Welcome, <cfoutput>#session.email#</cfoutput> <a href="/logout.cfm">[Logout]</a>
</div>
</cfif>

<div id="content-main">
<!-- InstanceBeginEditable name="Main" -->
            <cfoutput>
			<h2>Editing List  (#getList.name#)</h2>

<cfform method="post" action="list_edit.cfm?id=#url.id#&action=update">
<input type="hidden" name="ids" value="" />
<table border=1 class="formTable">
	<tr>
    	<th>ID</th>
        <th>Name</th>
        <th>Abbr.</th>
        <th>Description</th>
        <th>Default</th>
        <th>Order</th>
        <th>&nbsp;</th>
    </tr>
    <cfloop query="getData">
    	<tr>
        	<td class="head"><input type="hidden" name="ids" value="#id#" />#id#</td>
            <td><input name="elm_name_#id#" type="text" maxlength="100" class="textbox" value="#name#" style="width:180px;"></td>
            <td><input name="elm_abbreviation_#id#" type="text" maxlength="15" class="textbox" value="#abbreviation#" style="width:50px;"></td>
            <td><input name="elm_description_#id#" type="text" class="textbox" maxlength="1000" value="#description#" style="width:400px;"></td>
            <td><input type="checkbox" name="elm_default_selected_#id#" <cfif default_selected>checked</cfif> />
            <td><cfinput name="elm_sortorder_#id#" type="text" class="textbox" value="#sortorder#" required="yes" style="width:30px;" maxlength="4" validate="integer" message="Sort order must always be a number."></td>
            <td><a href="list_edit.cfm?id=#url.id#&action=delete&element_id=#id#" onClick="return confirm('Are you sure you want to delete this list element?');"><img src="../../images/delete.gif" alt="Delete" border="0" /></a></td>
        </tr>
    </cfloop>
    <cfset newCounter = getData.recordcount + 1>
    	<tr>
        	<td class="head">[New]</td>
            <td><input name="elm_name_new" id="elm_name_new" type="text" class="textbox" value="" style="width:180px;" maxlength="100"></td>
            <td><input name="elm_abbreviation_new" type="text" class="textbox" value="" style="width:50px;" maxlength="15"></td>
            <td><input name="elm_description_new" type="text" class="textbox" value="" style="width:400px;" maxlength="1000"></td>
            <td><input type="checkbox" name="elm_default_selected_new" />
            <td><cfinput name="elm_sortorder_new" type="text" class="textbox" value="#newCounter#"  style="width:30px;" maxlength="4" required="yes"  validate="integer" message="Sort order must always be a number."></td>
            <td>&nbsp;</td>
        </tr>
    
</table>
<cfinput type="submit" name="update" value="Update" /> <cfinput type="reset" name="reset" value="Reset" /> <input type="button" onClick="window.location.href='index.cfm';" value="Cancel"/>
</cfform>
</cfoutput>
<!-- InstanceEndEditable -->    
</div>

<cfinclude template="/includes/copyright.cfm" />
</div>

</body>
<!-- InstanceEnd --></html>