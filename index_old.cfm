<!-- uploads image -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> 
<script>
    // Test upload
    function uploadFile()
    {
        var files=document.getElementById("file").files;
        var req = new XMLHttpRequest();
        var data = new FormData();

        data.append('upload', files[0]);
        data.append('i', 4); // testing file upload
        req.onreadystateexchange = function() {if(req.readyState == XMLHttpRequest.DONE) {console.log(req);}}.bind(this);

        // sends data
        req.open("POST", "/rest/api/edit/upload?i=4");
        req.send(data);
    }

    // Test delete
    function ddelete()
    {
        $.post({
            url: "/rest/api/edit/delete",
            data: JSON.stringify({"id": $("#number").val()})
        });
    }

    // Test update
    function update()
    {
        var files=document.getElementById("file").files;
        var req = new XMLHttpRequest();
        var data = new FormData();

        data.append('upload', files[0]);
        data.append('id', $("#number").val());

        req.onreadystateexchange = function() {if(req.readyState == XMLHttpRequest.DONE) {console.log(req);}}.bind(this);
        req.open("POST", "/rest/api/edit/update");
        req.send(data);
    }

    function validate()
    {
        $.get("/rest/api/view/validatelibrary", {
            cols: $("#cols").val(),
            tbl: $("#table").val()
        }).done(function(data) {
            $("#validated").text(data);
        });
    }
</script>

<cfscript>
    // meaningless nonsense. syntax scratch paper
    if(true)
    {
        a = 1;
    }
</cfscript>

<cfinclude template="/includes/head.cfm" />

<cfoutput>
        

    <!-- upload tests -->
    <p>upload an image</p>
    <input type="file" id="file"/>
    <input type="number" id="number"/>
    <input type="button" id="btn_uploadfile" value="Upload" onclick="uploadFile();"/>
    <input type="button" id="btn_unitdelete" value="Delete" onClick="ddelete();"/>
    <input type="button" id="btn_update" value="Update" onclick="update();"/>
    <h3>validate library creation</h3>
    <p>source table name</p>
    <input type="textbox" id="table"/>
    <p>fields</p>
    <input type="textbox" id="cols"/>
    <input type="button" id="btn_validate" value="validate" onclick="validate();"/>
    <p id="validated"></p>
</cfoutput>