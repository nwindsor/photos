component restpath="/view" rest="true" {
    /**
     * Gets an array of library structs, in the follwing format:
     * {id, name, directory, displaysize, confidentiality}
     */
    remote struct function listLibraries()
    httpmethod="GET" restpath="libraries"
    {
        return PhotoLibrary::getLibrariesWithCounts();
    }

    /**
     * Within a given photo library, get an array of size structs from the database in the follwing format:
     * {{id, name, heightMin, heightMax, widthMin, widthMax, aspectMin, aspectMax}, ...}
     * 
     * @id ID of the library from which to get the sizes
     */
    remote array function librarySizes(numeric id restArgSource="Path")
    httpmethod="GET" restpath="libraries/{id}/sizes"
    {
        return PhotoLibrary::getSizes(arguments.id);
    }

    /**
     * Gets an array of structs representing the items.
     * Fields include all the visible/sortable fields, along with:
     * 
     * - "numphotos" (number of original photos of a given item),
     * - "thumbnail", representing the default photo id that should be displayed in the gallery
     * - "galleryitemid", representing the itemId in the gallery's items table,
     * - "sourceitemid", representing the id field in the source table
     *
     * @id library id
     */
    remote array function libraryItems(numeric id restArgSource="Path", string sort restArgSource="url")
    httpmethod="GET" restpath="libraries/{id}"
    {
        return Item::validateSort(arguments.sort ?: "/", arguments.id) ? Item::getItems(arguments.id, arguments.sort) : Item::getItems(arguments.id);
    }

    /**
     * Gets the original sized photo structs of all photos for a given item,
     * in the follwing format:
     * 
     * - "id" (the id of the photo),
     * - "createuser" (user id that first uploaded the photo),
     * - "createdate" (date on which the image was first uploaded),
     * - "modifyuser" (user id of the user that last modified the photo),
     * - "modifydate" (date on which the image was most recently modified)
     * 
     * @id id of item for which to get all original photos of.
     */
    remote array function itemPhotos(numeric id restArgSource="Path")
    httpmethod="GET" restpath="items/{id}"
    {
        return Item::getPhotos(arguments.id);
    }

    /**
     * Given an original photo, gets the sizes of all instances (including the original)
     * in terms of ID, extension, and size data, stored in array form:
     * {{id, ext, heightMin, heightMax, widthMin, widthMax, aspectMin, aspectMax}, ...}
     *
     * @id id of the original photo from which to find derivatives sizes
     */
    remote array function photoSizes(numeric id restArgSource="Path")
    httpmethod="GET" restpath="photos/{id}"
    {
        return Photo::getAllSizes(arguments.id);
    }

    /**
     * Returns an array containing all items lacking photos.
     */
    remote array function todo()
    httpmethod="GET" restpath="todo"
    {
        return arrayNew(1);
    }

    /**
     * PING PING PING 
     */
    remote string function ping()
    httpmethod="GET" restpath="ping"
    {
        return "pong!";
    }

    /**
     * 
     *
     * @tbl 
     * @cols 
     */
    remote boolean function validateLibrary(string tbl restargsource="Url", string cols restargsource="url")
    httpmethod="GET" restpath="validatelibrary"
    {
        return PhotoLibrary::validateColumns(arguments.cols, arguments.tbl);
    }

    /*remote array function items()
    httpmethod="GET" restpath="lib"
    {
        
    }*/
}