component restpath="/dummy"  rest="true" {
    import "obj.Test";
    import "obj.ItemSource";
    import "obj.SQLItemSource";
    import "obj.PhotoLibrary";

    // figuring out the elvis operator. if lashes is null, it returns what's after it.
    remote function jackal(numeric lashes restArgSource="url", string dumbquote restargsource="url")
    httpmethod="GET" restpath="jackal"
    {
        return lashes ?: "this is what happens when lion tamers unonize.";
    }

    remote string function urlArray(string str restArgSource="url")
    httpmethod="GET" restpath="enctest"
    {
        return arguments.str;
    }

    remote array function getFuncTest() httpmethod="GET" restpath="func"
    {
        t = new Test("Hello");

        return [t.sayAToWorld(), "uptown func gon give it to ya"];
    }

    remote array function getCats() httpmethod="GET" restpath="cats"
    {
        return ["https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.aFlYPhSDQIQAmSNnyJJJUgAAAA%26pid%3DApi&f=1","https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.rVot13D1MTpC1Yqxdv97PAHaLH%26pid%3DApi&f=1","floppa"];
    }

    remote array function testSplit() httpmethod="GET" restpath="split"
    {
        a = "jjjj##uwu## AND ##owo## neko nyaa! lumi is ########1!";
        return a.split('##');
    }

    remote string function unitReduce() httpmethod="GET" restpath="reduce"
    {
        return ArrayReduce(["Name", "Age", "Gamertag"], function(carry, value) {
            return carry & 't.' & value & ' as data_' & value & ', ';
        }, '');
    }

    remote bool function unitSaveImageToFiles() httpmethod="GET" restpath="ffs"
    {
        img = Test::getImage();
        extension = "jpg";

        Upload::uploadImage(img, extension, 2, 1);

        return true;
    }

    remote function unitUploadImage() httpmethod="POST" restpath="save"
    {
        // Checks the form data to see if a file exists.
        if(structKeyExists(form, "upload") && len(form.upload))
        {
            file = fileUpload(getTempDirectory(), "form.upload", "", "makeUnique");
        }
        else
        {
            // No file.
            throw(type="ForcedException", message="f", detail="f");
            return;
        }

        // Checks that the uploaded file is an image.
        if(Upload::validateImage(file))
        {
            img= imageRead(file.ServerDirectory & '\' & file.ServerFile)

            // image valid, load and upload it.
            Upload::uploadImage(img, file.serverFileExt, 3, 1);
        }
        else
        {
            // File is not an image.
            throw(type="ForcedException", message="#Upload::validateImageDebug(file)#", detail="oopsy woopsy! uwu we did a fucky wucky!");
            return;
        }
    }

    remote numeric function unitJ() httpmethod="GET" restpath="J"
    {
        return Photo::getOriginal(50);
    }

    remote boolean function unitValidateColumn() httpmethod="GET" restpath="validatecolumn"
    {
        return Item::validateSort("gamertag ASC", 1);
    }

    remote array function altsizes() httpmethod="GET" restpath="altsizes"
    {
        return Photo::getAlternateSizes(77);
    }

    remote function unitCount() httpmethod="GET" restpath="unitcount"
    {
        return PhotoLibrary::getLibrariesWithCounts();
    }

    /*remote array function getItems(number library) httpmethod="GET" restpath="images"
    {
        // Return variable
        r = arrayNew();

        // Makes queries
        qry = new query();
        qry.name = "get"
    }*/
    
}