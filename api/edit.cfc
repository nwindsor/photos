component restpath="/edit" rest="true" {
    import "obj.Upload";
    import "obj.Test";

    /**
     * Uploads a full sized image of an item and resizes accordingly.
     *
     * Option 1. upload?i=i
     * @i item for which to upload an image of.
     * 
     * Option 2. upload?library=library&sourceid=sourceid
     * @library the id of the library in which the item resides
     * @sourceid PK of the item in the source table the library references.
     */
    remote string function uploadImage(numeric i restArgSource="form", numeric library restArgSource="form", numeric sourceid restArgSource="form")
    httpmethod="POST" restpath="upload"
    {
        // TODO: Authenticate
        usr = 1;
        
        // Checks the form data to see if a file exists.
        file = Upload::saveTempFile(form);

        // itemid ? gen
        itemid = arguments.i ?: Item::getItem(arguments.library ?: 0, arguments.sourceid ?: 0);

        if(itemid == 0)
        {
            return 0;
        }

        // Checks that the uploaded file is an image.
        if(!file.isEmpty() && Upload::validateImage(file))
        {
            // image valid, load and upload it.
            // ArrayLast("image/peeNG".Split("/"));
            Upload::uploadImage(
                imageRead(file.ServerDirectory & '\' & file.ServerFile), // Loads image for processing
                Upload::getMimeFriendlyExtension(file), // Gets a new file extension that matches the MIME type
                arguments.i, // Passes ID
                usr
            );

            // logs action to photo uploads log. TODO: elaborate
            WriteLog(file="photoupdates.log",text="New image for item ###arguments.i# inserted by user ###usr#.");
        }
        else
        {
            // File is not an image.
            return;
        }
    }

    /**
     * Deletes all sizes of a photo from the database (and files if applicable).
     * also removes references to them in items' thumbnails.
     *
     * @body 
     */
    remote function delete(required string body restArgSource="body")
    httpmethod="POST" restpath="delete"
    {

        // TODO: Authenticate
        usr = 1;

        // TODO: check if exists

        // Gets original photo and deletes it along with all its sizes
        Photo::delete(
            Photo::getOriginal(deserializeJSON(arguments.body).id)
        );
    }

    /**
     * Updates an image, rescaling to update all related sizes.
     * 
     * If the image doesn't fit any of the library's sizes, it will save it as a separate 'original'-sized photo.
     * 
     * If the newly uploaded image matches a size, but the old one didn't, it removes the unsized variant.
     *
     * @id id of the photo to replace
     */
    remote function update(required numeric id restArgSource="FORM")
    httpmethod="POST" restpath="update"
    {
        // TODO: Authenticate
        usr = 1;

        // Saves temp file and gets struct
        file = Upload::saveTempFile(form);
        // validate id

        // Checks that the uploaded file is an image.
        if(!file.isEmpty() && Upload::validateImage(file))
        {
            // Updates image
            Upload::updateImage(
                imageRead(file.ServerDirectory & '\' & file.ServerFile),
                Upload::getMimeFriendlyExtension(file),
                arguments.id,
                usr
            );
        }
    }
}