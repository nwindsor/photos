component displayname="PhotoLibrary"
{
    /*property numeric id;
    property string name;
    property string directory;
    property numeric confidentiality; // 0 green, 1 yellow, 2 red
    property string nameFormat;
    property string descFormat;
    property ItemSource isrc;
    property PhotoUrlGenerator pug;
    property function uploadCallback;*/

    /**
     * Creates a new PhotoLibrary based on the data
     *
     */
    /*public function init(string name, string directory, numeric confidentiality, string nameFormat, string descFormat, ItemSource isrc)
    {
        variables.instance.name = arguments.name;
        variables.instance.directory = arguments.directory;
        variables.instance.confidentiality = arguments.confidentiality;
        variables.instance.nameFormat = arguments.nameFormat;
        variables.instance.descFormat = arguments.descFormat;
        variables.instance.isrc = arguments.isrc;
    }*/

    /**
     * Gets an item's name based on the library's specified name format.
     *
     * @item item to get the name of
     *
    public string function formatName(struct item)
    {
        return format(item, variables.instance.nameFormat);
    }

    /**
     * Gets an item's description based on the library's specified description format.
     *
     * @item item to get the description of
     *
    public string function formatDescription(struct item)
    {
        return format(item, variables.instance.descFormat);
    }

    /**
     * Replaces #variables# in a format string with the #variables# in a struct.
     * 
     * @fmt string to format
     * @item data to replace the variables with
     *
    public static string function format(struct item, string fmt)
    {
        // Result string
        r = "";

        // Sorts out tags from the item format string
    
        // Splits the array by the # operator.
        fmtags = arguments.fmt.split("##");
        for(i = 1; i <= arrayLen(fmtags); i++)
        {
            // Determines if index is odd or even
            
            if(i % 2)
            {
                // The array index is odd, and therefore text. Adds it to the string.
                r &= fmtags[i];
            }
            else
            {
                // The array index is even, and thus a tag or an escaped # symbol.
                if(fmtags[i] == "")
                {
                    // This is an escaped # symbol.
                    r &= "##";
                }
                else
                {
                    // This is a tag. Replaces it with the item's value at the tag's key.
                    r &= arguments.item[fmtags[i]];
                }             
            }
        }

        return r;
    }*/

    /**
     * Gets a list of items along with their thumbnails and accompanying information 
     * in the following format:
     * 
     * {id, name, desc, thumb, numphotos}
     * 
     * @sort source field to sort photos by
     *
    public array function getAllItems(string sort)
    {
        // Gets a list of items from the source
        itm = variables.instance.isrc.getItems(arguments.sort);

        // Adds name and description info to each struct in the array.
        return ArrayMap(itm, function(x) {
            return x.append({"nameinfo":{"name":formatName(x),"desc":formatDescription(x)}}, false);
        }, true, 16);
    }*/

    /**
     * Gets an array of links to all photos of an item in a given size.
     *
     * @itemid ID of the item for which to retreive photos.
     * @size size ID of the image. defaults to full size (null).
     */
    public static array function getPhotos(numeric itemid, numeric size="")
    {
        query name="ids" datasource="photos" returntype="array"
        params = {itemid:{type:'bigint', value=arguments.itemid}, size:{type:'bigint', value=arguments.size}} 
        {
            echo("
                SELECT id
                FROM photos
                WHERE itemid = :itemid AND size = :size
            ");
        }

        // TODO: null sql?

        return arrayMap(ids, function(x) {
            return "img.cfm?id=" & x;
        }/*, true, 16*/);
    }

    /**
     * Within a given photo library, get an array of size structs from the database in the follwing format:
     * {{id, name, heightMin, heightMax, widthMin, widthMax, aspectMin, aspectMax}, ...}
     * 
     * @libraryId ID of the library from which to get the sizes
     */
    public static array function getSizes(numeric libraryId)
    {
        query name="sizes" datasource="photos" returntype="array"
        params= {id:{type:'bigint', value=arguments.libraryId}}
        {
            echo("
                SELECT id, name, heightMin, heightMax, widthMin, widthMax, aspectMin, aspectMax
                FROM photoSizes
                WHERE libraryId = :id
            ");
        }

        return sizes;
    }

    /**
     * Gets the location in which a library's
     *
     * @libraryId 
     */
    public static string function getLocation(numeric libraryId)
    {
        query name="library" datasource="photos" returntype="array"
        params={id:{type:'bigint', value=arguments.libraryId}}
        {
            echo("
                SELECT directory
                FROM photoLibraries
                WHERE id = :id
            ");
        }

        return library[1].directory;
    }

    /**
     * Gets an array of library structs, in the follwing format:
     * {id, name, directory, displaysize, confidentiality}
     */
    public static struct function getAllLibraries()
    {
        query name="libs" datasource="photos" returntype="struct" columnkey="id"
        {
            echo("SELECT * FROM photoLibraries");
        }

        return libs;
    }

    /**
     * Gets an array of library structs like getAllLibraries(), but with an 
     * additional field, numItems, which references the amount of items in its source database
     */
    public static struct function getLibrariesWithCounts()
    {
        return PhotoLibrary::getAllLibraries().map((k, v) => {
            return v.append({
                "numItems": countItems(v.name)
            });
        }, false);
    }

    /**
     * Counts the number of items in a given table
     *
     * @sourceTable name of the table in which to count items
     */
    public static numeric function countItems(string sourceTable)
    {
        query name="count" datasource="photos" returntype="struct"
        {
            echo("SELECT count(*) as numItems FROM " & arguments.sourceTable);
        }

        return count.numItems;
    }

    /**
     * Creates a new library. Returns the inserted key on sucess, and 0 on failure.
     *
     * @visibleColumns string of columns in the source database the library is permitted to show in the gallery, separated by ", ". 
     * @confidentiality confidentiality of photos; 0 for green, 1 for yellow, 2 for red.
     * @sourceTableName name of the table the library draws from for search, sort, and filtration data.
     * @sourcePKName the primary key of said table (defaults to 'id').
     */
    public static numeric function createLibrary(string visibleColumns,string confidentiality, string sourceTableName, string sourcePKName = "")
    {
        // TODO: make sure name wasn't already taken

        // validate visible columns
        if(!validateVisibleColumns(arguments.visibleColumns, arguments.sourceTableName))
        {
            return 0;
        }

        // Inserts new record into photoLibraries
        queryExecute(
            "   INSERT INTO photoLibraries(sourcePKname, visibleColumns, confidentiality, directory, name)
                VALUES(:sourcePKName, :visibleColumns, :confidentiality, :directory, :name)
            ",
            {
                visibleColumns:{type:'nvarchar', value=arguments.visibleColumns},
                confidentiality:{type:'tinyint', value=arguments.confidentiality},
                directory:{type:'nvarchar', value=arguments.visibleColumns},
                name:{type:'nvarchar', value=arguments.sourceTableName}
            },
            {
                datasource="photos",
                result="result"
            }
        );

        return result.generatedKey;
    }

    /**
     * TODO: Adds a size to a library
     */
    public static function createSize(string name, numeric heightMax, numeric heightMin, numeric widthMax, numeric widthMin, numeric aspectmax, numeric aspectMin, numerc libraryId)
    {
        // check if library has a display size
        // if not, add it
    }

    /**
     * TODO
     *
     * @tbl 
     *
    public static function getFields(string tbl)
    {
        
    }*/

    /**
     * Ensures:
     * - visible columns and table name are all valid
     * - table exists
     * - all columns exist in the table
     * - columns are separated by a comma and space
     * - there's a comma and space at the end
     * 
     * example of valid input: "name, id, email, "
     * example of invalid input: "name,, id email'); DROP TABLE photos;--""
     * 
     * @visicols list of visible columns with commas and spaces at the end
     * @srctable name of the source table to check the columns in
     */
    public static boolean function validateColumns(string visicols, string srctable)
    {
        // Ensures strings are at least minimum length
        if(isEmpty(srctable) || len(visicols) <= 2)
        {
            return false;
        }

        // makes sure the columns string ends with a comma and a space
        if(visicols.mid(len(visicols) - 1, 2) != ", ")
        {
            return false;
        }

        /* Ensures that:
        - the visible columns string contain only letters, commas, and spaces
        - the source table string only contains letters*/
        if(reFind("[^A-Za-z, ]", arguments.visicols) || reFind("[^A-za-z]", arguments.srctable))
        {
            return false;
        }

        // removes trailing comma space and replaces comma-space pairs with commas.
        commacolumns = visicols.mid(1, len(visicols)-2).replace(", ", ",", "ALL");
        
        
        if(commacolumns.find(" "))
        {
            return false;
        }

        // ensures there aren't consecutive commas/spaces
        if(listFind(commacolumns, "", ",", true))
        {
            return false;
        }

        // checks if table exists
        query name="tablecheck" datasource="photos"
        params={tablename:{type:"nvarchar", value=srctable}}
        {
            echo("
                SELECT *
                FROM INFORMATION_SCHEMA.TABLES
                WHERE TABLE_NAME = :tablename
            ");
        }

        // If there are no records, the table doesn't exist.
        if(tablecheck.recordCount == 0)
        {
            return false;
        }

        // checks if specified columns exist in said table
        query name="columncheck" datasource="photos"
        params={tablename:{type:"nvarchar", value=srctable}}
        {
            echo("
                SELECT *
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_NAME = :tablename
            ");
        }

        // Gets the list of columns in the specified table.
        cols = columncheck.columndata("COLUMN_NAME");
        Writedump(cols);

        Writedump(commacolumns.split(","))
        // Ensures all of the specified columns exist in the table
        for(i in commacolumns.split(","))
        {
            if(!cols.find(i))
            {
                return false;
            }
        }

        // Data is valid.
        return true;
    }

    /**
     * 
     */
    public static boolean function validateColumn(string column, numeric libraryId)
    {
        // Ensures that the field specified contains only letters
        if(reFind("[^A-Za-z]", arguments.column))
        {
            return false;
        }

        // checks for relevant columns in database
        query name="cols" datasource="photos" returntype="struct"
        params={id:{type:"bigint", value=arguments.libraryId}}
        {
            echo("
                SELECT TOP(1) visibleColumns
                FROM photoLibraries
                WHERE id = :id
            ");
        }

        

        // ensures there is a result
        if(structKeyExists(cols, "visibleColumns"))
        {
            /// of the result (if there is one) find the uh if it fits
            if(listFind(cols.visibleColumns, arguments.column, ", "))
            {
                return true;
            }
        }

        return false;
    }
}