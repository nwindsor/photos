component {

    /**
     * Uploads an image of an item, resizing the image as appropriate for the library.
     * 
     * @image ColdFusion image
     * @extension File format of an image without the . , i.e. "png"
     * @libraryId ID of library in which the image is being stored
     * @location File folder in which to store the image (or empty for SQL blob storage)
     * @itemId Numeric ID of a given item.
     * @size SQL ID of the specific size to update.
     * @requestingUserId ID of the user requesting to upload.
     */
    public static function uploadImage(struct image, string extension, numeric itemId, numeric requestingUserId)
    {
        // Gets library info
        var libraryId = Item::getLibraryid(itemid);      
        var sizes = PhotoLibrary::getSizes(libraryId);
        var location = PhotoLibrary::getLocation(libraryId);
        
        // Saves arguments in a different scope to be referenced within uploadSize
        img = arguments.image;
        ext = arguments.extension;
        itm = arguments.itemId;
        usr = arguments.requestingUserId;

        // Checks if img matches any size constraints
        originalsize = getSizeFromDimensions(img, sizes);
        
        /* Inserts original size photo into database with either the matching size id
        or null size if there is none, and gets its generated primary key.*/
        ori = uploadSize(image, ext, libraryId, location, itm, originalsize ? sizes[originalsize].id : "", usr, "");
        
        // For each of the sizes, uploads an image scaled to the appropriate size. Multi-threaded.
        transaction
        {
            sizes.each(function(x) {
                if(x.id != sizes[originalsize]?.id ?: 0)
                {
                    uploadSize(
                        image=resizeImage(img, x.heightMin, x.heightMax, x.widthMin, x.widthMax, x.aspectMin, x.aspectMax),
                        extension=ext,
                        libraryId=libraryId,
                        location=location,
                        itemid=itm,
                        size=x.id,
                        requestingUserId=usr,
                        original=ori
                    );
                }
            }, true);
        }
    }

    /**
     * Given an image and an array of sizes, gets the index of the
     * first size from the array that matches the constraints.
     *
     * @image image struct for which to obtain the size id
     * @sizes array of sizes, namely from either PhotoLibrary::getSizes or Photo::getAllSizes.
     */
    public static numeric function getSizeFromDimensions(struct image, array sizes)
    {
        // Changes scope for use in nested function.
        img = arguments.image;
        // Finds the first size id matching the constraints
        return arguments.sizes.find((x) => {
            return img.width >= x.widthMin && img.width <= x.widthMax && img.height >= x.heightMin && img.height <= x.heightMax && img.width / img.height >= x.aspectMin && img.width / img.height <= x.aspectMax;
        });
    }

    /**
     * Checks if a recently uploaded file is an image.
     * 
     * @file file to validate as image
     * @sep platform file separator (defaults to \ due to windows)
     */
    public static Boolean function validateImage(struct file, string sep = "\")
    {
        tempfile = arguments.file.ServerDirectory & arguments.sep  & arguments.file.ServerFile;
        return listFind("jpg,jpeg,gif,png",arguments.file.serverFileExt) && listFind("image/png,image/jpg,image/gif,image/jpeg", FileGetMimeType(tempfile, true)) && isImageFile(tempfile);
    }

    /**
     * Generates a new file extension (without a dot) for a given file
     * that matches its MIME type.
     * 
     * i.e. if an image file's extension is ".jpg", but its MIME type is "image/jpeg",
     * it returns "jpeg".
     * 
     * @file File from which to generate a MIME type.
     * @sep platform file separator (defaults to \ due to windows)
     */
    public static string function getMimeFriendlyExtension(struct file, string sep = "\")
    {
        return ArrayLast(fileGetMimeType(arguments.file.ServerDirectory & arguments.sep & arguments.file.ServerFile, true).Split("/"));
    }

    /**
     * Uploads a specific size of an image. returns the database key of the image inserted
     *
     * @image ColdFusion image
     * @extension File format of an image without the . , i.e. "png"
     * @libraryId ID of library in which the image is being stored
     * @location File folder in which to store the image (or empty for SQL blob storage)
     * @itemId Numeric ID of a given item.
     * @size SQL ID of the specific size to update.
     * @requestingUserId ID of the user requesting to upload.
     * @original photo ID of the original image
     * 
     * @hint returns the database key of the image inserted
     */
    public static numeric function uploadSize(image, extension, libraryId, location, itemId, size, requestingUserId, original = 0)
    {
        var blob = '';

        // If using blob storage, prepare image blob for upload.
        if(arguments.location == "")
        {
            // Serializes image for upload
            blob = imageGetBlob(arguments.image);
        }

        
        // Prepare query SQL.
        qsql = 'INSERT INTO photos (libraryId, type, itemId, photoSize, modifyUser, modifyDate, createUser, createDate, blob, original)
        VALUES (:libraryId, :type, :itemId, :photoSize, :modifyUser, :modifyDate, :createUser, :createDate, :blob, :original)';
        
        // Set up query parameters.
        params={
            libraryId:{type:'bigint', value=arguments.libraryId},
            type:{type:'nvarchar', value=arguments.extension},
            itemId:{type:'bigint', value=arguments.itemId},
            photoSize:{type:'bigint', value=arguments.size},
            modifyUser:{type:'bigint', value=arguments.requestingUserId},
            createUser:{type:'bigint', value=arguments.requestingUserId},
            modifyDate:{type:'timestamp', value=now()},
            createDate:{type:'timestamp', value=now()},
            blob:{type:'blob', value=blob},
            original:{type:'bigint', value=arguments.original}
        }
        
        // Inserts photo into database, returning a result with the generated key.
        queryExecute(qsql,params,{datasource="photos", result="result"});

        // If using file storage, store a file ( location\photoid.fileextension )
        if(arguments.location != "")
        {
            saveToFile(arguments.image, location, result.generatedKey, arguments.extension);
        }

        // Returns the photo ID generated in the database.
        return result.generatedKey;
    }

    /**
     * Saves an image to file ( location\photoid.extension )
     */
    public static function saveToFile(struct image, string location, numeric photokey, string extension)
    {
        cfimage(action="write" source=arguments.image, destination="#arguments.location#\#arguments.photokey#.#arguments.extension#", overwrite=true);
    }

    /**
     * Resizes an image to fit a set of constraints.
     * 
     * @image Image to upload in blob format.
     * 
     * @heightMin The minimum possible height for the image.
     * @heightMax The maximum possible height for the image. (0 to not constrain)
     * 
     * @widthMin The minimum possible width for the image.
     * @widthMax The maximum possible width for the image. (0 to not constrain)
     * 
     * @aspectMin The minimum possible aspect ratio (width/height) for the image; namely, how tall an image can be vs its width.
     * @aspectMax The maximum possible aspect ratio (widht/height) for the image; namely, how wide an image can be vs its height. (0 to not constrain)
     */
    public static struct function resizeImage(struct image, numeric heightMin, numeric heightMax, numeric widthMin, numeric widthMax, numeric aspectMin, numeric aspectMax)
    {
        image = Duplicate(arguments.image)

        // Ensures image fits proper aspect ratio.
        if(arguments.aspectMin != '' && image.Width / image.Height < arguments.aspectMin)
        {
            // Image too tall. Squashes height to comply with minimum aspect ratio.
            imageResize(image, image.width, image.width / arguments.aspectMin);
        }
        else if (arguments.aspectMax != '' && arguments.aspectMax != 0 && image.Width / image.Height > arguments.aspectMax && arguments.aspectMax != 0)
        {
            // Image too wide. Limit width to maximum.
            imageResize(image, aspectMax * image.height, image.height);
        }
        
        // Ensures image is proper height.
        if(image.Height < arguments.heightMin)
        {
            imageResize(image, '', arguments.heightMin);
        }
        else if(image.Height > arguments.heightMax && arguments.heightMax != 0)
        {
            imageResize(image, '', arguments.heightMax);
        }

        // Ensures image is proper width.
        if(image.Width < widthMin)
        {
            imageResize(image, arguments.widthMin, '');
        }
        else if(image.Width > widthMax && arguments.heightMax != 0)
        {
            imageResize(image, arguments.widthMax, '');
        }

        return image;
    }

    /**
     * Saves the file from a form to the temporary directory and gets its file struct.
     * Returns {} if no file.
     * 
     * @frm The form uploaded to the application.
     */
    public static struct function saveTempFile(frm)
    {
        if(structKeyExists(frm, "upload") && len(frm.upload))
        {
            return fileUpload(getTempDirectory(), "form.upload", "", "makeUnique");
        }
        else
        {
            // no file
            return {};
        }
    }

    /**
     * Updates an image, rescaling to update alternate sizes.
     *
     * @image new image to replace the old image with
     * @extension file extension of new image
     * @id photo id of the image being updated
     * @requestingUserId the user that updated the image
     * @original image this image is a resize of 
     */
    public static function updateImage(struct image, string extension, numeric id, numeric requestingUserId, numeric original)
    {
        // Gets original photo id and information about other sizes
        ori = Photo::getOriginal(arguments.id);
        if(!ori) {return;} // Ensures photo exists in DB.

        allsizes = Photo::getAllSizes(ori);

        // Gets library information
        libraryId = Photo::getLibraryId(ori);
        loc = PhotoLibrary::getLocation(libraryId);

        // Saves argument in a different scope to be referenced within updateSize
        img = arguments.image;
        ext = arguments.extension;
        usr = arguments.requestingUserId;

        // Checks if old original matched a size
        newSize = getSizeFromDimensions(img, allsizes);

        // searches the array for items without a size.
        nullsize = allsizes.find((x) => x.sizeid == "");

        // Checks if old original matched a size
        if(!newSize)
        {
            // the uploaded photo doesn't match a size.

            // checks for null sized image, gets its id. if the uploaded image isn't sized and the old original isn't sized, carry out a normal update. 
            nullSize = allsizes.find((x) => x.widthMin == "" || x.widthMax == "" || x.heightMin == "" || x.heightMax == "" || x.aspectMin == "" || x.aspectMax == "");

            // if the size isn't null, insert new original without a size id and retrieves generated key
            if(!nullSize)
            {
                query name="item" datasource="photos" returntype="array"
                params={id:{type:'bigint', value=id}}
                {
                    echo("SELECT itemid FROM photos WHERE id=:id");
                }

                ori = uploadSize(img, ext, libraryId, loc, item[1]?.itemid ?: 0, "", usr, "");
            }
        }
        else 
        {
            // the uploaded photo matches a size.
            
            // if the old original didn't have a size, but the new original does, deletes the original photo and removes it from the array of photos to be updated.
            if(nullsize)
            {
                // generate new original id to fit the size.
                ori = allsizes[newsize].id;

                Photo::delete(allsizes[nullsize]?.id ?: 0, false, true);
                arrayDeleteAt(allsizes, nullsize);
            }
        }

        // Updates the images in the database.
        transaction
        {
            // For each alternate size, it rescales and updates the photo. Multi-threaded.
            allsizes.each(function(x) {
                updateSize(
                    image=x.id == ori ? img : resizeImage(img, x.heightMin, x.heightMax, x.widthMin, x.widthMax, x.aspectMin, x.aspectMax), // if not original, resize.
                    location=loc,
                    oldextension=x.ext,
                    newextension=ext,
                    photoid=x.id,
                    requestingUserId=usr,
                    original=x.id == ori ? "" : ori
                );
            }, true);
        }
    }

    /**
     * Updates a specific image size in the database.
     * Also replaces the file (or blob depending on library location settings).
     * 
     * @image The image to save.
     * @extension The file type of the image to save.
     * @photoid The id of the image to update.
     * @requestingUserId The ID of the user that requested the update.
     * @original id of the photo prior to resizing, or "" for empty.
     */
    public static function updateSize(struct image, string location, string oldextension, string newextension, numeric photoid, numeric requestingUserId, original)
    {
        // Initializes blob as empty
        var blob = '';

        // Determines if the library uses file or blob storage
        if(arguments.location == '')
        {
            // library uses blob storage. converts the image to blob format for storage
            blob = imageGetBlob(arguments.image);
        }
        else
        {
            // Deletes the old image.
            if(original != "" || arguments.newextension != arguments.oldextension)
            {
                cffile(action="delete", file=arguments.location&'\'&arguments.photoid&'.'&arguments.oldextension);
            }
            
            
            
            // library uses file storage. overwrites the file with a new one.
            saveToFile(arguments.image, arguments.location, arguments.photoid, arguments.newextension);
        }

        /* Where where the photo id matches, updates:
            - ModifyDate
            - ModifyUser
            - Blob
            - Extension */
        query name="updatePhoto" datasource="photos"
        params = {
            modifyDate:{type:'timestamp', value=now()},
            modifyUser:{type:'bigint', value=arguments.requestingUserId},
            blob:{type:'blob', value=blob},
            type:{type:'nvarchar', value=arguments.newextension},
            photoid:{type:'bigint', value=arguments.photoid},
            original:{type:'bigint', value=arguments.original}
        }
        {
            echo("
                UPDATE photos
                SET modifyDate=:modifydate, modifyUser=:modifyUser, blob=:blob, original=:original, type=:type
                WHERE id=:photoid
            ");
        }
    }
}