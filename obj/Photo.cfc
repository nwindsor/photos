component {

    /**
     * Gets the original id of a given photo. Returns 0 if image not found.
     * 
     * @id ID of the photo to get the original of
     */
    public static numeric function getOriginal(numeric id)
    {
        // Gets the original photo where the id matches the argument.
        query name="photo" datasource="photos" returntype="array"
        params={id: {type:'cf_sql_bigint', value:arguments.id}}
        {
            echo("SELECT original FROM photos WHERE id=:id");
        }
        
        // if original is empty, return 0. if it's null, return id. if it isn't null, return original. 
        return isEmpty(photo) ? 0 : photo[1].original == "" ? arguments.id : photo[1].original;
    }

    /**
     * Deletes all the sizes of a photo based on an original size.
     * If no photo was found, it does nothing.
     *
     * @id photo ID of original size.
     */
    public static function delete(numeric id, boolean children = true, boolean isOriginal = true)
    {
        // if the id is zero, do nothing.
        if(!arguments.id) {return;} 

        // Removes references to the image and its derivates (i.e. thumbnails) from databases
        removeReferences(arguments.id, arguments.children);

        // Tries to get library id of photo.
        lib = getLibraryId(arguments.id);
        if(!lib) {return;}        

        // Checks if it's stored as a file or not
        loc = PhotoLibrary::getLocation(getLibraryId(arguments.id));
        if(loc != "")
        {   
            // It's a file. removes it.

            // Gets an array of sizes
            query name="pics" datasource="photos" returntype="array"
            params= {id:{type:'bigint', value=arguments.id}}
            {
                echo("SELECT id, type FROM photos WHERE id=:id" & (arguments.children ? " OR original = :id" : ""));
            }

            //throw(type="ForcedException", message="#arrayLen(pics)#", detail="breakpoint.");

            // For each matching photo, deletes the file ( location\id.type ). multi-threaded.
            pics.each(
                (x) => filedelete(loc & "\" & x.id & "." & x.type),
                true);
        }

        // Deletes photos from database.
        query name="delete" datasource="photos"
        params={id: {type:'cf_sql_bigint', value:arguments.id}}
        {
            echo("DELETE FROM photos WHERE id=:id" & (arguments.children ? " OR original = :id" : ""));
        }
    }

    /**
     * Given an original photo, gets the sizes of all instances (including the original)
     * in terms of ID, extension, and size data, stored in array form:
     * {{id, ext, heightMin, heightMax, widthMin, widthMax, aspectMin, aspectMax}, ...}
     * 
     * @photoId the original photo from which to find derivatives sizes
     */
    public static array function getAllSizes(numeric photoId)
    {
        // NECESSARY : pid, type
        // OPTIONAL : [sizedata]

        query name="sizes" datasource="photos" returntype="array"
        params= {original:{type:'bigint', value=arguments.photoId}}
        {
            echo("
                SELECT p.id as id, p.type as ext, s.id as sizeid, heightMin, heightMax, widthMin, widthMax, aspectMin, aspectMax
                FROM photos p
                LEFT JOIN
                photosizes s
                ON s.id = p.photoSize
                WHERE p.original = :original or p.id = :original
            ");
        }

        return sizes;
    }

    /**
     * Gets the id of a library based on a photo id or 0 if invalid).
     *
     * @itemId Item from which to get the containing library info
     */
    public static numeric function getLibraryId(numeric photoId)
    {
        query name="item" datasource="photos" returntype="array"
        params={id:{type:'bigint', value:arguments.photoId}}
        {
            echo("
                SELECT libraryId
                FROM photos
                WHERE id=:id
            ");
        }
        
        return arrayLen(item) ? item[1].libraryId : 0;
    }

    /**
     * Removes all references to a photo (and its derivative sizes) in the database.
     * Currently removes: Thumbnails, "Original" references if applicable
     * 
     * @id photo id of the image to remove references of (the original id if children is true).
     * @includeChildren removes references to all automatically generated sizes as well as the specified image.
     * @isOriginal is this the original id of the photo? used to remove references from generated sizes if includeChildren is false. must be true if includeChildren is true. 
     */
    public static function removeReferences(numeric id, boolean includeChildren = true, boolean isOriginal=true)
    {
        transaction
        {

            // Remove thumbnail
            query name="references" datasource="photos"
            params={id:{type:'bigint', value:arguments.id}}
            {
                // update thumbnail in items to null where thumbnail = :id (includes original=id if children = true.)
                echo("
                    UPDATE items
                    SET items.thumbnail = NULL
                    FROM items
                    INNER JOIN photos
                    ON items.thumbnail = photos.id
                    WHERE photos.id = :id" & (arguments.isOriginal && arguments.includeChildren ? " OR photos.original = :id" : "")
                    );
            }

            // if this is an original copy of an image, remove references to it in the photos database.
            if(isOriginal && !includeChildren)
            {
                query name="original" datasource="photos"
                params={id:{type:'bigint', value:arguments.id}}
                {
                    echo("
                        UPDATE photos
                        SET original = NULL
                        WHERE original = :id
                    ");
                }
            }
        }
    }
}