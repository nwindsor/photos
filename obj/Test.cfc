component displayname="Test" {
    property string a;

    public function init(string argu = "d")
    {
        variables.instance.a = arguments.argu;
    }

    public string function sayAToWorld()
    {
        return variables.instance.a & ", World!";
    }

    /**
     * Gets... _an_ image... to use for unit testing.
     */
    public static struct function getImage()
    {
        return imageRead("C:\Projects\photos\gamers\shion.jpg");
    }

    /**
     * Checks if a recently uploaded file is an image, but throws an exception
     * for each respective check failed
     *
     * @file file to validate as image
     * @sep platform file separator (defaults to \ due to windows)
     */
    public static Boolean function validateImageDebug(struct file, string sep="\")
    {
        tempfile = arguments.file.ServerDirectory & arguments.sep  & arguments.file.ServerFile;

        if(!listFind("jpg,jpeg,gif,png",arguments.file.serverFileExt))
        {
            throw(type="ForcedException", message="Invalid extension: #arguments.file.serverFileExt#", detail="oopsy woopsy! uwu we did a fucky wucky!");
        }
        else if(!listFind("image/png,image/jpg,image/gif,image/jpeg", FileGetMimeType(tempfile, true)))
        {
            throw(type="ForcedException", message="Invalid MIME: #FileGetMimeType(tempfile, true)#", detail="oopsy woopsy! uwu we did a fucky wucky!");
        }
        else if(!isImageFile(tempfile))
        {
            throw(type="ForcedException", message="File not an image!", detail="oopsy woopsy! uwu we did a fucky wucky!");
        }

        return listFind("jpg,jpeg,gif,png",arguments.file.serverFileExt) != 0 && listFind("image/png,image/jpg,image/gif,image/jpeg", FileGetMimeType(tempfile, true)) != 0 && isImageFile(tempfile);
    }


    /* breakpoint
    throw(type="ForcedException", message="breakpoint", detail="breakpoint.");
    */
}