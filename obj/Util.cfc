component {
    /**
     * Parses a url string into an argument struct.
     */
    public static struct function UrlArgs(string encodedUrl)
    {
        // Decodes funky URL characters
        decoded = urlDecode(arguments.encodedUrl);

        /* Gets the arguments from the url and splits them up into an array.
        *  Creates a struct of this array where anything before "=" is the key, and after is the value.
        */
        s = arrayLast(decoded.split('\?'));
        spl = s.split('&');
        
        return arrayReduce(spl,
            function(carry, value)
            {
                spl = value.split("=");
                return carry.insert(spl[1],spl[2]);
            },StructNew()
        );
    }
}