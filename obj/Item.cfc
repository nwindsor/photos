component {
    /**
     * Gets the id of a library based on an item id.
     *
     * @itemId Item from which to get the containing library info
     */
    public static numeric function getLibraryId(numeric itemId)
    {
        query name="item" datasource="photos" returntype="array"
        params={id:{type:'bigint', value:arguments.itemId}}
        {
            echo("
                SELECT libraryId
                FROM items
                WHERE id=:id
            ");
        }

        return item[1].libraryId;
    }

    /**
     * Gets an item given a library ID and a PK from the library's table.
     * 
     * If no item exists for the given pair, it creates a new item in the database returning the inserted item id.
     * If one already exists for this combination, it returns the id of the existing item for this pair.
     *
     * @libraryId Library in which to insert the 
     * @foreignPK Primary key from the library's item table. Typically 'id'.
     */
    public static numeric function getItem(numeric libraryId, numeric foreignPK)
    {
        // if either are 0, returns 0.
        if(!libraryId || !foreignPK)
        {
            return 0;
        }

        // Scans database to make sure that no item already exists matching the library id and foreign PK
        query name="exists" datasource="photos" returntype="array"
        params={lid:{type:'bigint', value=arguments.libraryId}, sid:{type:'bigint', value=arguments.foreignPK}}
        {
            echo("SELECT id FROM items WHERE libraryId=:lid AND sourceId=:sid");
        }

        // If an item already exists for this library and foreign PK, return that.
        if(!isEmpty(exists))
        {
            return exists[1].id;
        }
        
        // Inserts record into database
        queryExecute("
                INSERT INTO items (libraryId, sourceId)
                VALUES (:lid, :sid)
            ",
            {
                lid:{type:'bigint', value=arguments.libraryId},
                sid:{type:'bigint', value=arguments.foreignPK}
            },
            {
                datasource="photos",
                result="result"
            }
        );

        // Returns generated primary key
        return result.generatedKey;
    }

    /**
     * Gets an array of structs representing the items.
     * Fields include all the visible/sortable fields, along with:
     * 
     * - "numphotos" (number of original photos of a given item),
     * - "thumbnail", representing the default photo id that should be displayed in the gallery
     * - "galleryitemid", representing the itemId in the gallery's items table,
     * - "sourceitemid", representing the id field in the source table
     * 
     * @sort Table column to sort the list by
     * @table The name of the item source's table (as used in the SQL database)
     * @visibleColumns Data columns the user of the site can view and use to sort items in the gallery site.
     * @sourcePKField The name of the primary key field of the foreign database. defaults to "id".
     * @todo should you include items from the source database that aren't in the items database?
     */
    public static array function getItems(numeric libraryid, string sort="", bool todo=true)
    {
        // Gets info on the library's source table
        query name="library" datasource="photos" returntype="array"
        params={id:{type:'bigint', value=arguments.libraryid}}
        {
            echo("
                SELECT name,visibleColumns,sourcePKName
                FROM photoLibraries
                WHERE id=:id
            ");
        }
        
        // If no library is found, return an empty array.
        if(isEmpty(library))
        {
            return library;
        }

        // Validates sort string if it exists (functionality moved to different function)
        /*if(arguments.sort != "")
        {
            // rejects if the sort argument isn't part of the arguments list
            if(!library[1].visibleColumns.split(", ").contains("t." & arguments.sort))
            {
                return ArrayNew(1);
            }
        }*/   

        // Formats visible columns from an into an SQL-friendly list string.
        visicols = ArrayReduce(library[1].visibleColumns.split(", "), function(carry, value) {
            return isNull(value) ? carry : carry & 't.' & value & ', ';
        }, '');
        

        // todo: merge queries.

        srcfield = library[1].sourcePKName == "" ? "id" : library[1].sourcePKName

        /* Gets:
            - The desired source rows for filtering and naming
            - The preferred photo / thumbnail of an item, in id form(if applicable)
            - The number of photos of an item, 0 if null.
            - The gallery item ID of the specified item, as "galleryid"
            - The source item ID of the specified item, as "sourceid"

            If "todo" is true, it includes items from the source DB that aren't yet in the items db.

            if a sort argument is passed, it sorts by said argument (ORDER BY [argument].)
        */  
        query name="photodata" datasource="photos" returntype="array"
        params={lid:{type:'bigint', value=arguments.libraryid}}
        {
            echo("
                SELECT #visicols#i.thumbnail as thumbnail, ISNULL(p.photocount, 0) as numphotos, i.id as galleryitemid, t.#srcfield# as sourceitemid
                FROM #library[1].name# t
                #arguments.todo ? "LEFT" : "INNER"# JOIN items i
                ON t.#srcfield# = i.sourceid AND i.libraryid = :lid
                LEFT JOIN
                (
                    SELECT itemid, count(id) AS photocount
                    FROM photos
                    WHERE original IS NULL
                    GROUP BY itemid
                ) AS p
                ON i.id = p.itemid
                LEFT JOIN photos h
                ON i.thumbnail = h.id 
                #arguments.sort == "" ? "" : "ORDER BY " & arguments.sort#
            ");
        }

        return(photodata);
    }

    /**
     * Ensures a given string is valid for use in an SQL SORT BY.
     * String can be one or two words (separated by a space) and ending with either "ASC" or "DESC"
     * 
     * @sort sort string to validate.
     * @libraryId 
     */
    public static boolean function validateSort(string sort, numeric libraryId)
    {
        if(arguments.sort == "")
        {
            return false;
        }

        // ensures that the string is either one or two words by splitting it across a space
        spl = arguments.sort.listToArray(" ", true);

        switch(arrayLen(spl))
        {
            case 1:
                // only one word, makes sure that it's a column in the specified library
                return PhotoLibrary::validateColumn(spl[1], arguments.libraryId);
                break;
            case 2:
                // 2 words, makes sure that the second word is either ASC or DESC
                if(spl[2] == "ASC" || spl[2] == "DESC")
                {
                    // makes sure that the first word is a column in the specified library
                    return PhotoLibrary::validateColumn(spl[1], arguments.libraryId);
                }
                else
                {
                    return false;
                }
                break;
            default:
                // invalid word count in sort string
                return false;
        }
    }

    /**
     * Gets an array of photo structs for every 
     */
    public static array function getPhotos(numeric itemid)
    {
        query name="qry" datasource="photos" returntype="array"
        params={id:{type:'bigint', value=arguments.itemid}}
        {
            echo("
                SELECT id, type, createuser, createdate, modifyuser, modifydate
                FROM photos
                WHERE itemid = :id AND original IS NULL
            ");
        }

        return qry;
    }
}