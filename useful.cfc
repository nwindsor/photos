<cfcomponent name="warehouseUsefulFunctions">
	<cfinclude template="/includes/timezone.cfm">
    <!---
	:::
		Function: setServerSettings
		Arguments: none
		Returns: none
		Description: Retrieves the server settings defined in the database and applies them to the application variable scope for app use
	:::
	--->
    <cffunction name="setServerSettings" access="public" returntype="void">
            <cfset Application.dsn="warehouse">
            
			<cfquery datasource="#application.dsn#" name="getServer">
 			SELECT * FROM server_settings
            </cfquery>
            <cfloop list="#getServer.columnList#" index="i">
            	<cfset Application[i]=getServer[i]>
            </cfloop> 
            <cfquery datasource="#application.dsn#" name="getLDAP">
            SELECT * FROM server_ldap
            </cfquery>
            <cfset Application.ldap=getLDAP>   
            <cfset Application.salt="Ware$h0use$D!v">
            <cfset Application.encryption_key="Port*Li0nnM$^">
            <cfset Application.docRoot="E:\webroots\warehouse.afsifilters.com">
            <cfset Application.erpPassword=decrypt(application.erpPassword,application.encryption_key)>
            <cfquery datasource="#application.dsn#" name="getJDEPrefix">
            SELECT description FROM list_element WHERE name=<cfqueryparam cfsqltype="cf_sql_varchar" value="#application.jdedsn#" /> AND list_id=2
            </cfquery>
            <cfset Application.jdeTablePrefix=listGetAt(getJDEPrefix.description,2,';')>
            <cfset Application.aisRoot=listGetAt(getJDEPrefix.description,3,';')>
            <cfset Application.jdeEnv=listGetAt(getJDEPrefix.description,4,';')>
            <cfset Application.jdeToolsDB=listGetAt(getJDEPrefix.description,5,';')>
		<cfreturn>
    </cffunction>
    
    <cffunction name="getHost">
    	<cfargument name="address" required="yes" />
        <!---<cfreturn "">--->
		<cfscript>
        /**
         * Performs a DNS lookup on an IP address.
         * 
         * @param address      IP address to look up. 
         * @return Returns a domain name. 
         * @author Ben Forta (ben@forta.com) 
         * @version 1, December 19, 2001 
         */
           // Variables
           var iaclass="";
           var addr="";
           
           // Init class
           iaclass=CreateObject("java", "java.net.InetAddress");
        
           // Get address
           addr=iaclass.getByName(arguments.address);
        
           // Return the name  
		   //dump(iaclass.getByName('10.175.1.145').getHostName());   
		   //dump(addr.getAllByName());  
           return addr.getHostName();
        </cfscript>
	</cffunction>
	
    	<!---
	:::
		Function: cleanupWhiteSpace
		Arguments: strIn -- simple varchar for cleanup
		Returns: strOut -- cleaned up varchar
		Description: Removes nested paragraphs and strange HR behaviors common from the WYSIWYG save.  Helps make messages more readable
			
	---->
    
    <cffunction name="cleanupWhiteSpace">
        <cfargument name="strIn">
        <!--- remove extra blank lines --->
        <cfset strOut=reReplace(arguments.strIn,"<p>&nbsp;</p>","","ALL")>
        <!--- remove nested <p> --->
        <cfset strOut=reReplace(strOut,"<p>&nbsp;[\s]*<p>","<p>","ALL")>
        <cfset strOut=reReplace(strOut,"<p>[\s]*<p>","<p>","ALL")>
        <!--- remove closing nested </p> --->
        <cfset strOut=reReplace(strOut,"</p>[\s]*</p>","</p>","ALL")>
        <!--- remove weird <hr> behavior --->
        <cfset strOut=reReplace(strOut,"<p>&nbsp;[\s]*<hr /></p>","<hr>","ALL")>
    <cfreturn strOut>
    </cffunction>

    <!---
	:::
		Function: cleanup
		Arguments: none
		Returns: none
		Description: Executes system cleanup tasks: email reminders, delete old messages, delete old drafts, cleanup logs
	:::
	--->
    <cffunction name="cleanup" access="public" returntype="void">
    	<cfquery datasource="#application.dsn#" name="insLog">
        INSERT INTO log_taskrun(attempt_ts) values(SYSUTCDATETIME())
        </cfquery>
       
        <!--- logs --->
        <cfset temp=dumpLogs(application.logRetentionDays)>
                
    </cffunction>
    
    <cffunction name="validateEmail" access="public" returnType="string">
    	<cfargument name="email" required="yes" type="string">
        
		<cfif arguments.email eq ''><cfreturn "Blank is not allowed"></cfif>
        <cfif not find('@',arguments.email)><cfreturn "Missing @ symbol."></cfif>
        <cfif not find('.',arguments.email)><cfreturn "Missing . symbol."></cfif>
        <cfif find(' ',arguments.email)><cfreturn "Spaces are not allowed"></cfif>
        <cfif listlen(arguments.email,'@') neq 2><cfreturn "Should have text surrounding both sides of the @ symbo."></cfif>
    	<cfreturn "OK">
    
    </cffunction>
    
    <cffunction name="validatePhone" access="public" returnType="string">
    	<cfargument name="phone" required="yes" type="string">
        
		<cfif arguments.phone eq ''><cfreturn "Blank is not allowed"></cfif>
        <cfif NOT isNumeric(phone)><cfreturn "Should be only numbers."></cfif>
        <cfif len(arguments.phone) neq 10><cfreturn "US 10 digit numbers only including area code."></cfif>
        
    	<cfreturn "OK">
    
    </cffunction>
    
    <!---
	:::
		Function: dumpLogs
		Arguments: retentionDays -- number of days after which logs are wiped
		Returns: none
		Description: Clears old log entries
	:::
	--->
    <cffunction name="dumpLogs" access="public" returnType="void">
    	<cfargument name="retentionDays" required="yes" type="numeric" />
        <cfinvoke component="list" method="list_data" list_id="79" returnVariable="logTables"></cfinvoke>
        <cfset logtables=valueList(logTables.name)>
        <cfloop list="#logTables#" index="i">
        	<cfset dtCompare=dateAdd('d',retentionDays*-1,now())>
        	<cfquery datasource="#application.dsn#" name="wipeLog">
            DELETE FROM #i# WHERE attempt_ts < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#dtCompare#" />
            </cfquery>
        </cfloop>
        	<cfset dtCompare=castToUTC(dateAdd('d',-2,now()),application.timezone)>
        	<cfquery datasource="#application.dsn#" name="wipeLog">
            DELETE FROM log_connectionRate WHERE attempt_ts < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#dtCompare#" />
            </cfquery>
    </cffunction>
    
    <!---
	:::
		Function: stripHTML
		Arguments: inStr - string
		Returns: instr argument with all HTML tags stripped out
		Description: Eliminates HTML tags to make display of user accessible content display-safe
	:::
	--->
    <cffunction name="stripHTML" access="public" returntype="string">
    	<cfargument name="inStr" type="string" />
        <cfreturn REReplaceNoCase(arguments.inStr,"<[^>]*>","","ALL")>
    </cffunction>
    
    
</cfcomponent>