<cfcomponent name="auth">
	<cffunction name="lockout" access="public">
    	<cfargument name="email" required="yes" />
    	<cfif session.loginAttempts gte 5>
    		<cfset unlockTime=dateadd('n',10,now())>    
        	<cfset session.lockoutExpires = unlockTime>
           	
            <cfquery datasource="#application.dsn#" name="setLockout">
            UPDATE users SET lockedout=1,lockoutExpires=<cfqueryparam cfsqltype="cf_sql_timestamp" value="#session.lockoutExpires#" /> WHERE email=<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.email#" />
            </cfquery>
        </cfif>
    </cffunction>
</cfcomponent>