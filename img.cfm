<cfparam name="errorcount" type="numeric" default=0/>
<cfparam name="blob" type="binary" default=""/>
<cfparam name="type" default=""/>
<cfparam name="link" default=""/>

<cfscript>
    access = false; // defaults to no access
    
    /**
     * Image API
     * Gets an image, usable in "img src=". Respects user permissions.
     * 
     * There are 3 ways to get images using the image API.
     * 
     * 1. Use the image id to get a specific image.
     * photos.afsifilters.com/img.cfm?id=[photoid]
     * 
     * 2. Use an item id to find the first image it can of a given item.
     * photos.afsifilters.com/img.cfm?item=[itemid]
     * 
     * 3. Use a library id to find the first image it can in a given library.
     * photos.afsifilters.com/img.cfm?lib=[libraryid]
     */

    /* Validates input and gets photo data from the db:
        link, blob, libraryid, confidentiality
        
        stored in the array "photo"
    */
    if(structKeyExists(url, "id"))
    {
        // TODO: Auth        

        // Validates 
        if(isNumeric(url.id))
        {
            // Queries based on photo id
            query name="photo" datasource="photos" returntype="array"
            params= {id: {type:'cf_sql_bigint', value:'#url.id#'}} 
            {
                echo("
                    SELECT blob, p.id, type, libraryid, l.directory, l.confidentiality
                    FROM photos p
                    INNER JOIN photoLibraries l
                    ON p.libraryId = l.id
                    WHERE p.id=:id
                ");
            }
        }
        else
        {
            // ERROR: ID NOT NUMERIC
            errorcount++;
        }
    }
    /*else if(structKeyExists(url, "i") && structKeyExists(url, "size"))
    {
        // Queries based on item id and size
        query name="photo" datasource="photos" returntype="array"
        params= {item: {type:'bigint', value:'#url.i#'}, size: {type:'varchar', value:'#url.size#'}}
        {
            echo("
                SELECT p.type, p.blob, l.id, l.directory, l.confidentiality
                FROM items i
                INNER JOIN photolibraries l
                ON l.id = i.libraryid
                INNER JOIN photoSizes s
                ON l.id = s.libraryid AND s.name = :size
                INNER JOIN photos p
                ON p.photoSize = s.id AND p.itemid = i.id
                WHERE i.id = :item
            ");
        }
    }*/
    else if(structKeyExists(url, "item"))
    {
        if(isNumeric(url.item))
        {
            query name="photo" datasource="photos" returntype="array"
            params= {id:{type:'bigint', value:url.item}}
            {
                echo("
                    SELECT TOP(1) blob, p.id, type, libraryid, l.directory, l.confidentiality
                    FROM photos p
                    INNER JOIN photoLibraries l
                    ON p.libraryId = l.id
                    WHERE p.itemid = :id
                ");
            }
        }
        else
        {
            errorcount++;
        }
    }
    else if(structKeyExists(url, "lib"))
    {
        if(isNumeric(url.lib))
        {
            // Searches for the first photo that matches a library's info.
            query name="photo" datasource="photos" returntype="array"
            params= {libraryid:{type:'bigint', value:url.lib}}
            {
                echo("
                    SELECT TOP(1) blob, p.id, type, libraryid, l.directory, l.confidentiality
                    FROM photos p
                    INNER JOIN photoLibraries l
                    ON p.libraryId = l.id
                    WHERE p.libraryId = :libraryid
                ");
            }
        }
        else
        {
            errorcount++;
        }        
    }
    else
    {
        // ERROR: Invalid Arguments
        //throw(type="ForcedException", message="fake args", detail="f");
        errorcount++;
    }

    // Checks that data exists and gives user access if they have permissions.
    if(errorcount == 0 && !isNull(photo) && !isEmpty(photo))
    {
        // TODO: Auth

        /* Checks confidentiality.
            0: Green
            1: Yellow
            2: Red
        */
        if(photo[1].confidentiality == 0) // Green (no auth)
        {
            access=true;
        }
        else
        {
            // TODO: Auth
            //throw(type="ForcedException", message="bad auth", detail="f");
            errorcount++;
        }
    }
    else
    {
        // ERROR: NO RESULT
        //throw(type="ForcedException", message="no result", detail="f");
        errorcount++;
    }

    // If the user is authorized. Sets the parameters to match the data.
    if(access)
    {
        // Gets the file extension from the "link" field. (it's just the extension in blob format)
        type = photo[1].type;

        // Is it a link or blob based photo?
        if(isNull(photo[1].blob) || isEmpty(photo[1].blob))
        {
            // The photo is link-based.

            // directory/photoid.png
            link = photo[1].directory & photo[1].id & '.' & type;
        }
        else
        {
            // The photo is blob-based.
            blob = photo[1].blob;
        }
    }

</cfscript>

<!-- Sets headers to blob to return the file -->
<cfif errorcount == 0>
    <!-- There were no errors. Sends image to user.-->
    <cfif isNull(blob) || isEmpty(blob)>
        <!-- blob is null, uses link -->
        <cfcontent reset="Yes" type="image/#type#" file="#link#" />
    <cfelse>
        <!-- blob is not null, uses blob -->
        <cfcontent reset="Yes" type="image/#type#" variable="#blob#" />
    </cfif>
<cfelse>
    <!-- TODO: real error image -->
    <cfcontent reset="Yes" type="image/png" file="C:\Projects\error.png" />
</cfif>