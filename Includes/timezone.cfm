
<cffunction name="castFromUTC" output="No" access="public" returntype="date">  
    <cfargument name="thisDate" required="yes" type="date">
    <cfargument name="thisTZ" required="no" default="GMT">
    
	<cfset variables.tzObj=createObject("java","java.util.TimeZone")>
	<cfset variables.tzList = arrayToList(tzObj.getAvailableIDs())>
    
    <cfscript>
        var timezone=tzObj.getTimeZone(arguments.thisTZ);
        var tYear=javacast("int",Year(arguments.thisDate));
        var tMonth=javacast("int",month(arguments.thisDate)-1); //java months are 0 based
        var tDay=javacast("int",Day(thisDate));
        var tDOW=javacast("int",DayOfWeek(thisDate));	//day of week
        var thisOffset=timezone.getOffset(1,tYear,tMonth,tDay,tDOW,0)/1000;
        return dateAdd("s",thisOffset,arguments.thisDate);
    </cfscript>
</cffunction>


<!--- returns UTC from given date in given TZ, takes DST into account --->
<cffunction name="castToUTC" output="No" access="public" returntype="date">  
	<cfargument name="thisDate" required="yes" type="date">
	<cfargument name="thisTZ" required="no" default="GMT">
	<cfset variables.tzObj=createObject("java","java.util.TimeZone")>
	<cfset variables.tzList = arrayToList(tzObj.getAvailableIDs())>
	<cfscript>
		var timezone=tzObj.getTimeZone(arguments.thisTZ);
		var tYear=javacast("int",Year(arguments.thisDate));
		var tMonth=javacast("int",month(arguments.thisDate)-1); //java months are 0 based
		var tDay=javacast("int",Day(thisDate));
		var tDOW=javacast("int",DayOfWeek(thisDate));	//day of week
		var thisOffset=(timezone.getOffset(1,tYear,tMonth,tDay,tDOW,0)/1000)*-1.00;
		return dateAdd("s",thisOffset,arguments.thisDate);
	</cfscript>
</cffunction>
