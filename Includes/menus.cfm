<cfif isDefined('session.loggedIn') AND session.loggedIn>
<div id="menu">
<nav>
<ul>
<li <cfif find('/transactions/',cgi.SCRIPT_NAME)>class="current"</cfif>><a href="/" >Transactions</a></li>
<li <cfif find('/history/',cgi.SCRIPT_NAME)>class="current"</cfif>><a href="/history/" >History</a></li>
<cfif session.history><li <cfif find('/supervisor/',cgi.SCRIPT_NAME)>class="current"</cfif>><a href="/supervisor" >Supervisor</a>
	<ul style="display:none;">
		<li><a href="/supervisor/sessions.cfm">Active Sessions</a></li>
		<li><a href="/supervisor/message.cfm">Send Message</a></li>
	</ul>
</li></cfif>
<cfif session.admin><li <cfif find('/admin/',cgi.SCRIPT_NAME)>class="current"</cfif>><a href="/admin" >Admin</a></li></cfif>
<li <cfif find('/help/',cgi.SCRIPT_NAME)>class="current"</cfif>><a href="/help">Help</a>
	<ul>
		<li><a href="/help">Basic Use</a></li>
		<li><a href="/help/changes.cfm">Change Log</a></li>
		<li><a href="/help/bugs.cfm">Open Bug List</a></li>
		<li><a href="/help/about.cfm">About</a></li>
	</ul>
</li>
<li><a href="/logout.cfm">Logout</a></li>
</ul>
</nav>
</div>
</cfif>
