<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content='width=device-width, initial-scale=.47, maximum-scale=.75, user-scalable=0' name='viewport' />
<!--- ---><meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="stylesheet" href="/CSS/bootstrap_3.3.7.css">
<link rel="stylesheet" href="/css/smoothness/jquery-ui-1.10.4.custom.min.css" />
<cfoutput>
<link rel="icon" 
      type="image/png" 
      href="https://#application.domainname#/favicon.png">
<link rel="shortcut icon" href="https://#application.domainname#/favicon.png?v=2" />      
</cfoutput>

<link rel="stylesheet" media="screen" type="text/css" href="/JS/colorpicker/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/JS/Magnific-Popup/dist/magnific-popup.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,500,600,600italic,700,800,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" media="screen" type="text/css" href="/JS/DataTables-1.10.4/media/css/dataTables.jqueryui.css" />

<link rel="stylesheet" href="/css/warehouse.css" />
<link rel="stylesheet" href="/css/nav.css" />


<link href="/images/ipad-icons/fork-57x57.png" rel="apple-touch-icon" sizes="57x57"/>
<link href="/images/ipad-icons/fork-72x72.png" rel="apple-touch-icon" sizes="72x72"/>
<link href="/images/ipad-icons/fork-114x114.png" rel="apple-touch-icon" sizes="114x114"/>
<link href="/images/ipad-icons/fork-144x144.png" rel="apple-touch-icon" sizes="144x144"/>
<link href="/images/ipad-icons/fork-144x144.png" rel="icon" sizes="144x144"/>
<link href="/images/ipad-icons/fork-144x144.png" rel="icon" sizes="114x114"/>


<script src="https://kit.fontawesome.com/c4b8e4d3ba.js" crossorigin="anonymous"></script>
<script language="javascript" src="/JS/useful.js"></script>
<script language="javascript" src="/JS/jquery-1.10.2.js"></script>
<script language="javascript" src="/JS/bootstrap_3.3.7.js"></script>
<script language="javascript" src="/JS/jquery-ui-1.10.4.custom.min.js"></script>
<script language="javascript" src="/JS/moment.min.js"></script>
<script type="text/javascript" src="/JS/colorpicker/js/colorpicker.js"></script>
<script language="javascript" src="/JS/jstz-1.0.4.min.js"></script>
<script language="javascript" src="/JS/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>
<script language="javascript" src="/JS/dexie.js"></script><!--- indexedDB wrapper --->
<script language="javascript" src="/JS/DataTables-1.10.4/media/js/jquery.dataTables.min.js"></script>
<script language="javascript" src="/JS/DataTables-1.10.4/media/js/dataTables.jqueryui.min.js"></script>
    <style type="text/css">
    .image-source-link {
      color: #98C3D1;
    }

    .mfp-with-zoom .mfp-container,
    .mfp-with-zoom.mfp-bg {
      opacity: 0.001;
      -webkit-backface-visibility: hidden;
      /* ideally, transition speed should match zoom duration */
      -webkit-transition: all 0.3s ease-out; 
      -moz-transition: all 0.3s ease-out; 
      -o-transition: all 0.3s ease-out; 
      transition: all 0.3s ease-out;
    }
    
    .mfp-with-zoom.mfp-ready .mfp-container {
        opacity: 1;
    }
    .mfp-with-zoom.mfp-ready.mfp-bg {
        opacity: 0.8;
    }
    
    .mfp-with-zoom.mfp-removing .mfp-container, 
    .mfp-with-zoom.mfp-removing.mfp-bg {
      opacity: 0;
    }


    </style>
<script language="javascript">
$(function() {
	$( ".datefield" ).datepicker();
	initTooltip();
	
	$(document).ready(function(){
		initLightbox();
	});
		
	/* create menu navigation */
	$('nav li ul').hide().removeClass('fallback');
	$('nav li').hover(
	  function () {
		$('ul', this).stop().slideDown(100);
	  },
	  function () {
		$('ul', this).stop().slideUp(100);
	  }
	);
			
	
	
});
function initTooltip(){

	$("input[title]").tooltip({
	  // place tooltip on the right edge
	  position: {my:"right-5 middle"},
	  offset: [4, 10],
	  effect: "fade",
	  opacity: 0.7,
	   content: function() {
        return $(this).prop('title');
    }

	  });
	$("label[title]").tooltip({
	  // place tooltip on the right edge
	  position: {my:"right-5 middle"},
	  offset: [4, 10],
	  effect: "fade",
	  opacity: 0.7,
	   content: function() {
        return $(this).prop('title');
    }

	  });
	$("img[title]").tooltip({
	  // place tooltip on the right edge
	  position: {my:"left+30 middle"},
	  offset: [4, 10],
	  effect: "fade",
	  opacity: 0.7,
	  content: function () {
		  return $(this).prop('title');
	  }
	  });
	$("span.tip[title]").tooltip({
	  // place tooltip on the right edge
	  position: {my:"right-5 middle"},
	  offset: [4, 10],
	  effect: "fade",
	  opacity: 0.7,
	  content: function () {
		  return $(this).prop('title');
	  }
	  });
	$("div.tip[title]").tooltip({
	  // place tooltip on the right edge
	  position: {my:"right-5 middle"},
	  offset: [4, 10],
	  effect: "fade",
	  opacity: 0.7,
	  content: function () {
		  return $(this).prop('title');
	  }
	  });
}
function initLightbox(){
	   $('.parent-image-group').each(function(){
		   $(this).magnificPopup({
		  delegate: 'a', // child items selector, by clicking on it popup will open
		  type: 'image',
          fixedContentPos: false,
          fixedBgPos: true,

          overflowY: 'auto',

          closeBtnInside: true,
          preloader: false,
          
          midClick: false,
          removalDelay: 300,
          mainClass: 'mfp-with-zoom mfp-img-mobile',
		  // other options
		  gallery: {
			  enabled: true
		  }/*,
  		  callbacks: {
    
    	  	buildControls: function() {
      		// re-appends controls inside the main container
      		this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
    		}
		  }*/
		});	
	   });
}
</script>
<cfoutput>
    <!--- override CSS elements with custom config from database --->
    <style type="text/css">
    
        <!--- THEME OVERRIDES--->
    <cfif isDefined('session.themeColorOverrides') AND listLen(session.themeColorOverrides,';') gte 4>
        body, ##transactionList, .white-popup-block, input, select, ##syncButton, .dataTable, .modal-content, .jumbotron, .list-group-item{
            background-color:###listGetAt(session.themeColorOverrides,1,";")#; 
            color:###listGetAt(session.themeColorOverrides,2,";")#;        
		}
		
        .dataTable{
            background-color:###listGetAt(session.themeColorOverrides,2,";")#; 
            color:###listGetAt(session.themeColorOverrides,4,";")#;        
		}
        .formTable{
            background-color:###listGetAt(session.themeColorOverrides,1,";")#; 
            color:###listGetAt(session.themeColorOverrides,2,";")#;        
		}
		<!---.formTable, .dataTable,.formTable tr,.dataTable tr,.formTable td,.dataTable td{
            background-color:###listGetAt(session.themeColorOverrides,1,";")#; 
			color:###listGetAt(session.themeColorOverrides,2,";")#;        
		}
		
		.formTable th,.dataTable th{
            background-color:###listGetAt(session.themeColorOverrides,3,";")#; 
			color:###listGetAt(session.themeColorOverrides,4,";")#;        
		}--->
        
        .task_form input[type=text]:focus,.task_form input[type=number]:focus,.task_form input[type=radio]:focus,.task_form input[type=checkbox]:focus,.task_form select:focus,.task_form label.labelfocus{
            color:###listGetAt(session.themeColorOverrides,4,";")#;  
            
        }
        
        .tranBtn, .tranBtn:visited, ##txnsHeader.txn, nav{
            background-color:###listGetAt(session.themeColorOverrides,3,";")#; 
            color:###listGetAt(session.themeColorOverrides,2,";")#;   
      
		}
		.tranBtnHighlight{
			background-color:##ff7e0d;
			color:##01048c;
		}
        
    .yellowHighlight {
        color: ##303030;
    }
        tr.highlight td{
            background-color:###listGetAt(session.themeColorOverrides,3,";")#; 
		}
        tr.highlight td{
            background-color:###listGetAt(session.themeColorOverrides,3,";")#; 
			color:###listGetAt(session.themeColorOverrides,2,";")#; 
		}
		
		.woListHeader,.poListHeader,.abNames{
			color:###listGetAt(session.themeColorOverrides,2,";")#;   
			background-color:###listGetAt(session.themeColorOverrides,3,";")#;
		}
		##resultsTable{
			color:###listGetAt(session.themeColorOverrides,4,";")#;    
		}


    </cfif>
         
        
        
    <!--- GENERAL APPEARANCE --->
    h1{
		<cfif application.headerColor neq ''>background-color:###application.headerColor#;</cfif>
		<cfif application.headerAccentColor neq ''>border-color:###application.headerAccentColor#;</cfif>
		        
    }
	nav ul,nav ul li ul{
		<cfif application.menuBorderColor neq ''>border-color:###application.menuBorderColor#;</cfif>
	}
	nav ul li a,nav ul li ul li a{
		<cfif application.menuTextColor neq ''>color:###application.menuTextColor#;</cfif>
	}
	div.wc,.current{
		<cfif application.menuOverlaySelectedColor neq ''>background-color:###application.menuOverlaySelectedColor#;</cfif>
		<cfif application.menuTextHighlightColor neq ''>color:###application.menuTextHighlightColor#;</cfif>
	}
	div.wc-log{
		<cfif application.menuOverlaySelectedColor neq ''>background-color:###application.menuTextHighlightColor#;</cfif>
		<cfif application.menuTextHighlightColor neq ''>color:###application.menuOverlaySelectedColor#;</cfif>
	}
	div.wc:hover,nav li:hover,nav ul li:hover, nav ul li:hover a, nav ul li:hover a:visited,nav ul li:hover ul li:hover a:visited,nav ul li:hover ul li:hover a, .current{
		<cfif application.menuOverlayHighlightColor neq ''>background-color:###application.menuOverlayHighlightColor#;</cfif>
		<cfif application.menuTextHighlightColor neq ''>color:###application.menuTextHighlightColor#;</cfif>
	}
	nav ul li:hover a:visited ul li,ul li:hover ul li a,nav ul li:hover ul li a:visited{
		background-color:##eeeeee;
		<cfif application.menuTextColor neq ''>color:###application.menuTextColor#;</cfif>
		
	}
	div.wc-log:hover{
		<cfif application.menuOverlayHighlightColor neq ''>background-color:###application.menuTextHighlightColor#;</cfif>
		<cfif application.menuTextHighlightColor neq ''>color:###application.menuOverlayHighlightColor#;</cfif>
	}
	##menuCol{
		<cfif application.menuBackgroundColor neq ''>background-color:###application.menuBackgroundColor#;</cfif>
	}
	a, a:visited, span.spanLink, .ui-widget-content a{
		<cfif application.linkColor neq ''>color:###application.linkColor#;</cfif>
	}
	nav ul li a:visited,nav ul li ul li a:visited{
		<cfif application.menuTextColor neq ''>color:###application.menuTextColor#;</cfif>
	}
	div##copyright{
		<cfif application.headerColor neq ''>background-color:###application.headerColor#;</cfif>
		<cfif application.headerAccentColor neq ''>border-color:###application.headerAccentColor#;</cfif>
		
	}
        
    
    </style>
</cfoutput>
<cfinclude template="/includes/timezone.cfm">

