<cfset message="">
<!---reReplace(url.message,'<[^>]*>','','ALL')--->
<cfif isDefined('url.message')><cfif message neq ''><cfset message=message & '<br><br>' & url.message><cfelse><cfset message=url.message></cfif></cfif>
<cfif isDefined('form.message')><cfif message neq ''><cfset message=message & '<br><br>' & form.message><cfelse><cfset message=form.message></cfif></cfif>


<div id="message">
<cfoutput>#message#</cfoutput>
</div>
<script language="javascript">
var msgBox = $('#message').dialog(
	{
		title: "System Message",
		modal: true,
		closeText: "Dismiss",
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
				}
			},
		autoOpen: false	
	}
);

$(document).ready(function(){
	<cfif message neq ''>msgBox.dialog("open");</cfif>
	funcCall="msgBox.dialog('close')";
	setTimeout(funcCall,4000);	
	
})
</script>
