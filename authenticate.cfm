<!----------------------------------------
Authentication routine


First we review the server settings.  If a base email domain is established for LDAP
and the users domain matches that domain then we auth with LDAP.  During LDAP
authentication we create the database user if it doesn't exist.  If not LDAP then we auth
with the database.  

Once authenticated we use the database values to see if the user is locked out or is an admin
---------------------------------------->
<cfparam name="form.email" default="" />
<cfparam name="form.password" default="" />
<cfparam name="form.mode" default="keyboard" />
<cfparam name="form.redirect" default="index.cfm" />

<cfset form.email = trim(form.email)>
<cfset session.setEmailCookie=form.email>
<cfset session.loggedIn=false>

<cfset session.loginAttempts=session.loginAttempts+1>
<cfset attemptsRemaining=5-session.loginAttempts><cfif attemptsRemaining lt 0><cfset attemptsRemaining=0></cfif>
<cfset passwordSalt="#application.salt#">


<!--- Step 1 : validate the form --->
<cfif form.mode eq 'keyboard'>
	
	<cfset valid=1>
	<cfset validMessage="">
	<cfif trim(form.email) eq ''><cfset valid=0><cfset validMessage = validMessage & "You must enter your username<br >" /></cfif>
	<cfif trim(form.password) eq ''><cfset valid=0><cfset validMessage = validMessage & "You must enter your password<br >" /></cfif>
	<!---<cfif not find('@',form.email)><cfset valid=0><cfset validMessage = validMessage & "Your e-mail should contain an @ symbol<br >" /></cfif>
	<cfif not find('.',form.email)><cfset valid=0><cfset validMessage = validMessage & "Your e-mail should contain at least one period<br >" /></cfif>--->
	<cfif find(' ',form.email)><cfset valid=0><cfset validMessage = validMessage & "Your e-mail should not have any spaces<br >" /></cfif>

<cfelseif form.mode eq 'rfid'>
	<cfset valid=1>
	<cfset validMessage="">
	<!--- set the other expected form values just so the system doesnt have to be validated constantly for if this is RFID or not --->
	<cfset form.email="noone@thisisrfid.com">
	<cfset form.password="noThisIsntValid">
	<cfif trim(application.rfidAuth) neq 1><cfset valid=0><cfset validMessage = validMessage & "RFID based authentication is disabled.<br >" /></cfif>
	<cfif trim(form.rfid) eq ''><cfset valid=0><cfset validMessage = validMessage & "You must scan your fob<br >" /></cfif>
</cfif>

<cfif valid eq 0>
<cfquery datasource="#application.dsn#" name="insLog">
        INSERT INTO log_login(email,attempt_ts,success,ip,reason)
        VALUES(
        	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
            SYSUTCDATETIME(),
            0,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
            'Form submission invalid'
            )
        </cfquery>
    	<cfinvoke component="auth" method="lockout" email="#form.email#"/>
        <cflocation url="login.cfm?message=#validMessage#" />
</cfif>

<!--- Step 2 : check for LDAP auth, if successful then update/insert the DB user with the email,password --->
<cfif listLen(form.email,'@') gte 2>
	<cfset userDomain=listGetAt(form.email,2,'@')>
    <cfset username=listGetAt(form.email,1,'@')>
<cfelse>
	<cfset username=form.email>
    <cfset userDomain="afsifilters.com">
    <cfset form.email = username & "@afsifilters.com">
</cfif>


<cfloop query="#application.ldap#">
	<cfif LDAPEmailDomain neq '' AND userDomain eq LDAPEmailDomain AND session.loggedIn eq false>
    	<cfset foundUser=0>
        <cfset userDN="">
				<cfif emailOrUser eq 'email'>
                    <cfset lookupVal=form.email>
                <cfelse>
                    <cfset lookupVal=username>
                </cfif>
            
                <cftry>
                <cfldap action="query" name="findDN" server="#LDAPServerFQDN#" start="#LDAPBaseDN#" 
                filter="(&(objectclass=person)(#directoryLookupField#=#lookupVal#))" attributes="memberOf,dn,cn,#ldaplastnamefield#,#ldapfirstnamefield#" scope="subtree"
                username="#LDAPAdminDN#" password="#LDAPAdminPassword#" />
                <!--- specified ldap server is down --->
                <cfcatch type="any"><cfbreak></cfcatch>
                </cftry>
                
				<cfif findDN.recordcount neq 0><cfset foundUser=1><cfset userDN=findDN.dn></cfif>
                
                <cfif foundUser neq 0>
                    <cftry>
                    <cfldap action="query" name="checkPass" server="#LDAPServerFQDN#" start="#LDAPBaseDN#" 
                    filter="(&(objectclass=person)(#directoryLookupField#=#lookupVal#))" attributes="dn,cn,memberOf" scope="subtree"
                    username="#userDN#" password="#form.password#" />
                		<cfcatch type="any">
                        
                            <cfinvoke component="auth" method="lockout" email="#form.email#"/>
                            <cfquery datasource="#application.dsn#" name="insLog">
                            INSERT INTO log_login(email,attempt_ts,success,ip,reason)
                            VALUES(
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
                                SYSUTCDATETIME(),
                                0,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
                                'LDAP AUTH: Incorrect password'
                                )
                            </cfquery>
        
    						<cflocation url="login.cfm?message=Login failed, you have #attemptsRemaining# attempts remaining." />
                                            
                        </cfcatch>
                    </cftry>
                    <!---<cfoutput>#findDN.memberOf#</cfoutput><cfabort>--->
                    
                    <cfif isDefined('checkPass') AND checkPass.recordcount neq 0>
                    	<cfif memberOfDN neq ''>
                        	<cfif not find(memberOfDN,findDN.memberOf)>
                        <!---<cfoutput>#memberOfDN#<br />
                        #findDN.memberOf#<br /></cfoutput><cfabort>
                                <cfinvoke component="auth" method="lockout" email="#form.email#"/>
                                <cfquery datasource="#application.dsn#" name="insLog">
                                INSERT INTO log_login(email,attempt_ts,success,ip,reason)
                                VALUES(
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
                                    SYSUTCDATETIME(),
                                    0,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
                                    'LDAP AUTH: Member not of required group'
                                    )
                                </cfquery>
            
                                <cflocation url="login.cfm?message=Login failed, you have #attemptsRemaining# attempts remaining." />
                                ---><cfcontinue>
                            </cfif>
                        </cfif>
						<cfset session.LoggedIn = 1>
                        <cfset session.email=form.email>
    					<!--- test and create their user account in the DB if they don't have one, otherwise update the password to match current accepted LDAP password --->                    
                    	<cfquery datasource="#application.dsn#" name="findUser">
                        SELECT * FROM users WHERE email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />
                        </cfquery>
                        <cfif findUser.recordcount eq 0>
                        	<cfquery datasource="#application.dsn#" name="createUser">
                            INSERT INTO users(email,firstname,lastname,password,timezone,status,allowedTasks,allowedBranch,defaultBranch)
                            VALUES(
                            	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
                            	<cfqueryparam cfsqltype="cf_sql_varchar" value="#finddn[ldapfirstnamefield]#" />,
                            	<cfqueryparam cfsqltype="cf_sql_varchar" value="#finddn[ldaplastnamefield]#" />,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(form.password & passwordSalt)#" />,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#defaultTimezone#" />,
                                'A','74','B10','B10')
                            </cfquery>
                        <cfelse>
                        	<cfquery datasource="#application.dsn#" name="updatePass">
                            UPDATE users
                            SET password=<cfqueryparam cfsqltype="cf_sql_varchar" value="#hash(form.password & passwordSalt)#" />,
                            	lastname=<cfqueryparam cfsqltype="cf_sql_varchar" value="#finddn[ldaplastnamefield]#" />,
                                firstname=<cfqueryparam cfsqltype="cf_sql_varchar" value="#finddn[ldapfirstnamefield]#" />,
                                status='A'
                            WHERE id=<cfqueryparam cfsqltype="cf_sql_bigint" value="#findUser.id#" /> AND status <> 'D'
                            </cfquery>
                        </cfif>
                    </cfif>
                </cfif>
	</cfif>
</cfloop>


<!--- Step 3: Now check for DB auth as well --->

<cfif NOT session.LoggedIn>
	<cfquery datasource="#application.dsn#" name="findUser">
    SELECT * FROM users WHERE <cfif form.mode eq 'keyboard'>email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" /><cfelse>keyFobID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.rfid#" /></cfif>
    </cfquery>
    <cfif findUser.recordcount neq 1>
    	<cfinvoke component="auth" method="lockout" email="#form.email#"/>
        <cfquery datasource="#application.dsn#" name="insLog">
        INSERT INTO log_login(email,attempt_ts,success,ip,reason)
        VALUES(
        	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
            SYSUTCDATETIME(),
            0,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
            'DB AUTH: User not found in DB'
            )
        </cfquery>
        
    	<cflocation url="login.cfm?message=Login failed, you have #attemptsRemaining# attempts remaining." />
    </cfif>
    
    <cfif findUser.status neq 'A'>
    	<cfinvoke component="auth" method="lockout" email="#form.email#"/>
        <cfquery datasource="#application.dsn#" name="insLog">
        INSERT INTO log_login(email,attempt_ts,success,ip,reason)
        VALUES(
        	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
            SYSUTCDATETIME(),
            0,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
            'DB AUTH: User not confirmed or active'
            )
        </cfquery>
        
    	<cflocation url="login.cfm?message=Login failed, you have #attemptsRemaining# attempts remaining." />
    </cfif>

    <cfif findUser.password neq hash(form.password & passwordSalt) AND form.mode eq 'keyboard'>
    	<cfinvoke component="auth" method="lockout" email="#findUser.email#" /><cfquery datasource="#application.dsn#" name="insLog">
        INSERT INTO log_login(email,attempt_ts,success,ip,reason)
        VALUES(
        	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
            SYSUTCDATETIME(),
            0,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
            'DB AUTH: Wrong password'
            )
        </cfquery>
    	<cflocation url="login.cfm?message=Login failed, you have #attemptsRemaining# attempts remaining." />
    </cfif>
    
	<cfset session.userid = findUser.id>
	<cfset session.email = findUser.email>
	<cfset session.loginAttempts = 0>    
	<cfset session.LoggedIn = true>
</cfif>

<!--- Step 4 : Set additional session info about administrative tasks --->

<cfquery datasource="#application.dsn#" name="findUser">
SELECT * FROM users WHERE email = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.email#" />
</cfquery>

<cfset session.userid = findUser.id>
<cfset session.email = findUser.email>
<cfset session.admin=findUser.admin>
<cfset session.history=findUser.history>
<cfset session.userid=findUser.id>
<cfset session.firstname=findUser.firstname>
<cfset session.lastname=findUser.lastname>
<cfset session.timezone=findUser.timezone>
<cfset session.allowedBranch=findUser.allowedBranch>
<cfset session.allowedTasks=findUser.allowedTasks>
<cfset session.defaultBranch=findUser.defaultBranch>
<cfset session.jdeUser=findUser.jdeUser>
<cfset session.keyFobID=findUser.keyFobID>
<cfset session.defaultLabelCopies=findUser.defaultLabelCopies>
<cfset session.printCompletionLabels=findUser.printCompletionLabels>
<cfset session.printReceiptLabels=findUser.printReceiptLabels>
<cfset session.printReverseTxfLabels=findUser.printReverseTxfLabels>
<cfset session.defaultLabelType=findUser.defaultLabelType>
<cfset session.matReqRequired=findUser.matReqRequired>
<cfset session.theme=findUser.theme>
<cfset session.loginAttempts = 0>

<cfquery datasource="#application.dsn#" name="getThemeColors">SELECT * FROM list_element WHERE id=<cfif isNumeric(session.theme)><cfqueryparam cfsqltype="bigint" value="#session.theme#" /><cfelse>120</cfif></cfquery>
<cfset session.themeColorOverrides=getThemeColors.description>
    
<cfif findUser.status eq 'D'>
		<cfinvoke component="auth" method="lockout" email="#findUser.email#" /><cfquery datasource="#application.dsn#" name="insLog">
        INSERT INTO log_login(email,attempt_ts,success,ip,reason)
        VALUES(
        	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
            SYSUTCDATETIME(),
            0,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
            'FAIL: User auth success, user deactivated'
            )
        </cfquery>
		<cfset session.LoggedIn=false>
    	<cflocation url="login.cfm?message=Login failed, you have #attemptsRemaining# attempts remaining." />
</cfif>


<!--- first we get the computer name based on the IP being reported on the CGI variables via a direct nslookup.
	  If that fails we take it from the computers table with the more recent IP address.  This ensures we can
	  match computer to IP --->
      
<cfinvoke component="useful" method="getHost" address="#cgi.remote_addr#" returnvariable="thisHost">
<cfset session.computer = reReplace(thisHost,'.afsifilters.com','')>

    
<!--- Step 5 : Lock them out if they're locked out even if they logged in successfully --->
<cfif findUser.lockedOut eq 1 or session.lockedOut eq 1>
	<cfif now() lt findUser.lockOutExpires>
		<cfset session.lockOutExpires = findUser.lockOutExpires>
		<cfset session.lockedOut = 1>
        <cfset session.LoggedIn = false>
        <cfquery datasource="#application.dsn#" name="insLog">
        INSERT INTO log_login(email,attempt_ts,success,ip,reason)
        VALUES(
        	<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.email#" />,
            SYSUTCDATETIME(),
            0,
            <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
            'DB AUTH: User authenticated, but locked out'
            )
        </cfquery>
		<cflocation url="login.cfm?message=Your account is locked out for 10 minutes." />
	<cfelse>
    	<cfquery datasource="#application.dsn#" name="undoLockout">
        UPDATE users
        SET lockedout=0,lockoutExpires=null
        WHERE email =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.email#" />
        </cfquery>
		<cfset session.lockedOut = 0>
		<cfset session.lockedOutExpires = ''>
	</cfif>
</cfif> 

<!--- Step 6 : Redirect to correct URL --->
        
        
<cfquery datasource="#application.dsn#" name="insLog">
INSERT INTO log_login(email,attempt_ts,success,ip,reason)
VALUES(
    <cfqueryparam cfsqltype="cf_sql_varchar" value="#findUser.email#" />,
    SYSUTCDATETIME(),
    1,
    <cfqueryparam cfsqltype="cf_sql_varchar" value="#cgi.REMOTE_ADDR#" />,
    'Success'
    )
</cfquery>

<cflocation url="#form.redirect#" />