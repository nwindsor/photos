<cfif find('/transactions/index.cfm',cgi.HTTP_REFERER) neq 0 AND
      isDefined('url.redirect') AND 
      find('/transactions/index.cfm',url.redirect) eq 0>
  Not Logged In -- AJAX based request.<cfabort>
</cfif>
<cfparam name="cookie.email" default="" />
<cfparam name="url.redirect" default="index.cfm" />
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><cfoutput>#application.sitename#</cfoutput> :: Login</title>
<cfinclude template="/includes/head.cfm" />
<link rel="stylesheet" href="/CSS/transactions.css">

<script language="javascript">
$(function(){
	
	window.addEventListener('online', function(e) {
	  goOnline();
	}, false);
	
	window.addEventListener('offline', function(e) {
	  goOffline();
	}, false);
	
if (navigator.onLine) {
  goOnline();
} else {
  goOffline();
}

});

function goOffline(){
	$('#connectionOff').css('display','');
	$('#loginBtn').attr('disabled',true);
}
function goOnline(){
	$('#connectionOff').css('display','none');
 	$('#loginBtn').attr('disabled',false);
	
}
</script>


</head>

<body>
<cfoutput><cfinclude template="/includes/message.cfm" /></cfoutput>
<div id="header">
<h1><a href="/"><cfoutput>#application.sitename#</cfoutput></a></h1>
</div>

<div id="content-main">
    <h2>Login</h2>
    <cfoutput>
    <p style="font-size:1em;margin-bottom:30px;margin-top:30px;">#application.welcomemessage#</p>
    </cfoutput>
    <div style="display:inline-block;vertical-align:top;">
    <form method="post" action="authenticate.cfm" class="task_form">
        <cfoutput><input type="hidden" name="redirect" value="#url.redirect#" /></cfoutput>
			<input type="hidden" name="mode" value="keyboard" />
        <table class="formTable">
            <tr>
                <td>Username:</td>
                <td><cfoutput><cfif isDefined('url.e') AND url.e neq ''><input type="hidden" name="email" value="#url.e#">#url.e#<cfelse><input type="" id="email" name="email" value="#cookie.email#" autocomplete="on" required maxlength="255" placeholder="username" style="width:300px;" /></cfif></cfoutput></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password"  style="width:300px;" required maxlength="25" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="Login" id="loginBtn" name="loginBtn" class="taskBtn"/></td>
                <!---  LDAP system, no need for password reset...
				<td style="text-align:right;"><input type="button" name="ForgotPassword" id="ForgotPassword" value="Forgot Password" onclick="window.location.href='forgot_password.cfm'" /></td>
            	--->
			</tr>
        </table>
    
    </form>
	</div>
	
	<cfquery name="isRFIDAllowed" datasource="#application.dsn#">
	SELECT * FROM list_element WHERE list_id=105 AND description = <cfqueryparam cfsqltype="cf_sql_varchar" value="#listGetAt(cgi.REMOTE_ADDR,1,'.')#.#listGetAt(cgi.REMOTE_ADDR,2,'.')#.#listGetAt(cgi.REMOTE_ADDR,3,'.')#" />
	</cfquery>
	
	<cfif isRFIDAllowed.recordcount gte 1 AND application.rfidAuth eq 1>
	<script language="javascript" src="/JS/rfidLoginBehavior.js"></script>
	<div style="display:inline-block;vertical-align:top;margin-left:50px"><a href="#rfidLoginPopup" id="rfidLoginPopupBtn"><img src="/Images/RFID.png" border="0"></a></div>
	<div id="rfidLoginPopup" class="white-popup-block mfp-hide" style="width:250px;height:175px;text-align:center;">
		<div id="ready" style="display:none;"><img src="/Images/rfid ready.png"></div>
		<div id="notReady" style="display:none;font-size:2em;">Not Ready<br><br><input type="button" id="rfidFocus" value="Click Here to Re-Initialize RFID"></div>
		
		<form method="post" action="/authenticate.cfm" id="rfidLogin" class="task_form">
			<cfoutput><input type="hidden" name="redirect" value="#url.redirect#" /></cfoutput>
			<input type="hidden" name="mode" value="rfid" />
			<input type="text" name="rfid" id="rfid" value="" maxlength="50" autocomplete="off"/>
		</form>
	</div>
    </cfif>
	
    <div id="connectionOff" style="text-align:center;font-weight:600;font-size:1.5em;border:1px dotted #666666;margin:10px;padding:10px;background-color:#efefef;">
    Error: Not Online<br>
    <span style="font-size:.8em;">You must be online in order to login. Please ensure you have a connection first. This screen will change to allow login as soon as a connection is established.</span>
    </div>
    
    <div style="margin-top:20px;border:2px solid gray;background-color:#F2F2F2;color:black;padding:15px;display:inline-block;">
    <h2>Notices</h2>
    
    <p>Don't remember your password?  It's the same as you use for your e-mail.  Your username is everything before the @ in your e-mail address, or you can type in the whole address.  Check with your Supervisor or IT if you have any issues. <!---Not sure you ever set one?  Use the <a href="forgot_password.cfm">forgot password</a> tool to fix it.---></p>
    </div>
</div>

<cfinclude template="/includes/copyright.cfm" />
</body>
</html>